/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.signalprocessing.frameprocessors.filter;

/**
 * Sliding window filter that sums the values in the window up.
 */
public class SumSlidingWindowFilter extends QueueSlidingWindowFilter<Double> {

    private final int filterLength;
    private Double sum;

    public SumSlidingWindowFilter(int filterLength, Double initialElement) {
        super(filterLength, initialElement);
        this.filterLength = filterLength;
        sum = initialElement * filterLength;
    }

    @Override
    public void onNewElement(Double element) {
        Double oldestElement = addNewElementToQueue(element);
        sum += element;
        sum -= oldestElement;
    }

    /**
     * Get the sum of the current elements inside the window.
     *
     * @return The sum.
     */
    public Double getSum() {
        return sum;
    }

    /**
     * Get the average of the current elements inside the window.
     *
     * @return The average.
     */
    public Double getAverage() {
        return sum / filterLength;
    }
}
