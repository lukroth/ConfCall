/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.client;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TimerTask;

/**
 * Removes all open conferences that have expired.
 */
public class OldConferenceRemover extends TimerTask {
    private static final long OPEN_CONFERENCE_TIMEOUT = 3000;
    private final List<AvailableConference> availableConferences;
    private final ClientConference.Listener availableConferenceListener;

    OldConferenceRemover(List<AvailableConference> availableConferences, ClientConference.Listener availableConferenceListener) {
        this.availableConferences = availableConferences;
        this.availableConferenceListener = availableConferenceListener;
    }

    @Override
    public void run() {
        long timeStamp = System.currentTimeMillis();
        Set<AvailableConference> timeOutConferences = new HashSet<>();
        for (AvailableConference availableConference : availableConferences) {
            if (timeStamp - availableConference.timeStamp > OPEN_CONFERENCE_TIMEOUT) {
                timeOutConferences.add(availableConference);
            }
        }
        if (!timeOutConferences.isEmpty()) {
            for (AvailableConference timedOutConference : timeOutConferences) {
                availableConferences.remove(timedOutConference);
            }
            availableConferenceListener.onAvailableConferencesChanged(availableConferences);
        }
    }
}
