/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.networking.wifibroadcaster.receive;

import java.net.DatagramPacket;
import java.util.Arrays;

public class Message {
    private final DatagramPacket originalMessage;
    // Warning: Data contains in the first bit the ReceiverIdentifier code!
    private final byte[] data;
    private String dataAsString;

    public byte[] getData() {
        return data;
    }

    public int getMessageReceiverIdentifier() {
        return messageReceiverIdentifier;
    }

    private final int messageReceiverIdentifier;

    /**
     * Never use this constructor!
     */
    private Message() {
        originalMessage = null;
        data = null;
        messageReceiverIdentifier = -1;
    }

    /**
     * Create the message from the a {@link DatagramPacket} containing a message of this type.
     *
     * @param originalMessage The original {@link DatagramPacket} which contains the message this.
     */
    public Message(DatagramPacket originalMessage) {
        this.originalMessage = originalMessage;
        this.data = originalMessage.getData();
        this.messageReceiverIdentifier = data[0];
    }

    public DatagramPacket getOriginalMessage() {
        return originalMessage;
    }

    public String getDataAsString() {
        // we read it if it is not cached already
        if (dataAsString == null) {
            // ignore the first int which specifies where the message belongs to
            dataAsString = (new String(Arrays.copyOfRange(data, 1, data.length))).trim();
        }
        return dataAsString;
    }
}
