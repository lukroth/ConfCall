/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.signalprocessing.synchronisation.gcc;

import android.util.Log;

import java.util.HashMap;
import java.util.Map;

import ch.ethz.disco.signalprocessing.frameprocessors.filter.MostSeenSlidingWindowFilter;

/**
 * This class is responsible to filter the resulting offsets we get from the GCC-Path. They might be
 * wrong (e.g. not much correlation of the signals due to low gain) for some reasons, and then we
 * want to ensure that for some longer time period we get the same offset and only then we want to
 * correct the recording.
 */
public class OffsetFilter {
    private static final String TAG = OffsetFilter.class.getCanonicalName();
    private static final int DEFAULT_SCALE_FACTOR = 10;
    /**
     * The length of the filter.
     */
    private int filterLength;
    /**
     * How many times must an offset been seen that it is considered to be a real offset. e.g. 70%
     * -> filterPercentage = 0.7
     */
    private double filterPercentage;
    /**
     * We round the offsets since the offset differs for some samples and we want to use a sliding
     * window filter that counts the same occurrences -> we round it down and then after the
     * filtering round it up again. This factor describes how much the offsets should be rounded.
     */
    private int scaleFactor;
    private Map<Integer, Map<Integer, MostSeenSlidingWindowFilter>> mostSeenValueSlidingWindowFilter;

    public OffsetFilter(int filterLength, double filterPercentage) {
        init(filterLength, filterPercentage, DEFAULT_SCALE_FACTOR);
    }

    OffsetFilter(int filterLength, double filterPercentage, int scaleFactor) {
        init(filterLength, filterPercentage, scaleFactor);
    }

    private void init(int filterLength, double filterPercentage, int scaleFactor) {
        this.filterLength = filterLength;
        this.filterPercentage = filterPercentage;
        this.scaleFactor = scaleFactor;
        this.mostSeenValueSlidingWindowFilter = new HashMap<>();
    }

    /**
     * Adding a new offset to the filter. The filter then returns the filtered offsets that can be
     * corrected.
     *
     * @param offsets map containing the offsets: signal1 -> signal2 -> offset(s1,s2)
     * @return Filtered offsets.
     */
    public Map<Integer, Map<Integer, Integer>> onNewOffsets(Map<Integer, Map<Integer, Integer>> offsets) {
        Map<Integer, Map<Integer, Integer>> resultMap = new HashMap<>();
        for (Map.Entry<Integer, Map<Integer, Integer>> entry : offsets.entrySet()) {
            for (Map.Entry<Integer, Integer> innerEntry : entry.getValue().entrySet()) {
                addToFilter(entry.getKey(), innerEntry.getKey(), innerEntry.getValue(), resultMap);
            }
        }
        return resultMap;
    }

    private void addToFilter(Integer s1ChannelId, Integer s2ChannelId, Integer offset, Map<Integer, Map<Integer, Integer>> resultMap) {
        if (!mostSeenValueSlidingWindowFilter.containsKey(s1ChannelId)) {
            mostSeenValueSlidingWindowFilter.put(s1ChannelId, new HashMap<Integer, MostSeenSlidingWindowFilter>());
        }
        if (!mostSeenValueSlidingWindowFilter.get(s1ChannelId).containsKey(s2ChannelId)) {
            mostSeenValueSlidingWindowFilter.get(s1ChannelId).put(s2ChannelId, new MostSeenSlidingWindowFilter(filterLength));
        }
        MostSeenSlidingWindowFilter filter = mostSeenValueSlidingWindowFilter.get(s1ChannelId).get(s2ChannelId);
        filter.newValue(downScaleOffset(offset));
        checkFilter(filter, s1ChannelId, s2ChannelId, resultMap);
    }

    private int downScaleOffset(Integer offset) {
        return offset / scaleFactor;
    }

    private int upScaleOffset(Integer offset) {
        return offset * scaleFactor;
    }

    /**
     * Checks if the filter
     *
     * @param filter    Filter that should be checked
     * @param s1Id      Corresponding signalId to the filter
     * @param s2Id      Corresponding signalId to the filter
     * @param resultMap The resultMap where the result should be written to if the filter gives a
     *                  result.
     */
    private void checkFilter(MostSeenSlidingWindowFilter filter, int s1Id, int s2Id, Map<Integer, Map<Integer, Integer>> resultMap) {
        MostSeenSlidingWindowFilter.MostSeenValue mostSeenValue = filter.getMostSeenValueAndCount();
        if (mostSeenValue.getValue() == 0) {
            // We want to ignore zero offset values.
            return;
        }
        if (mostSeenValue.getCount() >= filterLength * filterPercentage) {
            addToResultMap(s1Id, s2Id, upScaleOffset(mostSeenValue.getValue()), resultMap);
            // We reset the filter since we do not want to find the same offset again!
            filter.reset(filterLength);
        }
    }

    /**
     * Adds the mapping: s1Id -> s2Id -> offset to the resultMap
     *
     * @param s1Id      Id of the first signal
     * @param s2Id      Id of the second signal
     * @param offset    The offset between the two signals
     * @param resultMap The result map where the offset is added to
     */
    private void addToResultMap(int s1Id, int s2Id, int offset, Map<Integer, Map<Integer, Integer>> resultMap) {
        if (!resultMap.containsKey(s1Id)) {
            resultMap.put(s1Id, new HashMap<Integer, Integer>());
        }
        if (!resultMap.get(s1Id).containsKey(s2Id)) {
            resultMap.get(s1Id).put(s2Id, offset);
        } else {
            Log.e(TAG, "Multiple offset results for one signal does not make sense. Fix this.");
        }
    }
}
