/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.signalprocessing.frameprocessors.filter;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Abstract class for sliding window filters which want to have a queue as underlying element to
 * keep track of the oldest element that leaves the filter area.
 *
 * @param <E> Elements inside the Filter.
 */
abstract class QueueSlidingWindowFilter<E> {
    private Queue<E> queue = new LinkedList<>();

    /**
     * Create a new Sliding Window Filter.
     *
     * @param filterLength   The length of the sliding window
     * @param initialElement The sliding window is filled with this element at startup
     */
    QueueSlidingWindowFilter(int filterLength, E initialElement) {
        if (filterLength <= 0) {
            throw new IndexOutOfBoundsException("Tried to initialize a filter of length " + filterLength + ", but Filter must have at least length 1.");
        }
        // We fill the queue with initialElement
        for (int i = 0; i < filterLength; i++) {
            queue.add(initialElement);
        }
    }

    /**
     * Adds a new element to the queue and returns the oldest element.
     *
     * @param newElement The new element that should be added to the filter.
     * @return The oldest element that leaves the sliding window.
     */
    E addNewElementToQueue(E newElement) {
        queue.add(newElement);
        return queue.poll();
    }

    /**
     * Adding a new element to the filter
     * <p>
     * Most likely you want to call inhere super.addNewElementToQueue(E newElement);
     *
     * @param element The element that should be added to the filter
     */
    public abstract void onNewElement(E element);
}
