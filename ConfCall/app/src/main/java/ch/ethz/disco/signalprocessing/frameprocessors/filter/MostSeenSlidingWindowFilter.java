/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.signalprocessing.frameprocessors.filter;

import android.util.Log;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;


/**
 * A filter to make a signal (e.g. the current active microphones) more continuous.
 * <p>
 * When passing a new value it returns the most seen value since filterLength.
 */
public class MostSeenSlidingWindowFilter {
    private static final String TAG = MostSeenSlidingWindowFilter.class.getCanonicalName();
    HashMap<Integer, Integer> valuesToTimesSeen;
    private Queue<Integer> queue;

    /**
     * Creates a most seen value filter. Initial the filter is filled with zeros.
     *
     * @param filterLength The length of the filter.
     */
    public MostSeenSlidingWindowFilter(int filterLength) {
        this.reset(filterLength);
    }

    /**
     * Resets the filter. Same effect as creating a new filter.
     *
     * @param filterLength The length of the filter.
     */
    public void reset(int filterLength) {
        this.queue = new LinkedList<>();
        // We fill the queue with 0 assuming that at the startup of the filter just index 0 wins.
        for (int i = 0; i < filterLength; i++) {
            queue.add(0);
        }
        valuesToTimesSeen = new HashMap<>();
        valuesToTimesSeen.put(0, filterLength);
    }


    /**
     * When passing a new value it returns the most seen value since filterLength.
     *
     * @param newValue The current signal
     * @return int - The filtered signal
     */
    public int newValue(int newValue) {
        removeOldestSeenValue();
        addNewValue(newValue);
        return getMostSeenValue();
    }

    private int getMostSeenValue() {
        return getMostSeenValueAndCount().value;
    }

    /**
     * Get back the currently most seen value and how many times it was seen.
     *
     * @return MostSeenValue - The value and the count of the most seen value.
     */
    public MostSeenValue getMostSeenValueAndCount() {
        int seenCount = -1;
        int mostSeenValue = -1;
        for (Map.Entry<Integer, Integer> entry : valuesToTimesSeen.entrySet()) {
            int val = entry.getValue();
            if (val > seenCount) {
                mostSeenValue = entry.getKey();
                seenCount = val;
            }
        }
        if (seenCount == -1) {
            Log.w(TAG, "Could not find most seen index. Returning 0 instead.");
            return new MostSeenValue(0, 0);
        }
        return new MostSeenValue(mostSeenValue, seenCount);
    }

    private void addNewValue(int newValue) {
        queue.add(newValue);
        int val = 0;
        if (valuesToTimesSeen.containsKey(newValue)) {
            val = valuesToTimesSeen.get(newValue);
        }
        valuesToTimesSeen.put(newValue, val + 1);
    }

    private void removeOldestSeenValue() {
        if (queue.isEmpty()) {
            Log.e(TAG, "Size should never be below 1. Could not remove last value");
            return;
        }
        int oldestValue = queue.poll();
        int oldCount = 0;
        if (valuesToTimesSeen.containsKey(oldestValue)) {
            oldCount = valuesToTimesSeen.get(oldestValue);
        }
        if (oldCount <= 0) {
            Log.e(TAG, "Expected a count value > 0. Cannot remove.");
        }
        valuesToTimesSeen.remove(oldestValue);
        if (oldCount - 1 > 0) {
            valuesToTimesSeen.put(oldestValue, oldCount - 1);
        }
    }

    public class MostSeenValue {
        private int value;
        private int count;

        MostSeenValue(int mostSeenValue, int count) {
            this.value = mostSeenValue;
            this.count = count;
        }


        public int getValue() {
            return value;
        }

        public int getCount() {
            return count;
        }
    }
}