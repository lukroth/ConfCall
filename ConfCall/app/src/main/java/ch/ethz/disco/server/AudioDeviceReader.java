/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.server;

import android.media.AudioRecord;
import android.util.Log;

import ch.ethz.disco.signalprocessing.synchronisation.record.AudioSignalMixer;
import ch.ethz.disco.util.ArrayUtil;
import ch.ethz.disco.util.AudioRecordUtil;


/**
 * This class is responsible to read the audio data of this device and forwards it to the
 * audioSignalMixer.
 */
public class AudioDeviceReader extends Thread implements AudioSignalMixer.ChannelFiller {
    /**
     * When this flag is set to true, the native android echo canceller is disabled and we use the
     * Speex echo canceler also on the master device. Tests have shown that the speex echo canceler
     * is worse than the native android -> do not set this to true except for testing.
     */
    private static final boolean DISABLE_ANDROID_ECHO_CANCELLER = false;
    private static final String TAG = AudioDeviceReader.class.getCanonicalName();
    private final AudioRecord record;
    private final AudioSignalMixer audioSignalMixer;
    private final ErrorListener errorListener;
    private int channelId;
    private boolean goonRecording = false;

    /**
     * @param record           Initialized and recording audioRecord
     * @param audioSignalMixer The audioSignalMixer to which the data should be forwarded to
     */
    AudioDeviceReader(AudioRecord record, AudioSignalMixer audioSignalMixer, ErrorListener errorListener) {
        this.record = record;
        this.audioSignalMixer = audioSignalMixer;
        this.errorListener = errorListener;

        if (DISABLE_ANDROID_ECHO_CANCELLER) {
            AudioRecordUtil.disableAndroidEchoCanceler(record);
        }
    }

    public void startRecording() {
        if (goonRecording) {
            stopRecording();
        }
        goonRecording = true;
        channelId = audioSignalMixer.createNewHostChannel(this);
        // start the producer
        this.start();
    }

    public void stopRecording() {
        goonRecording = false;
        if (record != null) {
            try {
                record.stop();
                record.release();
            } catch (IllegalStateException e) {
                Log.d(TAG, "Record was already stopped. This is no problem.", e);
            }
        }
    }

    @Override
    public void run() {
        if (!checkRecordInitialized()) {
            return;
        }
        android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_AUDIO);
        record.startRecording();
        Log.v(TAG, "Started recording");
        while (goonRecording) {
            try {
                readAudioFromRecord();
            } catch (AudioDeviceReaderException e) {
                Log.e(TAG, "Could nor read", e);
                errorListener.onError(e.getMessage());
            }
        }
        // we do not call "record.release();"  since the SipDroid RtpStreamSender does release it.
        Log.v(TAG, "Stopped recording");
    }

    private void readAudioFromRecord() throws AudioDeviceReaderException {
        int expectedBufferLength = audioSignalMixer.getAudioDataPartLength() * 2;
        byte[] buffer = readNBytes(expectedBufferLength);
        if (buffer != null) {
            buffer = ArrayUtil.addZeroPadding(buffer, expectedBufferLength); // If we needed to add some samples
            audioSignalMixer.writeNativeEndian(channelId, buffer);
        }
    }

    /**
     * Reads size bytes from the input stream
     *
     * @param size How many bytes should be read.
     * @return The byte array of length @param{size} or null if an error occurred.
     */
    private byte[] readNBytes(int size) throws AudioDeviceReaderException {
        byte[] bytes = new byte[size];
        int offset = 0;
        while (offset < size) {
            int read = record.read(bytes, offset, size - offset);
            if (read <= 0) { // an error occurred
                if (goonRecording) {
                    Log.e(TAG, "Error while reading data from record. ErrorCode: " + read);
                    stopRecording();
                    throw new AudioDeviceReaderException("Error while reading data from record. ErrorCode: " + read);
                }
                return null;
            } else {
                offset += read;
            }
        }
        return bytes;
    }

    private boolean checkRecordInitialized() {
        if (record.getState() != AudioRecord.STATE_INITIALIZED) {
            Log.e(TAG, "Audio Record is not initialized");
            return false;
        }
        return true;
    }

    @Override
    public void correctOffset(int numberOfSamples) {
        throw new UnsupportedOperationException("On the master device we are not allowed to ignore samples");
    }

    public interface ErrorListener {
        void onError(String msg);
    }
}
