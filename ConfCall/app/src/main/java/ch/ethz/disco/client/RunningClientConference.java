/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.client;

import android.content.Context;
import android.util.Log;

import java.io.IOException;
import java.net.ConnectException;
import java.net.NoRouteToHostException;
import java.net.Socket;

import ch.ethz.disco.audio.MuteController;
import ch.ethz.disco.client.recorder.AudioRecorder;
import ch.ethz.disco.client.synchronisation.ClientPlaybackSynchronizer;
import ch.ethz.disco.networking.socket.ClientCommunicationInterface;
import ch.ethz.disco.playback.AudioTrackAndroidAudioPlayer;

import static ch.ethz.disco.client.synchronisation.Constants.ADMIRED_BUFFER_CONTENT_LENGTH_CLIENT_INITIAL_VAL;

/**
 * This class depicts a running client conference. Meaning there was a master which initiated a
 * conference ant this phone joined it. This consists of:
 * <p>
 * <li>the audio playback on the client device</li> <li>recording plus streaming to the master
 * device</li>
 */
public class RunningClientConference {
    private static final String TAG = RunningClientConference.class.getCanonicalName();
    private final Context context;
    // Can be empty. Wants to be informed about any status update.
    private final RunningClientConferenceEventListener runningClientConferenceEventListener;
    private AudioRecorder audioRecorder;

    private boolean isRunning = false;
    private AudioSender audioSender;
    private Socket socket;
    private int sampleRate;
    private AudioTrackAndroidAudioPlayer audioPlayer;
    private ClientCommunicationInterface clientCommunicationInterface;
    private ClientPlaybackSynchronizer clientPlaybackSynchronizer;
    private String hostAdress;

    public RunningClientConference(Context context, RunningClientConferenceEventListener runningClientConferenceEventListener) {
        this.context = context;
        this.runningClientConferenceEventListener = runningClientConferenceEventListener;
    }

    /**
     * Start streaming + playback, if not already running. If it is already running this call has no
     * effect.
     *
     * @param sampleRate  The sample rate of the record + playback
     * @param hostAddress The host address of the master
     * @param hostPort    The host port of the master
     * @throws IOException IO Exception
     */
    public synchronized void start(int sampleRate, String hostAddress, int hostPort) throws IOException {
        if (isRunning) {
            Log.d(TAG, "AudioBroadcaster is already running. Please first stop before restarting.");
        } else {
            this.sampleRate = sampleRate;
            isRunning = true;
            Log.v(TAG, "Start RunningClientConference");
            try {
                this.hostAdress = hostAddress;
                socket = new Socket(hostAddress, hostPort);
            } catch (NoRouteToHostException | ConnectException e) {
                Log.e(TAG, "Cannot reach host. Maybe firewall problems?", e);
                isRunning = false;
                runningClientConferenceEventListener.onReset("Cannot reach host. Maybe firewall problems?");
                return;
            }
            initRecording();
            initPlayback();
            runningClientConferenceEventListener.onStart(sampleRate, hostAddress, hostPort);
        }
    }

    private void initPlayback() throws IOException {
        audioPlayer = new AudioTrackAndroidAudioPlayer(sampleRate, context, ADMIRED_BUFFER_CONTENT_LENGTH_CLIENT_INITIAL_VAL);
        clientPlaybackSynchronizer = new ClientPlaybackSynchronizer(audioPlayer, sampleRate, hostAdress, runningClientConferenceEventListener);
        clientPlaybackSynchronizer.start();
        clientCommunicationInterface = new ClientCommunicationInterface(socket.getInputStream(), audioPlayer, this);
        clientCommunicationInterface.start();
    }

    private void initRecording() throws IOException {
        audioRecorder = new AudioRecorder(sampleRate);
        audioRecorder.startRecording();
        audioSender = new AudioSender(socket.getOutputStream(), audioRecorder);
        audioSender.startSending();
    }


    /**
     * Resets the current running conference.
     */
    public void resetCommand() {
        Log.w(TAG, "Got reset command from the master device.");
        this.stop();
        runningClientConferenceEventListener.onReset("Maybe connection problems to the master or other reasons.");
    }

    public void stopCommand() {
        Log.w(TAG, "Got stop command from the master device.");
        this.stop();
    }

    /**
     * Stop streaming + playback.
     */
    public synchronized void stop() {
        if (isRunning) {
            Log.v(TAG, "Stop RunningClientConference");
            stopRecording();
            stopPlayback();
            closeSocket();
            isRunning = false;
            runningClientConferenceEventListener.onStop();
        } else {
            Log.w(TAG, "Cannot stop a not running RunningClientConference");
        }
    }

    private void stopPlayback() {
        if (clientPlaybackSynchronizer != null) {
            clientPlaybackSynchronizer.stop();
        }

        if (clientCommunicationInterface != null) {
            clientCommunicationInterface.stop();
        }
        if (audioPlayer != null) {
            audioPlayer.stop();
        }
    }

    private void closeSocket() {
        if (socket != null) {
            try {
                socket.close();
            } catch (IOException e) {
                Log.e(TAG, "Could not close the socket", e);
            }
        }
    }

    private void stopRecording() {
        if (audioRecorder != null) {
            audioRecorder.stopRecording();
        }
        if (audioSender != null) {
            audioSender.stopSending();
        }
    }


    /**
     * @return A new mute controller or null if no one is available or no audio player is
     * initialized.
     */
    public MuteController getNewPlaybackMuteController() {
        if (audioPlayer != null) {
            MuteController muteController = new MuteController();
            audioPlayer.registerMuteController(muteController);
            return muteController;
        }
        return null;
    }

    public MuteController getNewRecordMuteController() {
        if (audioRecorder != null) {
            MuteController muteController = new MuteController();
            audioRecorder.registerMuteController(muteController);
            return muteController;
        }
        return null;
    }
}
