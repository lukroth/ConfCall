/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.networking.socket;

import android.util.Log;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;

/**
 * This class accepts new Sockets and calls its listeners if a new socket was accepted.
 */
public class ServerSocketAcceptor {
    private static final String TAG = ServerSocketAcceptor.class.getCanonicalName();
    private final int port;
    private final Listener listener;
    private ServerSocketAcceptorInternal serverSocketAcceptorInternal;

    /**
     * @param serverPort Port to listen on
     * @param listener   Listener which will be informed about every new socket.
     */
    ServerSocketAcceptor(int serverPort, Listener listener) {
        this.port = serverPort;
        this.listener = listener;
    }

    /**
     * Start listening for new Sockets
     */
    public void start() {
        serverSocketAcceptorInternal = new ServerSocketAcceptorInternal(port);
        serverSocketAcceptorInternal.start();
    }

    /**
     * Stop listening for new sockets.
     */
    public void stop() {
        if (serverSocketAcceptorInternal != null) {
            serverSocketAcceptorInternal.interrupt();
        }
    }

    /**
     * When creating a ServerSocketAcceptor a Listener which wants to be informed must be passed.
     * The Listener has to implement this interface.
     */
    interface Listener {
        /**
         * Gets called when a new Socket is accepted by the ServerSocketAcceptor.
         *
         * @param socket The new accepted Socket.
         */
        void onNewSocket(Socket socket);
    }

    /**
     * Internal Thread which accepts new Sockets.
     */
    class ServerSocketAcceptorInternal extends Thread {
        private final int port;

        /**
         * @param port The port where the ServerSocketAcceptorInternal listens on.
         */
        ServerSocketAcceptorInternal(int port) {
            this.port = port;
        }

        @Override
        public void run() {
            ServerSocket serverSocket = null;
            try {
                serverSocket = new ServerSocket(port);
                serverSocket.setSoTimeout(1000);
                Log.d(TAG, "Accepting new Connections");
                while (!this.isInterrupted()) {
                    acceptNewSocket(serverSocket);
                }
            } catch (SocketException e) {
                Log.e(TAG, "SocketError accepting new Sockets", e);
            } catch (IOException e) {
                Log.e(TAG, "IO error accepting new Sockets", e);
            } finally {
                if (serverSocket != null) {
                    try {
                        serverSocket.close();
                    } catch (IOException e) {
                        Log.e(TAG, "Could not close ServerSocket", e);
                    }
                }
            }
        }

        private void acceptNewSocket(ServerSocket serverSocket) throws IOException {
            try {
                Socket socket = serverSocket.accept();
                Log.d(TAG, "Accepted new Socket. Host address: " + socket.getInetAddress().getHostAddress());
                listener.onNewSocket(socket);
            } catch (SocketTimeoutException e) {
                // We can ignore this exception, we only set the so timeout
                // to ensure we can interrupt the thread
            }
        }
    }
}
