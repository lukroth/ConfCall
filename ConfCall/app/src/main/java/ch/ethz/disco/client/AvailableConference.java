/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.client;

public class AvailableConference {
    final int sampleRate;
    final String hostAddress;
    final int port;
    private final String conferenceName;
    long timeStamp;

    public AvailableConference(int sampleRate, String hostAddress, int port, String conferenceName, long timeStamp) {
        this.sampleRate = sampleRate;
        this.hostAddress = hostAddress;
        this.port = port;
        this.conferenceName = conferenceName;
        this.timeStamp = timeStamp;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof AvailableConference)) {
            return false;
        }
        AvailableConference other = (AvailableConference) obj;
        return other.conferenceName.equals(this.conferenceName) && other.hostAddress.equals(this.hostAddress) && other.port == this.port;
    }

    @Override
    public int hashCode() {
        int res = 8374;
        res *= 31;
        res += this.conferenceName.hashCode();
        res *= 31;
        res += this.hostAddress.hashCode();
        res *= 31;
        res += this.port;
        return res;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getName() {
        return conferenceName;
    }
}
