/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.signalprocessing.synchronisation.record;

import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import ch.ethz.disco.signalprocessing.synchronisation.TotalOffsetBookkeeper;
import ch.ethz.disco.signalprocessing.synchronisation.gcc.GCCPathOffsetFinder;
import ch.ethz.disco.signalprocessing.synchronisation.gcc.OffsetFilter;
import ch.ethz.disco.util.ArrayUtil;
import speex.EchoCancellerHelper;

/**
 * Simple AudioRecordSynchronizer that uses GCC to synchronize the audio record data.
 */
public class GCCAudioRecordSynchronizer implements AudioRecordSynchronizer {
    private static final String TAG = GCCAudioRecordSynchronizer.class.getCanonicalName();
    /**
     * How many frames are merged until GCC is performed.
     */
    private final int synchronisationLengthInFrames;
    private final TotalOffsetBookkeeper totalOffsetBookkeeper;
    private final EchoCancellerHelper echoCancellerHelper;
    private Queue<Frame> frameQueue = new ConcurrentLinkedQueue<>();
    private int offsetFilterLength = 5; // The filter length on which we search for same offsets
    private double requiredPercentage = 0.5; // We are fine if we find 2 with the same number
    private final OffsetFilter offsetFilter = new OffsetFilter(offsetFilterLength, requiredPercentage);
    private AudioSignalMixer audioSignalMixer;

    public GCCAudioRecordSynchronizer(int synchronisationLengthInFrames, TotalOffsetBookkeeper totalOffsetBookkeeper, EchoCancellerHelper echoCancellerHelper) {
        this.synchronisationLengthInFrames = synchronisationLengthInFrames;
        this.totalOffsetBookkeeper = totalOffsetBookkeeper;
        this.echoCancellerHelper = echoCancellerHelper;
    }

    @Override
    public synchronized void processFrame(Frame frame) {
        if (frame == null) {
            return;
        }
        frameQueue.add(frame);
        if (frameQueue.size() == synchronisationLengthInFrames) {
            processFrameBundle();
            this.frameQueue = new ConcurrentLinkedQueue<>();
        }
    }

    /**
     * Gets called when a bundle of frames is ready for processing. From this bundle then the
     * offsets are computed and adjusted.
     */
    private synchronized void processFrameBundle() {
        (new FrameBundleProcessor(frameQueue)).start();
    }

    @Override
    public void setAudioSignalMixer(AudioSignalMixer audioSignalMixer) {
        this.audioSignalMixer = audioSignalMixer;
    }

    class FrameBundleProcessor extends Thread {
        private final Queue<Frame> frameQueue;

        FrameBundleProcessor(Queue<Frame> frameQueue) {
            this.frameQueue = frameQueue;
        }

        @Override
        public void run() {
            // Note: this thread does not need audio thread priority since it does not matter if
            // we do not compute the offset immediately
            Map<Integer, short[]> assembledSignals = assembleSignals();
            computeOffsets(assembledSignals);
        }

        /**
         * Computes the offsets of the relative signals.
         *
         * @param assembledSignals The signals for which the offsets should be computed.
         */
        private void computeOffsets(Map<Integer, short[]> assembledSignals) {
            // We do not use this since we filter the results afterwards -> wrong results do not matter
            double minimumRequiredGain = 0;
            Map<Integer, Map<Integer, Integer>> offsets = GCCPathOffsetFinder.computeOffsets(assembledSignals, minimumRequiredGain);
            Log.v(TAG, "Offsets:" + offsets.toString());
            Map<Integer, Map<Integer, Integer>> filteredOffsets = offsetFilter.onNewOffsets(offsets);
            if (filteredOffsets.size() != 0) {
                Log.v(TAG, "FilteredOffsets:" + filteredOffsets.toString());
            }
            if (filteredOffsets.size() == 0) {
                return; // we don't need to adjust anything
            }
            Map<Integer, Integer> channelsToOffsets = getOffsetsRelativeToRefSignal(filteredOffsets);
            onNewNormedOffsets(channelsToOffsets);
        }

        /**
         * Should be called whenever we have a new set of offsets.
         *
         * @param channelsToOffsets signal -> offset. Meaning: Offset(RefSig, signal) = offset
         */
        private void onNewNormedOffsets(Map<Integer, Integer> channelsToOffsets) {
            correctOffsetsInAudioRecords(channelsToOffsets);
            totalOffsetBookkeeper.newOffsets(channelsToOffsets);
        }

        /**
         * Gets the different signal parts from the queue and assembles them to one big signal per
         * ChannelId. If one Channel has not a signal on every frame this channel gets ignored.
         *
         * @return A map channelId -> assembled signals.
         */
        Map<Integer, short[]> assembleSignals() {
            Map<Integer, List<short[]>> assembled = new HashMap<>();
            int numberOfFrames = frameQueue.size();
            while (!frameQueue.isEmpty()) {
                Map<Integer, short[]> frame = frameQueue.poll().getChannelsToDataMap();
                for (int channelId : frame.keySet()) {
                    if (!assembled.containsKey(channelId)) {
                        assembled.put(channelId, new ArrayList<short[]>());
                    }
                    assembled.get(channelId).add(frame.get(channelId));
                }
            }
            // we now have a list of the signals and have to concatenate this list to one array
            Map<Integer, short[]> result = new HashMap<>();
            for (int channelId : assembled.keySet()) {
                List<short[]> signalAsList = assembled.get(channelId);
                if (signalAsList.size() == numberOfFrames) {
                    // We only want to take signals that have full length, so no missing parts.
                    result.put(channelId, ArrayUtil.concatenateShortArrays(signalAsList));
                }
            }
            return result;
        }

        private void correctOffsetsInAudioRecords(Map<Integer, Integer> channelIdToOffsets) {
            for (Map.Entry<Integer, Integer> entry : channelIdToOffsets.entrySet()) {
                correctOffsetInAudioRecord(entry.getKey(), entry.getValue());
                // We inform the echo cancellation since we ignored samples so the echo canceller need to readjust/reset
                echoCancellerHelper.correctOffsetInAudioRecord(entry.getKey(), entry.getValue());
            }
        }

        private void correctOffsetInAudioRecord(int channelId, int sampleCount) {
            if (sampleCount == 0) {
                return; // we do not have to do anything
            }
            if (!audioSignalMixer.getChannelFillerMap().containsKey(channelId)) {
                Log.e(TAG, "Should ignore samples on a non existing channel - not possible");
                return;
            }
            AudioSignalMixer.ChannelFiller channelFiller = audioSignalMixer.getChannelFillerMap().get(channelId);
            channelFiller.correctOffset(sampleCount);
        }

        /**
         * This function calculates how much every signal must ignore/add such that then all signals
         * are aligned relative to the reference signal.
         *
         * @param filteredOffsets s1 -> s1 -> offset(s1, s2);
         * @return channelId -> how much samples this channel should ignore / add
         */
        private Map<Integer, Integer> getOffsetsRelativeToRefSignal(Map<Integer, Map<Integer, Integer>> filteredOffsets) {
            if (filteredOffsets.size() == 0) {
                return new HashMap<>(); // we don't need to adjust anything
            }
            // We assume that we have one signal
            if (filteredOffsets.size() != 1) {
                Log.e(TAG, "Expected to have just 1 signal and the others are relative to this one");
                return new HashMap<>();
            }
            if (filteredOffsets.keySet().iterator().next() != AudioSignalMixer.getHostChannelId()) {
                Log.e(TAG, "Expected to be relative to the host channel already. Can not handle this." + filteredOffsets.toString());
                return new HashMap<>();
            }
            return filteredOffsets.get(AudioSignalMixer.getHostChannelId());
        }
    }
}
