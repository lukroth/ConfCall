/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.util;

import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FileWriter {
    private static final String TAG = FileWriter.class.getName();
    private final String fileName;
    private StringBuilder stringBuilder = new StringBuilder();
    private boolean append = true;

    public FileWriter(String fileName, boolean append) {
        this.fileName = fileName;
        this.append = append;
    }

    /**
     * Creates a FileWriter with filename equal to the current time.
     */
    public FileWriter(String prefix, String postfix) {
        this.fileName = prefix + getDateForTitle() + postfix;
    }

    /**
     * Creates a FileWriter with filename equal to the current time.
     */
    public FileWriter() {
        this.fileName = getDateForTitle() + ".txt";
    }

    public static String getDateForTitle() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss_SSS");
        return sdf.format(new Date());
    }

    public static boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }

    /**
     * Appends the data to the file or logs an error if something went wrong.
     *
     * @param filename e.g. "myData.txt"
     * @param content  e.g. "Hello\nBlablabla\n"
     */
    public static void writeToFile(String filename, String content) {
        writeToFile(filename, content, true);
    }

    /**
     * Writes the data to the file or logs an error if something went wrong.
     *
     * @param filename e.g. "myData.txt"
     * @param content  e.g. "Hello\nBlablabla\n"
     * @param append   If append=true the data gets appended.
     */
    public static void writeToFile(String filename, String content, boolean append) {
        File root = android.os.Environment.getExternalStorageDirectory();
        if(root == null){
            Log.e(TAG, "Could not write to the file");
            return;
        }
        File dir = new File(root.getAbsolutePath() + "/sipdroid/");
        dir.mkdirs();
        File file = new File(dir, filename);
        FileOutputStream fileOutputStream = null;
        PrintWriter printWriter = null;
        try {
            fileOutputStream = new FileOutputStream(file, append);
            printWriter = new PrintWriter(fileOutputStream);
            printWriter.print(content);
            printWriter.flush();
            printWriter.close();
            fileOutputStream.close();
        } catch (IOException e) {
            onError(e);
        } finally {
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (IOException e) {
                    onError(e);
                }
            }
        }
    }

    private static void onError(IOException e) {
        Log.e(TAG, "Could not write to the file", e);
    }

    public void write(String content) {
        stringBuilder.append(content);
    }

    public void write(String content, boolean flush) {
        write(content);
        if (flush) {
            flushToFile();
        }
    }

    public void flushToFile() {
        writeToFile(fileName, stringBuilder.toString(), append);
        stringBuilder = new StringBuilder();
    }
}
