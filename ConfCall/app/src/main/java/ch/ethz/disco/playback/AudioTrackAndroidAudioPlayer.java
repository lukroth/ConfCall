/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.playback;

import android.content.Context;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.os.Build;
import android.util.Log;

import java.util.concurrent.ConcurrentLinkedQueue;

import ch.ethz.disco.ConferenceSettings;
import ch.ethz.disco.signalprocessing.frameprocessors.filter.SumSlidingWindowFilter;

/**
 * Simple audio player stream player. Wraps the AudioTrack of android.
 * <p>
 * Tries to have always the same amount of samples inside of its buffer.
 */
public class AudioTrackAndroidAudioPlayer extends SynchronisationCapableAudioPlayer {
    /**
     * If the {@link this#admiredBufferContentLength} is below this value it is ignored.
     */
    private static final int MINIMUM_ADMIRED_BUFFER_CONTENT_LENGTH = 500;
    private static final String TAG = AudioTrackAndroidAudioPlayer.class.getCanonicalName();
    private static final int STREAM_TYPE = AudioManager.STREAM_VOICE_CALL;
    private static final int INITIAL_SPEAKER_VOLUME_IN_PERCENTAGE = 60;
    private static final double MINIMUM_BUFFER_SIZE_IN_SECONDS = 0.2;
    /**
     * Should be slower than {@link AudioTrackAndroidAudioPlayer#AVERAGE_ELEMENTS_IN_BUFFER_FILTER_LENGTH}
     * to enable a fast synchronisation at startup.
     */
    private static final int AVERAGE_ELEMENTS_IN_BUFFER_FILTER_LENGTH_STARTUP = 10;
    private static final int AVERAGE_ELEMENTS_IN_BUFFER_FILTER_LENGTH = 120;
    private final Context context;
    private final Object offsetLock = new Object();
    private final ConcurrentLinkedQueue<BufferObject> bufferQueue = new ConcurrentLinkedQueue<>();
    /**
     * This field decides how many rounds the {@link AudioTrackAndroidAudioPlayer#AVERAGE_ELEMENTS_IN_BUFFER_FILTER_LENGTH_STARTUP}
     * is taken until it is replaced by {@link AudioTrackAndroidAudioPlayer#AVERAGE_ELEMENTS_IN_BUFFER_FILTER_LENGTH}
     * .
     */
    private int averageElemsInBufferFilterLengthStartupCounter = 18;
    /**
     * The buffer is adjusted from time to time to this value. This is necessary to keep different
     * devices synchronized.
     */
    private int admiredBufferContentLength;
    private int oldDeviceVolume = 0;
    private AudioTrack audioTrack;
    private boolean isPlaying = false;
    private int totalPlayedSamples = 0;
    private int offsetToCorrect = 0;
    private boolean insertingSamplesIfNecessary = false;
    private boolean startUp = true;
    private int samplesInBufferQueue = 0;
    private SumSlidingWindowFilter averageElementsInBuffer = new SumSlidingWindowFilter(AVERAGE_ELEMENTS_IN_BUFFER_FILTER_LENGTH_STARTUP, (double) 0);
    private int admiredBufferContentLengthCounter = 0;

    public AudioTrackAndroidAudioPlayer(int sampleRateInHz, Context context, int admiredBufferContentLength) {
        this.context = context;
        this.admiredBufferContentLength = admiredBufferContentLength;
        int channelConfig = AudioFormat.CHANNEL_OUT_MONO;
        int bufferSize = AudioTrack.getMinBufferSize(sampleRateInHz, channelConfig, ConferenceSettings.AUDIO_FORMAT);

        bufferSize = (int) Math.max(bufferSize, sampleRateInHz * MINIMUM_BUFFER_SIZE_IN_SECONDS * 2); // *2 since in bytes and we write shorts.
        Log.v(TAG, "Initializing AudioTrack (2). SampleRateHZ = " + sampleRateInHz + ", ChannelConfig=" + channelConfig + ", BufferSizeInBytes=" + bufferSize);
        audioTrack = new AudioTrack(STREAM_TYPE, sampleRateInHz, channelConfig, ConferenceSettings.AUDIO_FORMAT, bufferSize, AudioTrack.MODE_STREAM);
        initVolume();
    }

    private void initVolume() {
        AudioManager am = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        if (am == null) {
            Log.e(TAG, "Could not get the audioManager. Can not set to mode MODE_IN_COMMUNICATION and not on speaker mode");
        } else {
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                am.setMode(AudioManager.MODE_IN_COMMUNICATION);
            } else {
                am.setMode(AudioManager.MODE_IN_CALL);
            }
            am.setSpeakerphoneOn(true);
        }
        setTrackVolume();
        oldDeviceVolume = setDeviceVolume(STREAM_TYPE, INITIAL_SPEAKER_VOLUME_IN_PERCENTAGE);
    }

    private void setTrackVolume() {
        // I am not sure if this even has an effect...
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            int volumeResponseCode = audioTrack.setVolume(AudioTrack.getMaxVolume());
            if (volumeResponseCode != AudioTrack.SUCCESS) {
                Log.e(TAG, "Could not set the audio volume, error code = " + volumeResponseCode);
            }
        } else {
            Log.w(TAG, "Could not set audio track volume, to old SDK");
        }
    }

    /**
     * Sets the devices volume.
     *
     * @param streamType e.g. RunningClientConference.STREAM_MUSIC
     * @param percentage 0 - 100: how loud, 0 = silence, 100 = maximum
     * @return old volume in percentage.
     */
    private int setDeviceVolume(int streamType, int percentage) {
        AudioManager am = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        if (am == null) {
            Log.e(TAG, "Could not get Audio Manager. Will not set Volume");
            return 0;
        }
        double oldVolume = am.getStreamVolume(streamType);
        if (percentage > 100 || percentage < 0) {
            Log.e(TAG, "Volume percentage must be in [0:100], but was " + percentage + ". Will not set Volume");
            return (int) oldVolume;
        }
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (am.isVolumeFixed()) {
                Log.w(TAG, "Cannot set volume, since device has VolumeFixed");
                return (int) oldVolume;
            }
        } else {
            Log.w(TAG, "Cannot change volume. To old SDK.");
            return (int) oldVolume;
        }

        // or use adjustStreamVolume instead?
        int maxVolume = am.getStreamMaxVolume(streamType);
        int volume = (int) (maxVolume * (0.01 * percentage));
        am.setStreamVolume(
                streamType,
                volume,
                0);
        return (int) (oldVolume / maxVolume);
    }

    @Override
    public void start() {
        startUp = false;
        Log.v(TAG, "Start playing");
        if (audioTrack != null) {
            try {
                Log.v(TAG, "Samples in AudioTrack:" + (totalPlayedSamples - audioTrack.getPlaybackHeadPosition()) + ", inBufferQueue:" + samplesInBufferQueue + ", queueLength= " + bufferQueue.size());
                isPlaying = true;
                audioTrack.play();
            } catch (IllegalStateException e) {
                Log.e(TAG, "Could not start", e);
            }
        }
    }

    @Override
    public void pause() {
        Log.v(TAG, "Pause playing");
        isPlaying = false;
        audioTrack.pause();
    }

    @Override
    public int getPlaybackHeadPosition() {
        try {
            return audioTrack.getPlaybackHeadPosition();
        } catch (IllegalStateException e) {
            Log.w(TAG, "Could not get playbackHeadPosition", e);
            return 0;
        }
    }

    @Override
    public void stop() {
        Log.v(TAG, "Stop playing");
        isPlaying = false;
        audioTrack.stop();
        audioTrack.release();
        setDeviceVolume(STREAM_TYPE, oldDeviceVolume);
    }

    @Override
    public int write(short[] audioData, final int offsetInShorts, final int sizeInShorts, int writtenShorts) {
        startIfNotAlreadyPlaying();
        insertSamplesIfNecessary();
        writeIntoOwnBuffer(new BufferObject(audioData, offsetInShorts, sizeInShorts));
        transferDataFromBufferToAudioTrack();
        return sizeInShorts;
    }

    private void insertSamplesIfNecessary() {
        if (!insertingSamplesIfNecessary) { // Avoid recursive infinity loop when writing again.
            synchronized (offsetLock) {
                insertingSamplesIfNecessary = true;
                if (offsetToCorrect > 0) {
                    int toInsertOriginal = offsetToCorrect;

                    int bundleMaxSize = 320;
                    int toInsert = toInsertOriginal;
                    while (toInsert > 0) {
                        int insertInThisRound = Math.min(bundleMaxSize, toInsert);
                        // We need to insert some samples
                        // We add it to the queue and do not write it directly!
                        write(new short[insertInThisRound], 0, insertInThisRound, 0);
                        toInsert -= insertInThisRound;
                    }
                    Log.v(TAG, "Inserted " + toInsertOriginal + " samples");
                    offsetToCorrect -= toInsertOriginal;
                }
                insertingSamplesIfNecessary = false;
            }
        }
    }

    private int writeToAudioTrack(short[] audioData, final int offsetInShorts, final int sizeInShorts) {
        if (isMute()) {
            audioData = new short[audioData.length];
        }
        if (offsetToCorrect < 0) { // We do not want to take the lock in each round, only if we think there might be something.
            synchronized (offsetLock) {
                int offsetToCorrectCopy = offsetToCorrect;
                if (offsetToCorrect < 0) {
                    // We need to delete some samples
                    int maxPossibleOffsetWeCanCorrect = sizeInShorts - offsetInShorts;
                    int offsetThatGetsCorrected = Math.min(maxPossibleOffsetWeCanCorrect, -offsetToCorrect);
                    int playedSamplesWhenDeleted = audioTrack.write(audioData, offsetInShorts, sizeInShorts - offsetThatGetsCorrected);
                    offsetToCorrect += offsetThatGetsCorrected; // We might only have corrected some parts of the offset.
                    totalPlayedSamples += playedSamplesWhenDeleted;
                    Log.v(TAG, "Deleted " + offsetThatGetsCorrected + " samples | offset before correction:" + offsetToCorrectCopy + ". Offset after correction:" + offsetToCorrect);
                    return sizeInShorts; //We behave as if we would have played everything.
                }
            }
        }
        int playedSamples;
        playedSamples = audioTrack.write(audioData, offsetInShorts, sizeInShorts);
        totalPlayedSamples += playedSamples;
        startIfNotAlreadyPlaying(); // We start now if we did not start at the beginning because of to less data.
        return playedSamples;
    }

    private synchronized void transferDataFromBufferToAudioTrack() {
        BufferObject nextBufferedObject = bufferQueue.peek();
        while (nextBufferedObject != null) {
            int written = writeToAudioTrack(nextBufferedObject.audioData, nextBufferedObject.offsetInShorts, nextBufferedObject.sizeInShorts);
            if (isErrorCode(written)) {
                return;
            }
            samplesInBufferQueue -= written;
            if (written != nextBufferedObject.sizeInShorts) {
                nextBufferedObject.offsetInShorts += written;
                Log.e(TAG, "Could not write all data:" + written + ", instead of " + nextBufferedObject.sizeInShorts + " have been written");
                // We exit since we could not write everything we know that the buffer is full.
                break;
            } else {
                bufferQueue.poll(); //Remove the element we just wrote
                nextBufferedObject = bufferQueue.peek(); // And check for a new one
            }
        }
        adjustToAdmiredBufferContent();
    }

    private void adjustToAdmiredBufferContent() {
        int samplesInAudioTrackBuffer = totalPlayedSamples - getPlaybackHeadPosition();
        averageElementsInBuffer.onNewElement((double) samplesInAudioTrackBuffer);

        int filterLength = getBufferElementFilterLength();
        if ((!startUp) && (admiredBufferContentLength > MINIMUM_ADMIRED_BUFFER_CONTENT_LENGTH) && (admiredBufferContentLengthCounter++ % filterLength == 0)) {
            int currentAverage = averageElementsInBuffer.getAverage().intValue();
            int samplesToAdd = admiredBufferContentLength - currentAverage;
            Log.v(TAG, "currentAverage=" + currentAverage + " -> samplesToAdd=" + samplesToAdd);
            correctOffset(samplesToAdd);
            averageElemsInBufferFilterLengthStartupCounter--;
            filterLength = getBufferElementFilterLength(); // The current filter length might have changed -> use the new filter.
            averageElementsInBuffer = new SumSlidingWindowFilter(filterLength, (double) 0);
        }
    }

    private boolean isErrorCode(int written) {
        if (written < 0) {
            Log.e(TAG, "Error Writing audio, ErrorCode=" + written);
            return true;
        }
        return false;
    }

    private void writeIntoOwnBuffer(BufferObject bufferObject) {
        bufferQueue.add(bufferObject);
        samplesInBufferQueue += bufferObject.sizeInShorts;
    }

    @Override
    public void startIfNotAlreadyPlaying() {
        if (!isPlaying) {
            start();
        }
    }

    @Override
    public void correctOffset(int numberOfSamplesToAddToThisDevicesPlaybackStream) {
        synchronized (offsetLock) {
            offsetToCorrect += numberOfSamplesToAddToThisDevicesPlaybackStream;
        }
    }

    @Override
    public boolean setAdmiredBufferContentLength(int admiredBufferContentLength) {
        if (MINIMUM_ADMIRED_BUFFER_CONTENT_LENGTH < admiredBufferContentLength) {
            Log.v(TAG, "New admiredBufferContentLength = " + admiredBufferContentLength);
            this.admiredBufferContentLength = admiredBufferContentLength;
            return true;
        } else {
            Log.v(TAG, "To small value, must be above:" + MINIMUM_ADMIRED_BUFFER_CONTENT_LENGTH + " but was " + admiredBufferContentLength);
            return false;
        }
    }

    private int getBufferElementFilterLength() {
        if (averageElemsInBufferFilterLengthStartupCounter > 0) {
            return AVERAGE_ELEMENTS_IN_BUFFER_FILTER_LENGTH_STARTUP;
        } else {
            return AVERAGE_ELEMENTS_IN_BUFFER_FILTER_LENGTH;
        }
    }

    private class BufferObject {
        private final short[] audioData;
        private final int sizeInShorts;
        private int offsetInShorts;

        BufferObject(short[] audioData, int offsetInShorts, int sizeInShorts) {
            this.audioData = audioData;
            this.offsetInShorts = offsetInShorts;
            this.sizeInShorts = sizeInShorts;
        }
    }
}
