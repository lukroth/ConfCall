/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.signalprocessing.frameprocessors.relativegain;

import android.util.Log;

import java.util.HashMap;
import java.util.Map;

/**
 * This class finds out what the noise level of each channel is.
 * <p>
 * It analyses the audio data and searches for "silent" periods. It then checks how Loud
 * this silent periods are and assumes this to be the noise.
 */
class MaximumMeasurementsBookkeeper {
    private static final String TAG = MaximumMeasurementsBookkeeper.class.getCanonicalName();
    private final int maxMeasuredSliceLength;
    private final Map<Integer, MaximumMeasurementsBookkeeperPerSignal> noiseBookkeepersPerSignal = new HashMap<>();
    private final int forgetThreshold;

    /**
     * @param maxMeasuredSliceLength         The length of the sliding window which is used to find
     *                                       the slice
     * @param maxMeasuredSliceValidityWindow How many frames ago the maxMeasuredSlice is allowed to
     *                                       be.
     */
    MaximumMeasurementsBookkeeper(int maxMeasuredSliceLength, int maxMeasuredSliceValidityWindow) {
        this.maxMeasuredSliceLength = maxMeasuredSliceLength;
        this.forgetThreshold = maxMeasuredSliceValidityWindow;
    }

    /**
     * Should be called on every new frame.
     *
     * @param avgGains The average gain values of the different microphones.
     */
    public void onNewFrame(Map<Integer, Double> avgGains) {
        for (Integer channelId : avgGains.keySet()) {
            if (!noiseBookkeepersPerSignal.containsKey(channelId)) {
                noiseBookkeepersPerSignal.put(channelId, new MaximumMeasurementsBookkeeperPerSignal(maxMeasuredSliceLength, forgetThreshold));
            }
            noiseBookkeepersPerSignal.get(channelId).onNewGain(avgGains.get(channelId));
        }
    }


    /**
     * Calculates from the avgGains the Signal-to-noise ratios.
     *
     * @param avgGains The average gains
     * @return {@code List<Long>} The SNRs derived from the average gains passed as an argument.
     */
    public Map<Integer, Double> getRelativeFromAverageGains(Map<Integer, Double> avgGains) {
        Map<Integer, Double> relativeGains = new HashMap<>(avgGains.size());
        for (Integer channelId : avgGains.keySet()) {
            if (noiseBookkeepersPerSignal.containsKey(channelId)) {
                double maxSeenGain = noiseBookkeepersPerSignal.get(channelId).getMaxSeenGain();
                if (maxSeenGain != 0) {
                    double avgGain = avgGains.get(channelId);
                    relativeGains.put(channelId, avgGain / maxSeenGain);
                } else {
                    Log.w(TAG, "Got zero maxSeenGain for signal " + channelId + ", which is not possible. Adding the average instead of the maxSeenGain");
                    relativeGains.put(channelId, avgGains.get(channelId));
                }
            } else {
                Log.e(TAG, "Expected to have for every signal a maxSeenGain value. This is not the case. Adding the average instead of the maxSeenGain");
                relativeGains.put(channelId, avgGains.get(channelId));
            }
        }
        return relativeGains;
    }

    /**
     * @param channelId The channelId for which the measurement is requested
     * @return The maximum measurement of channelID or 0 if no measurement available.
     */
    public double getMaxMeasurement(int channelId) {
        MaximumMeasurementsBookkeeperPerSignal maximumMeasurementsBookkeeperPerSignal = noiseBookkeepersPerSignal.get(channelId);
        if (maximumMeasurementsBookkeeperPerSignal != null) {
            return maximumMeasurementsBookkeeperPerSignal.getMaxSeenGain();
        } else {
            return 0;
        }
    }
}
