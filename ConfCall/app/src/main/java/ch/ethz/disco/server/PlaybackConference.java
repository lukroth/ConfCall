/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.server;

import android.content.Context;
import android.util.Log;

import java.util.concurrent.atomic.AtomicInteger;

import ch.ethz.disco.networking.socket.ServerCommunicationInterface;
import ch.ethz.disco.playback.AudioBroadcaster;
import ch.ethz.disco.playback.AudioTrackAndroidAudioPlayer;
import ch.ethz.disco.playback.SynchronisationCapableAudioPlayer;
import speex.EchoCancellerHelper;

import static ch.ethz.disco.client.synchronisation.Constants.ADMIRED_BUFFER_CONTENT_LENGTH_MASTER;

/**
 * Responsible to play the audio data on all the phones connected to the conference.
 * <p>
 * Imitates the behaviour of a {@code android.media.AudioTrack}.
 */
public class PlaybackConference {
    private static final String TAG = PlaybackConference.class.getCanonicalName();
    private final SynchronisationCapableAudioPlayer audioPlayerOfThisDevice;
    private final AudioBroadcaster audioBroadcaster;
    private final EchoCancellerHelper echoCancellerHelper;
    private final int sampleRate;
    private boolean isPlaying;
    private AtomicInteger writtenShorts = new AtomicInteger(0);
    private boolean firstWrite = true;
    private int lastSeenSeq;
    private int defaultPacketSize;

    PlaybackConference(Context context, int sampleRateInHz, int bufferSizeInBytes, ServerCommunicationInterface serverCommunicationInterface, EchoCancellerHelper echoCancellerHelper) {
        this.sampleRate = sampleRateInHz;
        Log.v(TAG, "Initializing the audioPlayback Conference");
        isPlaying = false;
        if (bufferSizeInBytes % 2 != 0) {
            Log.e(TAG, "Expected an even byte buffer size number");
        }
        this.audioPlayerOfThisDevice = new AudioTrackAndroidAudioPlayer(sampleRateInHz, context, ADMIRED_BUFFER_CONTENT_LENGTH_MASTER);
        this.audioBroadcaster = new AudioBroadcaster(serverCommunicationInterface);
        this.echoCancellerHelper = echoCancellerHelper;
    }

    /**
     * Starts playing
     */
    public void play() {
        isPlaying = true;
        audioPlayerOfThisDevice.start();
        audioBroadcaster.start();
    }

    /**
     * Pauses the playback of the audio data. Data that has not been played back will not be
     * discarded. Subsequent calls to play() will play this data back.
     */
    public void pause() {
        isPlaying = false;
        audioPlayerOfThisDevice.pause();
        audioBroadcaster.pause();
    }

    public int getPlaybackHeadPosition() {
        return audioPlayerOfThisDevice.getPlaybackHeadPosition();
    }

    public void stop() {
        isPlaying = false;
        audioPlayerOfThisDevice.stop();
        audioBroadcaster.stop();
        writtenShorts.set(0);
    }

    private int write(short[] audioData, int offsetInShorts, int sizeInShorts) {
        int tempWrittenShorts = writtenShorts.addAndGet(sizeInShorts);
        echoCancellerHelper.playedData(audioData, offsetInShorts, sizeInShorts);
        int sentShorts = audioBroadcaster.write(audioData, offsetInShorts, sizeInShorts, isPlaying, tempWrittenShorts);
        if (sentShorts != sizeInShorts) {
            Log.w(TAG, "Did not broadcast all the shorts for some reason...");
        }
        writeToThisDevice(audioData, offsetInShorts, sizeInShorts, tempWrittenShorts);
        return sizeInShorts;
    }

    /**
     * We add a manual delay to this device to ensure the others have enough audioData and can
     * synchronize to this signal.
     */
    private void writeToThisDevice(final short[] audioData, final int offsetInShorts, final int sizeInShorts, int tempWrittenShorts) {
        int written = audioPlayerOfThisDevice.write(audioData, offsetInShorts, sizeInShorts, tempWrittenShorts);
        if (written != sizeInShorts) {
            Log.e(TAG, "Expected to write " + sizeInShorts + " shorts, but wrote only " + written);
        }
    }

    public int write(short[] audioData, int offsetInShorts, int sizeInShorts, int sequenceNr) {
        sequenceNr = sequenceNr & 0xff;
        if (firstWrite) {
            this.lastSeenSeq = sequenceNr - 1;
            this.defaultPacketSize = sizeInShorts;
            Log.v(TAG, "Initializing with sequenceNr=" + lastSeenSeq + ", defaultPacketSize=" + defaultPacketSize);
            firstWrite = false;
        }

        if (sequenceNr <= lastSeenSeq && (lastSeenSeq < (0xff - 20))) {
            // We can ignore this packet since it is arrived to late
            Log.w(TAG, "Packet with seq=" + sequenceNr + " arrived to late (lastSeenSeq=" + lastSeenSeq + ")");
            return sizeInShorts;
        }
        int expectedSeqNr = (lastSeenSeq + 1) & 0xff;
        if (expectedSeqNr < 20 && sequenceNr > (0xff - 20)) {
            //corner case if we have to fill when the sequence number reaches its maximum: e.g. when expected = 10 & sequenceNr = 0xff - 2, we also have to fill
            int gap = Math.abs((sequenceNr - expectedSeqNr) & 0xff);
            Log.w(TAG, "[1] Packet with seq=" + sequenceNr + " arrived (expectedSeqNr=" + expectedSeqNr + "), filling in zeros");
            addZeroPackets(gap);
        } else {
            // We fill with zeros to ensure we do not get a buffer underflow when playing the audio -> always enough audio is available
            if (sequenceNr > expectedSeqNr) {
                int gap = sequenceNr - expectedSeqNr;
                Log.w(TAG, "[2] Packet with seq=" + sequenceNr + " arrived (expectedSeqNr=" + expectedSeqNr + "), filling in zeros");
                addZeroPackets(gap);
            }
        }
        lastSeenSeq = sequenceNr;
        return write(audioData, offsetInShorts, sizeInShorts);
    }

    /**
     * Used to fill in gaps of packets that were not received by the VOIP protocol. Since no data
     * available zero data packets were added to get a continuous audio stream.
     *
     * @param numberOfPacketsToAdd Number of packets that will be inserted.
     */
    private void addZeroPackets(int numberOfPacketsToAdd) {
        Log.w(TAG, "Filling in " + numberOfPacketsToAdd + " zero packets");
        for (int i = 0; i < numberOfPacketsToAdd; i++) {
            write(new short[defaultPacketSize], 0, defaultPacketSize);
        }
    }
}
