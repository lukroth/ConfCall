/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.signalprocessing.echocancellation;

import java.util.Map;

import ch.ethz.disco.signalprocessing.synchronisation.record.AudioSignalMixer;
import ch.ethz.disco.signalprocessing.synchronisation.record.Frame;

import static ch.ethz.disco.util.ArrayUtil.average;

/**
 * This echo canceller uses the hostChannel to find out if only echo is present and if so it does
 * set the data from the other channels to 0 as well.
 * <p>
 * The assumption is that the hostChannel will send a silent array of data when only echo is present
 * (which is the case if the android echo canceller is working). Then we know that also the other
 * channels should not record.
 * <p>
 * <b>Warning:</b> This only works if the hostChannel records all speakers! If the phones are to far
 * apart, then the hostChannel might not record a speaker far apart and the phone in front of the
 * speaker that is far apart will be muted even though no echo is present. Then this echo canceller
 * makes no sense!
 * <p>
 * Note: A normal Echo Canceller must still be present since if somebody interrupts the callee this
 * echo canceller will not work.
 */
public class EchoCancellerUseAndroidEchoCanceller {
    private EchoCancellerUseAndroidEchoCanceller() {
    }

    /**
     * Removes the data from all other channels except the host channel if no data is present on the
     * hostChannel.
     *
     * @param frame The frame on which the echo gets removed.
     */
    public static void processFrame(Frame frame) {
        if (hostHasNoSignal(frame)) {
            setAllSignalsToZero(frame);
        }
    }

    private static boolean hostHasNoSignal(Frame frame) {
        Integer hostChannelId = AudioSignalMixer.getHostChannelId();
        short[] hostChannelAudioData = frame.getChannelsToDataMap().get(hostChannelId);
        double avgGain = average(hostChannelAudioData);
        return avgGain < 1;
    }

    private static void setAllSignalsToZero(Frame frame) {
        Map<Integer, short[]> channelsToData = frame.getChannelsToDataMap();
        int hostChannelId = AudioSignalMixer.getHostChannelId();
        for (Map.Entry<Integer, short[]> entry : channelsToData.entrySet()) {
            if (entry.getKey() != hostChannelId) {
                entry.setValue(new short[entry.getValue().length]);
            }
        }
    }
}
