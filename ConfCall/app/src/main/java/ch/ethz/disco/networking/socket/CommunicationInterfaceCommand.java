/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.networking.socket;

import android.annotation.SuppressLint;
import android.util.Log;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

/**
 * Holds all possible commands and maps them to integers and back.
 */
public enum CommunicationInterfaceCommand {
    // IMPORTANT: If you add a state here also add it in the static block where the commandToCodes map is filed.
    AUDIO_START, // Start playing
    AUDIO_PAUSE, // Pause playing
    AUDIO_STOP, // Stop playing
    AUDIO_WRITE, // Write audio data which should be played
    AUDIO_WRITE_START_PLAY_IF_NOT_PLAYING, // Same as AUDIO_WRITE but additional start playing if not already playing
    CONFERENCE_RESET_CONNECTION, // Tells the client to reset everything.
    CONFERENCE_STOP; // Tells the clients that the conference is over.

    private static final Map<CommunicationInterfaceCommand, Integer> commandToCodes = new EnumMap<>(CommunicationInterfaceCommand.class);
    /**
     * To have a bidirectional map implemented over two maps. Do not want to get extra dependency.
     */
    private static final Map<Integer, CommunicationInterfaceCommand> codesToCommand = new HashMap<>();
    private static final String TAG = CommunicationInterfaceCommand.class.getCanonicalName();

    static {
        //Note: 0 as code is not allowed.
        commandToCodes.put(AUDIO_START, 1);
        commandToCodes.put(AUDIO_PAUSE, 2);
        commandToCodes.put(AUDIO_STOP, 3);
        commandToCodes.put(AUDIO_WRITE, 4);
        commandToCodes.put(AUDIO_WRITE_START_PLAY_IF_NOT_PLAYING, 5);
        commandToCodes.put(CONFERENCE_RESET_CONNECTION, 6);
        commandToCodes.put(CONFERENCE_STOP, 7);
        createBidirectionalMap();
    }

    /**
     * Creates a copy of the map which maps in the other direction.
     */
    private static void createBidirectionalMap() {
        for (Map.Entry<CommunicationInterfaceCommand, Integer> entry : commandToCodes.entrySet()) {
            codesToCommand.put(entry.getValue(), entry.getKey());
        }
    }

    /**
     * Code -> Enum
     *
     * @param code The code number of the {@link CommunicationInterfaceCommand}
     * @return The {@link CommunicationInterfaceCommand} associated to the code. Null if no {@link
     * CommunicationInterfaceCommand} corresponds to this code.
     */
    public static CommunicationInterfaceCommand fromCode(int code) {
        return codesToCommand.get(code);
    }

    /**
     * Enum -> Code
     *
     * @return The code associated with this {@link CommunicationInterfaceCommand}.
     */
    public int getCode() {
        Integer code = commandToCodes.get(this);
        if (code == null) {
            Log.e(TAG, "No code defined for enum: " + this);
            return 0;
        }
        return code;
    }
}
