/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.signalprocessing.frameprocessors.snr;

import android.util.Log;

import java.util.HashMap;
import java.util.Map;

/**
 * This class finds out what the noise level of each channel is.
 * <p>
 * To do so it analyses the audio data and searches for "silent" periods. It then checks how Loud
 * this silent periods are and assumes this to be the noise.
 */
class NoiseThresholdBookkeeper {
    private static final String TAG = NoiseThresholdBookkeeper.class.getCanonicalName();
    private final int silenceSlidingWindowSize;
    /**
     * Maps the ChannelIds to the {@link NoiseThresholdBookkeeperPerSignal}
     */
    private final Map<Integer, NoiseThresholdBookkeeperPerSignal> noiseBookkeepersPerSignal = new HashMap<>();
    private final int forgetThreshold;

    /**
     * @param silenceSlidingWindowSize The length of the sliding window which is used to find the
     *                                 silence - in other words how long the silence parts are to
     *                                 measure the silence
     * @param forgetThreshold          How many frames ago the silence is allowed to be.
     */
    NoiseThresholdBookkeeper(int silenceSlidingWindowSize, int forgetThreshold) {
        this.silenceSlidingWindowSize = silenceSlidingWindowSize;
        this.forgetThreshold = forgetThreshold;
    }

    /**
     * Should be called on every new frame to update the noise threshold for each of the
     * microphones.
     *
     * @param avgGains The average gain values of the different microphones.
     */
    public void onNewFrame(Map<Integer, Double> avgGains) {
        for (Integer channelId : avgGains.keySet()) {
            if (!noiseBookkeepersPerSignal.containsKey(channelId)) {
                noiseBookkeepersPerSignal.put(channelId, new NoiseThresholdBookkeeperPerSignal(silenceSlidingWindowSize, forgetThreshold));
            }
            noiseBookkeepersPerSignal.get(channelId).onNewGain(avgGains.get(channelId));
        }
    }


    /**
     * Calculates from the avgGains the Signal-to-noise ratios.
     *
     * @param avgGains The average gains
     * @return {@code List<Long>} The SNRs derived from the average gains passed as an argument.
     */
    public Map<Integer, Double> getSNRFromAvgGains(Map<Integer, Double> avgGains) {
        Map<Integer, Double> snrs = new HashMap<>(avgGains.size());
        for (Integer channelId : avgGains.keySet()) {
            if (noiseBookkeepersPerSignal.containsKey(channelId)) {
                double noise = noiseBookkeepersPerSignal.get(channelId).getNoiseGain();
                if (noise != 0) {
                    snrs.put(channelId, avgGains.get(channelId) / noise);
                } else {
                    Log.w(TAG, "Got zero noise for signal " + channelId + ", which is not possible. Adding the average instead of the noise");
                    snrs.put(channelId, avgGains.get(channelId));
                }
            } else {
                Log.e(TAG, "Expected to have for every signal a noise value. This is not the case. Adding the average instead of the noise");
                snrs.put(channelId, avgGains.get(channelId));
            }
        }
        return snrs;
    }
}
