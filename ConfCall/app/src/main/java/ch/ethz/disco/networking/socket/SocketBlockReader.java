/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.networking.socket;

import android.os.Process;
import android.util.Log;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;

import ch.ethz.disco.signalprocessing.synchronisation.record.AudioSignalMixer;
import ch.ethz.disco.util.StreamUtil;

import static ch.ethz.disco.util.ArrayUtil.addZeroPadding;

/**
 * A wrapper class for the Socket class.
 * <p>
 * Reads the data from a socket in blocks if available and forwards it to the audioSignalMixer.
 */
class SocketBlockReader extends Thread implements AudioSignalMixer.ChannelFiller {
    private static final String TAG = SocketBlockReader.class.getCanonicalName();
    private final Socket socket;
    private final AudioSignalMixer audioSignalMixer;
    private final ServerCommunicationInterface serverCommunicationInterface;
    private final InputStream inputStream;
    private final int channelId;
    private final Object offsetToCorrectLock = new Object();
    private final DataOutputStream dataOutputStream;
    private boolean isReading = false;
    private Integer offsetToCorrect = 0;

    /**
     * Creates a SocketBlockReader. The @param{socket} is used to read from and the data is passed
     * to the audioSignalMixer.
     *
     * @param socket                       The socket the class should wrap
     * @param audioSignalMixer             The audioSignalMixer where the read packets are sent to.
     * @param serverCommunicationInterface The communication interface to read from
     */
    SocketBlockReader(Socket socket, AudioSignalMixer audioSignalMixer, ServerCommunicationInterface serverCommunicationInterface) throws IOException {
        this.socket = socket;
        this.inputStream = socket.getInputStream();
        this.dataOutputStream = new DataOutputStream(socket.getOutputStream());
        this.audioSignalMixer = audioSignalMixer;
        this.serverCommunicationInterface = serverCommunicationInterface;
        this.channelId = this.audioSignalMixer.createNewChannel(this);
    }

    /**
     * Stop reading and close the socket connected to this SocketReader.
     */
    public void stopReading() {
        isReading = false;
        try {
            this.socket.close();
            Log.d(TAG, "Socket closed");
        } catch (IOException e) {
            Log.e(TAG, "Could not close the Socket.", e);
        }
        serverCommunicationInterface.removeSocketBlockReader(this);
    }

    /**
     * Starts reading blocks from the socket and forwards them to the audioSignalMixer
     */
    public void startReading() {
        isReading = true;
        this.start();
    }

    @Override
    public void run() {
        Process.setThreadPriority(Process.THREAD_PRIORITY_URGENT_AUDIO);
        try {
            while (isReading) {
                readAudioFromSocket();
            }
        } catch (IOException e) {
            // If not isReading we can ignore the exception since we will catch a SocketClosed
            // exception when we stop reading. We then are no more interested in the socket anyway.
            if (isReading) {
                Log.w(TAG, "No more input from channel channelId=" + channelId + ". Removing the Channel", e);
            }
        }
        Log.d(TAG, "Stop reading from socket");
        stopReading();
        audioSignalMixer.removeChannel(channelId);
    }

    private void readAudioFromSocket() throws IOException {
        int expectedBufferLength = audioSignalMixer.getAudioDataPartLength() * 2;
        int toRead = expectedBufferLength;
        synchronized (offsetToCorrectLock) {
            if (offsetToCorrect > 0) {
                StreamUtil.readNBytes(inputStream, offsetToCorrect * 2);
                Log.d(TAG, "Ignored " + offsetToCorrect + " samples on channel:" + getChannelId());
                offsetToCorrect = 0;
            } else if (offsetToCorrect < 0) {
                // we have to add some samples
                // if we need to add more samples that we would read in one round
                int samplesAdded = Math.min(toRead, Math.abs(offsetToCorrect * 2)); // / 2 since here are bytes
                toRead = toRead - samplesAdded;
                offsetToCorrect = offsetToCorrect + samplesAdded / 2; // / 2 since here are bytes
                Log.d(TAG, "Added " + samplesAdded + " samples to channel:" + getChannelId());
            }
        }
        byte[] buffer = StreamUtil.readNBytes(inputStream, toRead);
        if (buffer != null) {
            buffer = addZeroPadding(buffer, expectedBufferLength); // If we needed to add some samples
            audioSignalMixer.writeNativeEndian(channelId, buffer);
        }
    }

    @Override
    public void correctOffset(int numberOfSamples) {
        synchronized (offsetToCorrectLock) {
            offsetToCorrect += numberOfSamples;
        }
    }

    public int getChannelId() {
        return channelId;
    }

    /**
     * Sends the command and the data over the socket: [command, data.length, data]
     *
     * @param command The command code
     * @param data    The data. (Not null).
     */
    public synchronized void sendCommandAndData(int command, int value, byte[] data) {
        // is synchronized since we want the writing to the outStream happen one command after each other.
        try {
            dataOutputStream.writeInt(command);
            dataOutputStream.writeInt(value);
            int dataLength = data.length;
            dataOutputStream.writeInt(dataLength);
            if (dataLength > 0) {
                dataOutputStream.write(data);
            }
            dataOutputStream.flush();
        } catch (IOException e) {
            if (isReading) {
                Log.e(TAG, "Could not send the command and the data to channel channelId=" + getChannelId());
            }
        }
    }
}
