/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.util;

import android.util.Log;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.List;

public class ArrayUtil {
    private static final String TAG = ArrayUtil.class.getCanonicalName();

    private ArrayUtil() {
    }

    /**
     * Creates a new array of the newLength as a copy of the old array with zeros at the biginning.
     *
     * @param original  The original array
     * @param newLength The length the result array should have. Expected to be >= original.length
     * @return The Zero-padded array.
     * @throws ArrayIndexOutOfBoundsException If newLength < original
     */
    public static byte[] addZeroPadding(byte[] original, int newLength) {
        if (original.length == newLength) {
            return original;
        } else {
            byte[] newArray = new byte[newLength];
            System.arraycopy(original, 0, newArray, newLength - original.length, original.length);
            return newArray;
        }
    }

    /**
     * Concatenates the arrays passed to one big array
     *
     * @param list The arrays that should be concatenated.
     * @return The concatenated array.
     */
    public static short[] concatenateShortArrays(List<short[]> list) {
        int totalLength = 0;
        for (short[] array : list) {
            totalLength += array.length;
        }
        short[] result = new short[totalLength];
        int currentIndex = 0;
        for (short[] array : list) {
            System.arraycopy(array, 0, result, currentIndex, array.length);
            currentIndex += array.length;
        }
        if (currentIndex != totalLength) {
            Log.e(TAG, "Expected the merged array to have the same length as the sum of the initial arrays");
        }
        return result;
    }


    /**
     * Creates a copy of the input short[] data and returns it as byte array (Big endian).
     *
     * @param data           The data that should be converted
     * @param offsetInShorts The offset from where on the data is taken in the data array.
     * @param sizeInShorts   The size of the copied range.
     * @return A copy of the specified range in data as byte array
     */
    public static byte[] toByteArray(short[] data, int offsetInShorts, int sizeInShorts) {
        ByteBuffer byteBuf = ByteBuffer.allocate(2 * sizeInShorts);
        int pos = offsetInShorts;
        while (sizeInShorts + offsetInShorts > pos) {
            byteBuf.putShort(data[pos]);
            pos++;
        }
        return byteBuf.array();
    }

    /**
     * @param data The byte array. Assumes its length to be even.
     * @return A copy of the input array as a short array. (Big Endian Conversion)
     */
    public static short[] toShortArray(byte[] data) {
        if (data.length % 2 != 0) {
            Log.e(TAG, "Expected the length of the data array to be even. Returning empty array.");
            return new short[0];
        }
        short[] shorts = new short[data.length / 2];
        ByteBuffer.wrap(data).order(ByteOrder.BIG_ENDIAN).asShortBuffer().get(shorts);
        return shorts;
    }


    /**
     * Converts an integer to a byte array. Use {@link ArrayUtil#fromByteArray(byte[])} to get the
     * integer back.
     *
     * @param value The integer to convert
     * @return The integer as a byte array.
     */
    public static byte[] toByteArray(int value) {
        return ByteBuffer.allocate(4).putInt(value).array();
    }

    /**
     * Converts an byte array to an integer. Use {@link ArrayUtil#toByteArray(int)} to get the bytes
     * from an integer.
     *
     * @param bytes The integer as byte array
     * @return The integer as integer
     */
    public static int fromByteArray(byte[] bytes) {
        return ByteBuffer.wrap(bytes).getInt();
    }


    /**
     * Calculates the average of an array of shorts.
     *
     * @param shorts The short data array
     * @return double - the average of the short data
     */
    public static double average(short[] shorts) {
        double sum = 0;
        for (short s : shorts) {
            sum += Math.abs(s);
        }
        return sum / shorts.length;
    }
}
