/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.signalprocessing.frameprocessors.snr;

import ch.ethz.disco.signalprocessing.frameprocessors.filter.MinimumSlidingWindowFilter;
import ch.ethz.disco.signalprocessing.frameprocessors.filter.SumSlidingWindowFilter;

/**
 * Keeps track of the noise level of a signal.
 */
class NoiseThresholdBookkeeperPerSignal {
    private final SumSlidingWindowFilter sumSlidingWindowFilter;
    private final MinimumSlidingWindowFilter<Double> minimumSlidingWindowFilter;

    NoiseThresholdBookkeeperPerSignal(int silenceSlidingWindowSize, int forgetThreshold) {
        /*
        We start with a very high value since then we assume a very noisy channel in the beginning
        until we actually have some data.

        We have to divide it by silenceSlidingWindowSize because otherwise we would create an
        overflow in the filter.
         */
        double initialValue = 9999d;
        sumSlidingWindowFilter = new SumSlidingWindowFilter(silenceSlidingWindowSize, initialValue);
        minimumSlidingWindowFilter = new MinimumSlidingWindowFilter<>(forgetThreshold, initialValue);
    }

    void onNewGain(double newGain) {
        sumSlidingWindowFilter.onNewElement(newGain);
        double noiseGainOfCurrentWindow = sumSlidingWindowFilter.getAverage();
        minimumSlidingWindowFilter.onNewElement(noiseGainOfCurrentWindow);
    }

    double getNoiseGain() {
        return minimumSlidingWindowFilter.getMinimum();
    }
}
