/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.signalprocessing.frameprocessors.snr;

import android.util.Log;

import java.util.HashMap;
import java.util.Map;

import ch.ethz.disco.signalprocessing.frameprocessors.ChannelSelector;
import ch.ethz.disco.signalprocessing.frameprocessors.FrameValidityChecker;
import ch.ethz.disco.signalprocessing.frameprocessors.filter.MostSeenSlidingWindowFilter;
import ch.ethz.disco.signalprocessing.synchronisation.record.AudioSignalMixer;

import static ch.ethz.disco.util.ArrayUtil.average;

/**
 * ChannelSelector that takes the frame with the maximum Signal to noise ratio (SNR)
 */
public class MaximumSignalToNoiseRatio implements ChannelSelector {
    private static final String TAG = MaximumSignalToNoiseRatio.class.getCanonicalName();

    /**
     * The length of the filter which controls the change of speaker.
     */
    private static final int SPEAKER_CHANGE_FILTER_LENGTH_MS = 333;
    /**
     * How long the silence should be that it will be measured as silence.
     */
    private static final int SILENCE_LENGTH_IN_MS = 2000; // 2 Sec
    /**
     * How long a measured silence is valid.
     */
    private static final int SILENCE_VALIDITY_IN_MS = 60 * 1000; // 1 Minute
    private final MostSeenSlidingWindowFilter speakerChangeFilter;
    private final NoiseThresholdBookkeeper noiseThresholdBookkeeper;
    private int oldActive = -1;

    MaximumSignalToNoiseRatio(int sampleRate, int frameLength) {
        int silenceSlidingWindow = (int) convertMsToFrames(sampleRate, SILENCE_LENGTH_IN_MS, frameLength);
        int silenceValidityWindow = (int) convertMsToFrames(sampleRate, SILENCE_VALIDITY_IN_MS, frameLength);
        noiseThresholdBookkeeper = new NoiseThresholdBookkeeper(silenceSlidingWindow, silenceValidityWindow);
        speakerChangeFilter = new MostSeenSlidingWindowFilter((int) convertMsToFrames(sampleRate, SPEAKER_CHANGE_FILTER_LENGTH_MS, frameLength));
    }

    /**
     * Calculates the amount of frames such that it will have a real length of ms
     *
     * @param sampleRatePerSec Sample rate of the audio data
     * @param lengthInMs       The length in ms which should be converted into frames
     * @param frameLength      The length of one frame
     * @return The filter length that must be passed to the filter - or how many frames are used to
     * have the desired time
     */
    static double convertMsToFrames(int sampleRatePerSec, int lengthInMs, int frameLength) {
        return (double) lengthInMs * sampleRatePerSec / (1000 * frameLength);
    }

    @Override
    public short[] selectFrame(Map<Integer, short[]> frame) {
        if (FrameValidityChecker.isInvalidFrame(frame)) {
            return new short[0];
        }
        return findCurrentActiveFrame(frame);
    }

    private short[] findCurrentActiveFrame(Map<Integer, short[]> frame) {
        int currentMaxSNRIndex = findMaximumSNRIndex(frame);
        int currentActiveFilteredIndex = speakerChangeFilter.newValue(currentMaxSNRIndex);
        logActiveMicrophone(currentActiveFilteredIndex);
        if (frame.containsKey(currentActiveFilteredIndex)) {
            return frame.get(currentActiveFilteredIndex);
        } else {
            Log.v(TAG, "Active phone not sending at the moment. Returning the master phones frame instead");
            short[] hostFrame = frame.get(AudioSignalMixer.getHostChannelId());
            if (hostFrame == null) {
                Log.e(TAG, "Active and host frames are empty");
            }
            return hostFrame;
        }
    }

    private void logActiveMicrophone(int currentActive) {
        if (oldActive != currentActive) {
            Log.v(TAG, "Changed active microphone to " + currentActive);
            oldActive = currentActive;
        }
    }

    int findMaximumSNRIndex(Map<Integer, short[]> frame) {
        Map<Integer, Double> avgGains = calculateAverageGains(frame);
        noiseThresholdBookkeeper.onNewFrame(avgGains);
        Map<Integer, Double> snrValues = noiseThresholdBookkeeper.getSNRFromAvgGains(avgGains);
        if (snrValues.size() != frame.size()) {
            Log.e(TAG, "Expected the snr values to have the same size as the noise values");
        }
        double maxSNR = -1;
        int maxSNRIndex = -1;
        for (Integer channelId : snrValues.keySet()) {
            double snr = snrValues.get(channelId);
            if (snr > maxSNR) {
                maxSNR = snr;
                maxSNRIndex = channelId;
            }
        }
        if (maxSNR == -1) {
            Log.e(TAG, "Could not find maximum inside data. Returning the first frame");
            return 0;
        }
        return maxSNRIndex;
    }

    private Map<Integer, Double> calculateAverageGains(Map<Integer, short[]> frame) {
        Map<Integer, Double> avgGains = new HashMap<>(frame.size());
        for (Integer channelId : frame.keySet()) {
            avgGains.put(channelId, average(frame.get(channelId)));
        }
        return avgGains;
    }
}
