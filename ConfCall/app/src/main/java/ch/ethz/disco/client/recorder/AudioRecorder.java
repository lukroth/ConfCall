/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.client.recorder;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.os.Build;
import android.util.Log;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import ch.ethz.disco.audio.MuteController;

/**
 * Reads the audio Samples into a queue and buffers it to ensure everything is read. Acts as a
 * producer of AudioBuffers.
 * <p>
 * Thanks to https://github.com/roman10/roman10-android-tutorial/tree/master/AndroidWaveRecorder
 */
public class AudioRecorder extends Thread {
    private static final String TAG = AudioRecorder.class.getCanonicalName();
    private final int sampleRate;

    private AudioRecord record;
    private boolean goonRecording = false;

    private BlockingQueue<AudioBuffer> audioBufferQueue = new LinkedBlockingQueue<>();

    private int bufferSize;
    private Set<MuteController> muteControllers = new HashSet<>();

    public AudioRecorder(int sampleRate) {
        this.sampleRate = sampleRate;
    }

    /**
     * Start recording and filling AudioBuffers into the {@code audioBufferQueue}.
     */
    public void startRecording() {
        if (goonRecording) {
            stopRecording();
        }
        goonRecording = true;
        // start the producer
        this.start();
    }

    /**
     * Stop recording.
     */
    public void stopRecording() {
        goonRecording = false;
    }

    /**
     * Returns the next buffered AudioBuffer if available. Blocks until next AudioBuffer gets
     * available.
     *
     * @return AudioBuffer - The next available AudioBuffer.
     * @throws InterruptedException When interrupted during waiting for new audioBuffers
     */
    public AudioBuffer take() throws InterruptedException {
        return audioBufferQueue.take();
    }

    @Override
    public void run() {
        Log.d(TAG, "Initialize AudioRecord. SampleRate:" + sampleRate);
        android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_AUDIO);
        initBufferSize();
        try {
            int audioSource;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                audioSource = MediaRecorder.AudioSource.VOICE_COMMUNICATION;
            } else {
                audioSource = MediaRecorder.AudioSource.MIC;
            }
            record = new AudioRecord(audioSource,
                    sampleRate,
                    AudioFormat.CHANNEL_IN_MONO,
                    AudioConstants.ENCODING,
                    bufferSize);
            if (!checkRecordInitialized()) {
                return;
            }
            android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_AUDIO);
            record.startRecording();
            Log.v(TAG, "Started recording");
            while (goonRecording) {
                readAudioFromRecord();
            }
        } finally {
            record.release();
        }
        Log.v(TAG, "Stopped recording");
    }

    /**
     * Reads the audio data from the AudioRecord into a buffer and adds the buffer to the
     * bufferQueue.
     */
    private void readAudioFromRecord() {
        AudioBuffer audioBufferObj = new AudioBuffer(bufferSize);
        byte[] audioBuffer = audioBufferObj.getAudioByteBuffer();
        int numReadBytes = record.read(audioBuffer, 0, audioBuffer.length);
        if (onMute()) {
            // If we are on mute we set the recording to 0
            audioBufferObj.setAudioByteBufferToZero();
        }
        audioBufferObj.setReadBytes(numReadBytes);
        try {
            audioBufferQueue.put(audioBufferObj);
        } catch (InterruptedException e) {
            Log.e(TAG, e.getMessage());
            Thread.currentThread().interrupt();
        }
    }

    private boolean onMute() {
        for (MuteController muteController : muteControllers) {
            if (muteController.isMute()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks if the record is initialized.
     *
     * @return True if initialized. False otherwise.
     */
    private boolean checkRecordInitialized() {
        if (record.getState() != AudioRecord.STATE_INITIALIZED) {
            Log.e(TAG, "Audio Record can't initialize!");
            return false;
        }
        return true;
    }

    /**
     * Calculates the BufferSize for this device + the settings of this AudioRecorder and saves it
     */
    private void initBufferSize() {
        // buffer size in bytes
        bufferSize = AudioRecord.getMinBufferSize(sampleRate,
                AudioFormat.CHANNEL_IN_MONO,
                AudioConstants.ENCODING);

        if (bufferSize == AudioRecord.ERROR || bufferSize == AudioRecord.ERROR_BAD_VALUE) {
            bufferSize = sampleRate * 2;
        }
    }

    public void registerMuteController(MuteController muteController) {
        muteControllers.add(muteController);
    }
}
