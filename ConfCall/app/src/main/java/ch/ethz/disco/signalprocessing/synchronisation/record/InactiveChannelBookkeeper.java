/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.signalprocessing.synchronisation.record;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Keeps track of the inactive channels and reports them if inactive for to long.
 */
class InactiveChannelBookkeeper {
    /**
     * How long a channel is allowed to be inactive until it gets reported.
     */
    private final int inactiveThreshold;
    /**
     * Maps the channels that are inactive to the number of times they have been inactive
     * (inactiveCounter).
     */
    private Map<Integer, Integer> inactiveChannels = new HashMap<>();

    /**
     * @param inactiveThreshold How many times a channel is allowed to be inactive until it gets
     *                          reported.
     */
    InactiveChannelBookkeeper(int inactiveThreshold) {
        this.inactiveThreshold = inactiveThreshold;
    }

    /**
     * Removes all the channels that were not in the inactiveChannelsIds from the {@link
     * InactiveChannelBookkeeper#inactiveChannels} map. Increments the inactiveCount for the newly
     * inactive channels. Reports the inactive channels above the threshold.
     *
     * @param inactiveChannelIds The channels that are inactive at the moment.
     * @return The channels that were longer inactive than {@link InactiveChannelBookkeeper#inactiveThreshold}
     */
    public synchronized Set<Integer> onInactiveChannels(Set<Integer> inactiveChannelIds) {
        updateInactiveThresholdMap(inactiveChannelIds);
        return getChannelsAboveThreshold();
    }

    private Set<Integer> getChannelsAboveThreshold() {
        Set<Integer> channelsAboveThreshold = new HashSet<>();
        for (Map.Entry<Integer, Integer> entry : inactiveChannels.entrySet()) {
            if (entry.getValue() > inactiveThreshold) {
                channelsAboveThreshold.add(entry.getKey());
            }
        }
        return channelsAboveThreshold;
    }

    private void updateInactiveThresholdMap(Set<Integer> inactiveChannelIds) {
        Map<Integer, Integer> stillInactive = new HashMap<>();
        for (Integer inactiveChannelId : inactiveChannelIds) {
            stillInactive.put(inactiveChannelId, getIncrementedInactiveCounter(inactiveChannelId));
        }
        inactiveChannels = stillInactive;
    }

    /**
     * Returns the new inactiveCount for the channelId.
     *
     * @param channelId The channel id for which we want the incremented inactiveCounter
     * @return 1 if first time inactive. old + 1 otherwise.
     */
    private Integer getIncrementedInactiveCounter(Integer channelId) {
        if (!inactiveChannels.containsKey(channelId)) {
            // this means this channel was the first time inactive.
            return 1;
        }
        int inactiveCounter = inactiveChannels.get(channelId);
        return inactiveCounter + 1;
    }
}
