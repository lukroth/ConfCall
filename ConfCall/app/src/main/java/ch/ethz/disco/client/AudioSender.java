/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.client;

import android.util.Log;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;

import ch.ethz.disco.client.recorder.AudioBuffer;
import ch.ethz.disco.client.recorder.AudioRecorder;

/**
 * This class takes the recorded samples form the producer queue (e.g. audioRecorder) and sends it
 * over the socket to the consumer.
 */
class AudioSender extends Thread {
    private static final String TAG = AudioSender.class.getCanonicalName();
    private final AudioRecorder audioRecorder;
    private boolean statusSending = false;
    private OutputStream outputStream;

    /**
     * @param outputStream  The outputStream to which the audio data is written to.
     * @param audioRecorder The audio recorder from where the audio samples are taken.
     */
    AudioSender(OutputStream outputStream, AudioRecorder audioRecorder) {
        this.outputStream = outputStream;
        this.audioRecorder = audioRecorder;
    }

    void stopSending() {
        statusSending = false;
    }

    void startSending() {
        if (statusSending) {
            stopSending();
        }
        statusSending = true;
        this.start();
    }

    @Override
    public void run() {
        try {
            while (statusSending) {
                processAudioBuffer(audioRecorder.take());
            }
        } catch (InterruptedException | IOException e) {
            if (statusSending) {// If we are not sending everything is fine
                Log.e(TAG, "We were interrupted during sending.", e);
            }
            Thread.currentThread().interrupt();
        }
        Log.d(TAG, "Stopped sending packets");
    }

    private void processAudioBuffer(AudioBuffer audioBuffer) throws IOException {
        int numReadBytes = audioBuffer.getReadBytes();
        byte[] buffer = audioBuffer.getAudioByteBuffer();
        if (numReadBytes != buffer.length) {
            outputStream.write(Arrays.copyOfRange(buffer, 0, numReadBytes));
        } else {
            outputStream.write(buffer);
        }
        outputStream.flush();
    }
}
