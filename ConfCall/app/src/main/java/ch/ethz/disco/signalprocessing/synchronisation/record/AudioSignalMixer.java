/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.signalprocessing.synchronisation.record;

import android.content.SharedPreferences;
import android.os.Process;
import android.preference.PreferenceManager;
import android.util.Log;

import org.sipdroid.sipua.ui.Settings;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.ShortBuffer;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;

import ch.ethz.disco.confcall.Receiver;
import ch.ethz.disco.networking.socket.CommunicationInterfaceCommand;
import ch.ethz.disco.networking.socket.ServerCommunicationInterface;
import ch.ethz.disco.signalprocessing.echocancellation.EchoCancellerUseAndroidEchoCanceller;
import ch.ethz.disco.signalprocessing.frameprocessors.ChannelSelector;
import ch.ethz.disco.signalprocessing.frameprocessors.relativegain.RelativeMaximumGain;
import ch.ethz.disco.signalprocessing.synchronisation.AudioSynchronizer;
import speex.EchoCancellerHelper;

/**
 * Mixes the audio data of multiple input channels to one output channel. The actual mixing strategy
 * is defined by the channelSelector.
 * <p>
 * So far this class is used to mix the different microphone input signals of the phones which
 * participate in the conference to one signal which can be sent to the UA communicating with the
 * conference.
 */
public class AudioSignalMixer implements ChannelResetter {
    private static final String TAG = AudioSignalMixer.class.getCanonicalName();
    private static final int HOST_CHANNEL_ID = 0;
    private final ChannelSelector channelSelector;
    /**
     * Buffers the shorts until they were read.
     */
    private final DataOutputStream pipedDataOutputStream;
    private final DataInputStream pipedDataInputStream;

    /*
    We use a BlockingQueue because we fill elements from the producer threads into the queue while
    the audioSignalMixer consumes the elements and waits if no were there. The Integer is the ChannelId.
     */
    private final Map<Integer, BlockingQueue<short[]>> channelAudioPartsQueue = new ConcurrentHashMap<>();
    private final int audioRecordBufferSize;
    private final ServerCommunicationInterface communicationInterface;
    private final AudioSynchronizer synchronizer;
    private final EchoCancellerHelper echoCanceller;
    private final int frameLength;
    private final boolean useAndroidEchoCancellerPropagator;
    /**
     * This counter describes which channelId's are free Note: channelId 0 is reserved for the
     * host-channel (so the channel of the master device)
     */
    private int freeChannelCounter = HOST_CHANNEL_ID + 1;
    private MixerThread mixerThread;
    private Map<Integer, ChannelFiller> channelFillerMap = new ConcurrentHashMap<>();
    private boolean isRunning = false;
    private int logCounter = 0;
    private BlockingQueue<short[]> processedShortsBuffer = new LinkedBlockingQueue<>();
    private final BufferReadHelper readHelper = new BufferReadHelper(processedShortsBuffer);

    public AudioSignalMixer(int frameLength, int sampleRate, int audioRecordBufferSize, ServerCommunicationInterface communicationInterface, AudioSynchronizer synchronizer, EchoCancellerHelper echoCanceller) {
        this.frameLength = frameLength;
        /*
        Since the change to voice_communication the maximumSignalToNoiseRatio does no more work:
        This is because the maximumSignalToNoiseRatio does not find any noise when we operate on
        VOICE_COMMUNICATION

        Therefore the new RelativeMaximumGain is activated
        */
        this.channelSelector = new RelativeMaximumGain(sampleRate, frameLength);
        this.audioRecordBufferSize = audioRecordBufferSize;
        this.communicationInterface = communicationInterface;
        this.synchronizer = synchronizer;

        PipedInputStream pipedInputStream = new PipedInputStream();
        this.pipedDataInputStream = new DataInputStream(pipedInputStream);
        DataOutputStream temp = null;
        try {
            temp = new DataOutputStream(new PipedOutputStream(pipedInputStream));
        } catch (IOException e) {
            Log.e(TAG, "Could not create pipedOutputStream", e);
        }
        pipedDataOutputStream = temp;
        this.echoCanceller = echoCanceller;

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(Receiver.mContext);
        if (sharedPref != null) {
            this.useAndroidEchoCancellerPropagator = sharedPref.getBoolean(Settings.PREF_USE_ANDROID_ECHO_CANCELER_PROPAGATOR, Settings.DEFAULT_USE_ANDROID_ECHO_CANCELER_PROPAGATOR);
        } else {
            this.useAndroidEchoCancellerPropagator = Settings.DEFAULT_USE_ANDROID_ECHO_CANCELER_PROPAGATOR;
        }
    }

    /**
     * DO NOT USE THIS CONSTRUCTOR
     */
    private AudioSignalMixer() {
        throw new UnsupportedOperationException();
    }

    /**
     * Thanks to http://www.java2s.com/Code/Java/Language-Basics/Utilityforbyteswappingofalljavadatatypes.htm
     * Byte swap a single short value.
     *
     * @param value Value to byte swap.
     * @return Byte swapped representation.
     */
    private static short swap(short value) {
        int b1 = value & 0xff;
        int b2 = (value >> 8) & 0xff;

        return (short) (b1 << 8 | b2);
    }

    public static int getHostChannelId() {
        return HOST_CHANNEL_ID;
    }

    /**
     * @param audioDataPart The audio shorts as byte array.
     * @param isBigEndian   if true takes this order. if false swaps the bytes per short
     * @return A short array containing the shorts parsed from the @param{audioDataPart}
     */
    private static short[] convertByteArrayToShortArray(byte[] audioDataPart, boolean isBigEndian) {
        if (audioDataPart.length % 2 != 0) {
            Log.e(TAG, "Expected an even length");
            return new short[0];
        }

        ByteBuffer byteBuffer = ByteBuffer.wrap(audioDataPart).order(ByteOrder.BIG_ENDIAN);
        ShortBuffer shortBuffer = byteBuffer.asShortBuffer();

        int shortLength = audioDataPart.length / 2;
        short[] shorts = new short[shortLength];
        for (int i = 0; i < shortLength; i++) {
            if (isBigEndian) {
                shorts[i] = shortBuffer.get();
            } else {
                shorts[i] = swap(shortBuffer.get());
            }
        }
        return shorts;
    }

    /**
     * @return int - The length of audioParts the AudioSignalMixer operates on in shorts.
     */
    public int getAudioDataPartLength() {
        return frameLength;
    }

    /**
     * @see OutputStream s read method. Same but for shorts.
     */
    public int read(short[] audioData, int offsetInShorts, int sizeInShorts) {
        try {
            return readHelper.readFromBuffer(audioData, offsetInShorts, sizeInShorts);
        } catch (InterruptedException e) {
            Log.e(TAG, "Got interrupted where not expected to be interrupted", e);
            Thread.currentThread().interrupt();
            return 0;
        }
    }

    private synchronized int createNewChannel(ChannelFiller channelFiller, boolean isHostChannel) {
        int channelId;
        if (isHostChannel) {
            channelId = getHostChannelId();
        } else {
            channelId = freeChannelCounter++;
        }

        channelAudioPartsQueue.put(channelId, new LinkedBlockingQueue<short[]>());
        channelFillerMap.put(channelId, channelFiller);
        Log.v(TAG, "Added channel with channelId:" + channelId);
        return channelId;
    }

    /**
     * Create a new host Channel. A call to this method makes the AudioSignalMixer wait for data on
     * this channel!
     * <p>
     * The host channel is reserved for the audio recorder of the master device.
     *
     * @return channelId - The id (0 <= id) of the new created Channel. -1 if channel name is
     * already occupied.
     */
    public synchronized int createNewHostChannel(ChannelFiller channelFiller) {
        return createNewChannel(channelFiller, true);
    }

    /**
     * Create a new Channel. A call to this method makes the AudioSignalMixer wait for data on this
     * channel!
     *
     * @return channelId - The id (0 <= id) of the new created Channel. -1 if channel name is
     * already occupied.
     */
    public synchronized int createNewChannel(ChannelFiller channelFiller) {
        return createNewChannel(channelFiller, false);
    }

    /**
     * Removes an existing channel.
     *
     * @param channelId The channelId of the channel which should be removed.
     */
    public synchronized void removeChannel(int channelId) {
        BlockingQueue<short[]> removedQueue = channelAudioPartsQueue.remove(channelId);
        if (removedQueue == null) {
            Log.e(TAG, "Try to remove a queue for a channelId which is not present. ChannelID = " + channelId);
        }
        ChannelFiller removedFiller = channelFillerMap.remove(channelId);
        if (removedFiller != null) {
            Log.v(TAG, "Removed channel with channelId:" + channelId);
        } else {
            Log.e(TAG, "You removed a channel not present in the channelFillerMap. ChannelID = " + channelId);
        }
    }

    /**
     * @param channelId     Id of the channel the audioData belongs to. Receive a new channelId by
     *                      AudioSignalMixer.createNewChannel().
     * @param audioDataPart The audio data as byte array in NativeEndian. Not null. Assumes that the
     *                      length is equal to AudioSignalMixer.getAudioDataPartLength()*2 since
     *                      passing bytes and not shorts. Note that the audioMixer does not
     *                      necessarily creates a copy of the array for performance reasons and
     *                      might change the array passed here.
     */
    public void writeNativeEndian(int channelId, byte[] audioDataPart) {
        if (audioDataPart == null) {
            Log.e(TAG, "Null audioDataPart added");
            return;
        }
        if (!isValid(channelId)) {
            Log.e(TAG, "Invalid channelId:" + channelId);
            return;
        }
        if (audioDataPart.length != getAudioDataPartLength() * 2) {
            Log.e(TAG, "Wrong size of audioDataPart:" + audioDataPart.length);
            return;
        }
        channelAudioPartsQueue.get(channelId).add(convertByteArrayToShortArray(audioDataPart, false));
        logAudioQueueLength();
    }

    private void logAudioQueueLength() {
        if (logCounter++ % 100 == 0) { // only log from time to time
            StringBuilder stringBuilder = new StringBuilder("AudioPartsQueueLengths ");
            for (Map.Entry<Integer, BlockingQueue<short[]>> channelQueue : channelAudioPartsQueue.entrySet()) {
                stringBuilder.append(channelQueue.getKey());
                stringBuilder.append(":");
                stringBuilder.append(channelQueue.getValue().size());
                stringBuilder.append("  ");
            }
            Log.v(TAG, stringBuilder.toString());
        }
    }

    /**
     * Checks if the channel id is valid, meaning the audioSignalMixer knows about this a channel.
     *
     * @param channelId The channelId to check for.
     * @return True if valid. False if invalid.
     */
    private boolean isValid(int channelId) {
        return channelAudioPartsQueue.containsKey(channelId);
    }

    /**
     * Start the AudioSignalMixer.
     */
    public void start() {
        if (isRunning) {
            Log.w(TAG, TAG + " is already running. Please stop before starting.");
        }
        isRunning = true;
        mixerThread = new MixerThread();
        mixerThread.setRunning(true);
        mixerThread.start();
    }

    /**
     * Stop the AudioSignalMixer.
     */
    public void stop() {
        isRunning = false;
        if (mixerThread != null) {
            mixerThread.setRunning(false);
            mixerThread.interrupt();
        }
        try {
            if (pipedDataInputStream != null) {
                pipedDataInputStream.close();
            }
            if (pipedDataOutputStream != null) {
                pipedDataOutputStream.close();
            }
        } catch (IOException e) {
            Log.e(TAG, "Could not close the OutputStream", e);
        }
    }

    @Override
    public void resetChannels(Set<Integer> channelIds) {
        for (Integer channelId : channelIds) {
            resetChannel(channelId);
        }
    }

    @Override
    public void resetChannel(Integer channelId) {
        Log.v(TAG, "Resetting channel with channelId = " + channelId);
        communicationInterface.sendCommandAndData(channelId, CommunicationInterfaceCommand.CONFERENCE_RESET_CONNECTION.getCode(), 0, new byte[0]);
    }

    public Map<Integer, ChannelFiller> getChannelFillerMap() {
        return channelFillerMap;
    }

    /**
     * If one wants to create a new channel and fill the channel with audio data one must implement
     * this interfaces methods.
     */
    public interface ChannelFiller {
        /**
         * The AudioSignalMixer will adjust the relative offsets in the records by forcing the
         * channelFillers to add (positive offset)/ignore (negative offset) some samples. This is
         * necessary to synchronize all records.
         *
         * @param numberOfSamples The number of samples (shorts) that should be ignored.
         */
        void correctOffset(int numberOfSamples);
    }

    class MixerThread extends Thread {
        private final FrameTakerMasterAsReference frameTaker;
        private boolean isRunning = false;

        MixerThread() {
            super();
            frameTaker = new FrameTakerMasterAsReference(channelAudioPartsQueue, audioRecordBufferSize, AudioSignalMixer.this);
        }

        @Override
        public void run() {
            Process.setThreadPriority(Process.THREAD_PRIORITY_URGENT_AUDIO);
            while (!Thread.interrupted() && isRunning()) {
                int numberOfRegisteredChannels = channelAudioPartsQueue.size();
                if (numberOfRegisteredChannels < 1) {
                    noRegisteredChannel();
                } else {
                    readFromChannels();
                }
            }
        }

        private void readFromChannels() {
            Frame currentFrame;
            try {
                currentFrame = takeFrameFromQueue();
            } catch (InterruptedException e) {
                // We expect to get interrupted when we should stop so everything is fine.
                Thread.currentThread().interrupt();
                if (isRunning()) {
                    Log.e(TAG, "Got interrupted while running", e);
                }
                return;
            }
            short[] shorts = processFrame(currentFrame);
            if (shorts == null) {
                Log.w(TAG, "Null frame received");
                return;
            }
            writeToOutputStream(shorts);
        }

        private short[] processFrame(Frame currentFrame) {
            if (synchronizer != null) {
                synchronizer.newRecordFrame(currentFrame);
            } else {
                Log.w(TAG, "No synchronizer set -> will not have synchronized audio data");
            }
            echoCanceller.processFrame(currentFrame);
            /*
             * See problems of {@link EchoCancellerUseAndroidEchoCanceller}. We do not want it to be activated since even at small distances we already had problems of muting other phones accidentally.
             */
            if (useAndroidEchoCancellerPropagator) {
                EchoCancellerUseAndroidEchoCanceller.processFrame(currentFrame);
            }
            short[] selectedSignal = channelSelector.selectFrame(currentFrame.getChannelsToDataMap());
            return selectedSignal;
        }

        private void writeToOutputStream(short[] shorts) {
            try {
                processedShortsBuffer.put(shorts);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                logInterruptedExceptionIfRunning(e);
            }
        }

        private void logInterruptedExceptionIfRunning(Exception e) {
            if (isRunning()) {
                Log.e(TAG, "Got interrupted while running", e);
            } // Else: We expect to be interrupted -> ignore this exception and exit
        }

        /**
         * If no channel was registered. Waits some time.
         */
        private void noRegisteredChannel() {
            try {
                Log.w(TAG, "No input channels on a running AudioSignalMixer");
                Thread.sleep(100);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                logInterruptedExceptionIfRunning(e);
            }
        }

        private Frame takeFrameFromQueue() throws InterruptedException {
            return frameTaker.takeNextFrame();
        }

        public boolean isRunning() {
            return isRunning;
        }

        public void setRunning(boolean running) {
            isRunning = running;
        }
    }
}
