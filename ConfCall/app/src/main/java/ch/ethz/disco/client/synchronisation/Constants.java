/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.client.synchronisation;

public class Constants {
    public static final int ADMIRED_BUFFER_CONTENT_LENGTH_MASTER = 2000;
    /**
     * Note: Not sure what this field should be to work optimal. Theoretically it should be 0, but
     * tests showed that sometimes 360 makes sense....
     * <p>
     * for Nexus6 as master, Nexus5 as client and HotSpot: 670 is optimal
     * <p>
     * for Nexus5 as master, Nexus5 as client and HotSpot: 160 is optimal
     */
    public static final int BUFFER_DIFFERENCE_MASTER_CLIENT_ZERO_LATENCY = 160;
    public static final int ADMIRED_BUFFER_CONTENT_LENGTH_CLIENT_INITIAL_VAL = ADMIRED_BUFFER_CONTENT_LENGTH_MASTER - BUFFER_DIFFERENCE_MASTER_CLIENT_ZERO_LATENCY;

    private Constants() {
    }
}
