/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.networking.wifibroadcaster.send;

import android.os.AsyncTask;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.MulticastSocket;

import ch.ethz.disco.Constants;
import ch.ethz.disco.networking.wifibroadcaster.NetworkConstants;
import ch.ethz.disco.util.NetworkUtil;

/**
 * This class is responsible to broadcast messages over the WIFI connection.
 * <p>
 * Thanks to https://stackoverflow.com/questions/17308729/send-broadcast-udp-but-not-receive-it-on-other-android-devices
 */
public class SimpleNetworkPacketSender implements NetworkPacketSender {
    private static final String TAG = SimpleNetworkPacketSender.class.getCanonicalName();

    @Override
    public void broadcastMessage(int receiverIdentifier, String message) throws IOException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        outputStream.write(receiverIdentifier);
        outputStream.write(message.getBytes(Constants.CHARSET));
        sendBroadcast(outputStream.toByteArray());
    }

    @Override
    public void broadcastMessage(int receiverIdentifier, byte[] data) throws IOException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        outputStream.write(receiverIdentifier);
        outputStream.write(data);
        sendBroadcast(outputStream.toByteArray());
    }

    private void sendBroadcast(byte[] data) {
        new SenderAsyncTask().execute(data);
    }

    private static class SenderAsyncTask extends AsyncTask<byte[], Void, Void> {
        private static final String BROADCAST_ADDRESS = "255.255.255.255";

        @Override
        protected Void doInBackground(byte[]... bytes) {
            byte[] data = bytes[0];
            ip4Broadcast(data);
            ip6Broadcast(data);
            return null;
        }

        private void ip6Broadcast(byte[] data) {
            MulticastSocket multicastSocket = null;
            try {
                InetAddress inetAddress = InetAddress.getByName(NetworkConstants.ADDRESS_IP6_MULTICAST);
                multicastSocket = new MulticastSocket(NetworkConstants.PORT_MULTICAST_IP6);
                NetworkUtil.bindToWlanNetworkInterface(multicastSocket);
                multicastSocket.joinGroup(inetAddress);
                DatagramPacket datagramPacket = new DatagramPacket(data, data.length, inetAddress, NetworkConstants.PORT_MULTICAST_IP6);
                multicastSocket.send(datagramPacket);
                multicastSocket.leaveGroup(inetAddress);
            } catch (IOException e) {
                Log.e(TAG, "Could not send IP6 MultiCast.", e);
            } finally {
                if (multicastSocket != null) {
                    multicastSocket.close();
                }
            }
        }

        private void ip4Broadcast(byte[] data) {
            DatagramSocket socket = null;
            try {
                socket = new DatagramSocket();
                socket.setBroadcast(true);
                InetAddress inetAddress = InetAddress.getByName(BROADCAST_ADDRESS);
                DatagramPacket sendPacket = new DatagramPacket(data, data.length, inetAddress, NetworkConstants.PORT_BROADCAST_IP4);
                socket.send(sendPacket);
            } catch (IOException e) {
                Log.e(TAG, "Could not send ip4 broadcast.", e);
            } finally {
                if (socket != null) {
                    socket.close();
                }
            }
        }
    }
}
