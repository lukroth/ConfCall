/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.server;

import android.content.Context;
import android.media.AudioRecord;
import android.preference.PreferenceManager;
import android.util.Log;

import org.sipdroid.sipua.ui.Settings;

import ch.ethz.disco.client.ClientConference;
import ch.ethz.disco.confcall.Receiver;
import ch.ethz.disco.networking.socket.CommunicationInterfaceCommand;
import ch.ethz.disco.networking.socket.ServerCommunicationInterface;
import ch.ethz.disco.signalprocessing.synchronisation.AudioSynchronizer;
import ch.ethz.disco.signalprocessing.synchronisation.GCCSynchronizer;
import ch.ethz.disco.signalprocessing.synchronisation.record.AudioSignalMixer;
import speex.EchoCancellerHelper;

/**
 * Main class to maintain a conference Call from the servers/masters view.
 */
public class ServerConference {
    /**
     * The record stream gets partitioned. This defines the frame length of this partitions.
     * <p>
     * This field has a high influence on how much delay gets introduced. The higher the number the
     * more delay.
     * <p>
     * Note: Preferably a power of two since then FFT which is done on this data junks works
     * faster.
     */
    private static final int AUDIO_RECORD_FRAME_LENGTH = 1024;
    private static final String TAG = ServerConference.class.getCanonicalName();
    private static final int START_CONF_MAX_RETRIES = 50;
    private static final int START_CONF_RETRY_TIMEOUT_MS = 200;

    private final AudioSignalMixer audioSignalMixer;
    private final ServerCommunicationInterface serverCommunicationInterface;
    private final OpenConferenceInformer openConferenceInformer;
    private final int sampleRate;

    private final AudioSynchronizer synchronizer;
    private final EchoCancellerHelper echoCancellerHelper;
    /**
     * True if the conference is running. False otherwise.
     */
    private boolean isRunning = false;
    /**
     * Handles everything about recording the audio signal on this and the connected devices.
     * <p>
     * Null means uninitialized.
     */
    private RecordConference recordConference;
    /**
     * Is responsible for the audio playback on this and the other devices.
     * <p>
     * Null means uninitialized.
     */
    private PlaybackConference playbackConference;
    private Context context;


    /**
     * Creates a new Conference
     */
    public ServerConference(Context context, int sampleRate) {
        this.context = context;
        this.sampleRate = sampleRate;
        /*
         * Describes how many samples +- we collect until we do GCC on this part of data. Also defines
         * the maxQueueSize.
         */
        int audioRecordSynchronisationLengthInSamples = sampleRate;
        /*
         * Describes on how many merged frames we do GCC. Should be larger than {@code
         * audioRecordBufferSize}!
         */
        int audioRecordSynchronisationLengthInFrames = audioRecordSynchronisationLengthInSamples / AUDIO_RECORD_FRAME_LENGTH;
        /*
        we cannot find the offset over the whole synchronisation window so
        we allow only for a smaller number inside the queue such that we can for sure compensate
        offsets in this range.

         * The audioRecordBufferSize describes how long the queue should get at most. If one Channel is
         * no more sending then the AudioSignalMixer waits until the audioRecordBufferSize is reached
         * and then ignores this channel until he sends again.
         * <p>
         * If one signal is behind the other this adds a delay to the recordings if it is big. (We would
         * wait long for the other signal)
         * <p>
         * Note: The audioRecordBufferSize has to be in relation to the audioRecordSynchronisationLengthInFrames
         */
        int audioRecordBufferSize = audioRecordSynchronisationLengthInFrames / 2;

        this.openConferenceInformer = new OpenConferenceInformer();
        this.serverCommunicationInterface = new ServerCommunicationInterface();
        this.echoCancellerHelper = new EchoCancellerHelper(sampleRate, AUDIO_RECORD_FRAME_LENGTH);
        this.synchronizer = new GCCSynchronizer(audioRecordSynchronisationLengthInFrames, serverCommunicationInterface, echoCancellerHelper);
        this.audioSignalMixer = new AudioSignalMixer(AUDIO_RECORD_FRAME_LENGTH, sampleRate, audioRecordBufferSize, serverCommunicationInterface, synchronizer, echoCancellerHelper);
        this.serverCommunicationInterface.setAudioSignalMixer(audioSignalMixer);
        synchronizer.setAudioSignalMixer(audioSignalMixer);
    }

    /**
     * Starts a server conference. You can only start the conference if the conference is not
     * running already.
     */
    public void start() {
        if (isRunning) {
            Log.e(TAG, "Conference is already running. Ignoring the start() call");
            return;
        }
        isRunning = true;
        Log.d(TAG, "Start Conference Application");
        stopClientConferencesRunningOnThisPhone();
        if (!waitForRecordConferenceInitialisation()) {
            return;
        }
        openConferenceInformer.startOfferingNewConference(getRecordConference().getSampleRate(), getConferenceName());
        getRecordConference().start();
    }

    private void stopClientConferencesRunningOnThisPhone() {
        ClientConference.setIsServerConference(true);
        ClientConference.stopRunningClientConferences();
    }

    /**
     * Waits some time to give the conference time to initialize.
     * <p>
     * Returns true if succeeded, false otherwise.
     */
    private boolean waitForRecordConferenceInitialisation() {
        int startRetryCounter = 0;
        while (getRecordConference() == null) {
            Log.v(TAG, "No RecordConference initialized - Waiting " + START_CONF_RETRY_TIMEOUT_MS + " ms");
            startRetryCounter++;
            if (startRetryCounter > START_CONF_MAX_RETRIES) {
                Log.e(TAG, "Could not start conference. No RecordConference initialized");
                return false;
            }
            try {
                Thread.sleep(startRetryCounter);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                return false;
            }
        }
        return true;
    }

    /**
     * Stops the conference application
     */
    public void stop() {
        if (!isRunning) {
            Log.e(TAG, "Conference is not running. Ignoring the stop() call");
            return;
        }
        Log.d(TAG, "Stop Conference Application");
        openConferenceInformer.stopOfferingConference();
        serverCommunicationInterface.broadcastCommandAndData(CommunicationInterfaceCommand.CONFERENCE_STOP.getCode(), 0, new byte[0]);
        if (getRecordConference() != null) {
            getRecordConference().stop();
        }

        ClientConference.setIsServerConference(false);
        isRunning = false;
    }

    /**
     * Initializes the recordConference.
     *
     * @param record     The audio record from which the RecordConference should read from.
     * @param sampleRate The sampleRate at which the RecordConference (and the record) operates.
     */
    public void initRecordConference(AudioRecord record, int sampleRate) {
        Log.v(TAG, "Initialising the record conference");
        checkCorrectSampleRate(sampleRate);
        if (!recordConferenceIsInitialized()) {
            this.recordConference = new RecordConference(record, this.sampleRate, serverCommunicationInterface, audioSignalMixer, this);
            this.synchronizer.setRecordConference(recordConference);
        } else {
            Log.e(TAG, "Second initialisation of RecordConference. Only initialise the record conference once per conference. Will not initialize again.");
        }
    }

    /**
     * Checks the passed sample rate against the sample rate the conference operates on. If not
     * equal report an error.
     *
     * @param sampleRate The sample rate that should be compared.
     */
    private void checkCorrectSampleRate(int sampleRate) {
        if (sampleRate != this.sampleRate) {
            Log.e(TAG, "Expected the sampleRate to be " + this.sampleRate + ", but was " + sampleRate);
        }
    }

    /**
     * Same parameters as for the {@code AudioTrack} constructor method, see javadoc from there.
     */
    public void initPlaybackConference(int sampleRateInHz, int bufferSizeInBytes) {
        Log.v(TAG, "Initialising the playback conference");
        checkCorrectSampleRate(sampleRateInHz);
        if (!playbackConferenceIsInitialized()) {
            this.playbackConference = new PlaybackConference(context, this.sampleRate, bufferSizeInBytes, serverCommunicationInterface, echoCancellerHelper);
            this.synchronizer.setPlaybackConference(playbackConference);
        } else {
            Log.e(TAG, "Second initialisation of PlaybackConference. Only initialise once per conference. Will not initialize again.");
        }
    }

    public RecordConference getRecordConference() {
        return recordConference;
    }

    public PlaybackConference getPlaybackConference() {
        return playbackConference;
    }


    /**
     * Checks if the PlaybackConference is initialized.
     *
     * @return True if initialized. False otherwise.
     */
    private boolean playbackConferenceIsInitialized() {
        return playbackConference != null;
    }

    /**
     * Checks if the RecordConference is initialized.
     *
     * @return True if initialized. False otherwise.
     */
    public boolean recordConferenceIsInitialized() {
        return recordConference != null;
    }

    private String getConferenceName() {
        return PreferenceManager.getDefaultSharedPreferences(Receiver.mContext).getString(Settings.PREF_CONFERENCE_INITIATOR_CONF_NAME, Settings.DEFAULT_CONFERENCE_INITIATOR_CONF_NAME);
    }
}
