/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.networking.wifibroadcaster;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import ch.ethz.disco.networking.wifibroadcaster.send.SimpleNetworkPacketSender;

/**
 * Broadcasts messages to the network to inform clients about an open conference.
 */
public class WifiConferenceOfferMessageBroadcaster {
    private static final String TAG = WifiConferenceOfferMessageBroadcaster.class.getCanonicalName();
    /**
     * At which interval the start message is sent.
     */
    private static final long PERIODIC_DELAY_MS = 1000;
    private final SimpleNetworkPacketSender networkPacketSender;
    /**
     * True if broadcasting information about the current conference.
     */
    private boolean isSending = false;
    private Timer startConfSenderTaskTimer = new Timer();

    public WifiConferenceOfferMessageBroadcaster() {
        this.networkPacketSender = new SimpleNetworkPacketSender();
    }

    /**
     * This method should be called when a new conference call starts and the other phones should be
     * informed about it.
     */
    public void startConference(int sampleRate, String conferenceName) {
        if (isSending) {
            Log.e(TAG, "Already running");
            return;
        }
        isSending = true;

        StartConfSenderTask startConfSenderTask = new StartConfSenderTask(sampleRate, conferenceName);
        startConfSenderTaskTimer.schedule(startConfSenderTask, 0, PERIODIC_DELAY_MS);
    }

    /**
     * Sends stop message to the other phones and stops sending start messages.
     */
    public void stopConference() {
        if (isSending) {
            startConfSenderTaskTimer.cancel();
            startConfSenderTaskTimer = new Timer();
        } else {
            Log.w(TAG, "Stopped a non-running conference broadcast. You most likely do not want to do that.");
        }
        isSending = false;
    }

    public static class Messages {
        public static final String MESSAGE_TYPE = "TYPE";
        public static final String MESSAGE_TYPE_START = "START";
        public static final String MESSAGE_SAMPLE_RATE = "SAMPLE_RATE";
        public static final String MESSAGE_PORT = "PORT_COMMUNICATION_INTERFACE";
        public static final String MESSAGE_CONFERENCE_NAME = "CONFERENCE_NAME";
        private static final String TAG = Messages.class.getCanonicalName();

        public static String getStartMessage(int sampleRate, String conferenceName) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put(MESSAGE_TYPE, MESSAGE_TYPE_START);
                jsonObject.put(MESSAGE_SAMPLE_RATE, sampleRate);
                jsonObject.put(MESSAGE_PORT, NetworkConstants.PORT_COMMUNICATION_INTERFACE);
                jsonObject.put(MESSAGE_CONFERENCE_NAME, conferenceName);
            } catch (JSONException e) {
                Log.e(TAG, "Could not create json object", e);
            }
            return jsonObject.toString();
        }
    }

    /**
     * Task that periodically broadcasts a start packet over the wifi network.
     */
    private class StartConfSenderTask extends TimerTask {

        private final String conferenceName;
        private int sampleRate;

        StartConfSenderTask(int sampleRate, String conferenceName) {
            this.sampleRate = sampleRate;
            this.conferenceName = conferenceName;
        }

        @Override
        public void run() {
            try {
                sendMessage(WifiConferenceOfferMessageBroadcaster.Messages.getStartMessage(sampleRate, conferenceName));
            } catch (IOException e) {
                Log.e(TAG, "Could not broadcast start message to the other mobile phones", e);
            }
        }

        private void sendMessage(String message) throws IOException {
            networkPacketSender.broadcastMessage(ReceiverIdentifierConstants.MESSAGE_BROADCASTER, message);
        }
    }
}
