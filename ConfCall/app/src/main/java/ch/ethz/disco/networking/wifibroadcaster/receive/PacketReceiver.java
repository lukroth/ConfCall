/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.networking.wifibroadcaster.receive;

import android.app.Activity;
import android.content.Context;
import android.net.wifi.WifiManager;
import android.os.PowerManager;
import android.util.Log;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.SocketTimeoutException;
import java.util.HashSet;
import java.util.Set;

import ch.ethz.disco.networking.wifibroadcaster.NetworkConstants;
import ch.ethz.disco.util.NetworkUtil;

/**
 * Receives broadcast packets that arrive over the wifi network. Broadcasts them to the listeners
 * that register at this class.
 * <p>
 * Thanks to https://stackoverflow.com/questions/17308729/send-broadcast-udp-but-not-receive-it-on-other-android-devices
 */
public class PacketReceiver implements NetworkMessageBroadcaster {
    private static final int SO_TIMEOUT = 1000;
    private static final String TAG = PacketReceiver.class.getCanonicalName();
    private static final String LOG_TAG = "SmartConferenceLocks";
    private final Activity activity;
    private boolean receivePackets = false;
    private Ip4PacketReceiver ip4PacketReceiver;
    private Ip6PacketReceiver ip6PacketReceiver;
    private Set<NetworkMessageReceiver> networkMessageReceivers = new HashSet<>();
    private WifiManager.MulticastLock wifiMulticastLock;
    private WifiManager.WifiLock wifiLock;
    private PowerManager.WakeLock wakeLock;

    public PacketReceiver(Activity activity) {
        this.activity = activity;
    }

    /**
     * Starts to listen for packets.
     */
    public synchronized void startListeningForPackets() {
        if (receivePackets) {
            stopListeningForPackets();
        }
        receivePackets = true;
        acquireWifiLocks();
        ip4PacketReceiver = new Ip4PacketReceiver();
        ip4PacketReceiver.start();
        ip6PacketReceiver = new Ip6PacketReceiver();
        ip6PacketReceiver.start();
    }

    /**
     * Stops listening for packets.
     */
    public synchronized void stopListeningForPackets() {
        releaseWifiLocks();
        if (receivePackets) {
            receivePackets = false;
            ip4PacketReceiver.interrupt();
            ip6PacketReceiver.interrupt();
        } else {
            Log.w(TAG, "You stop a non-running PacketReceiver");
        }
    }

    /**
     * Necessary to surely receive all broadcast packets. Otherwise android does not forward them
     * for energy consumption reasons.
     */
    private void acquireWifiLocks() {
        WifiManager wifi = (WifiManager) activity.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        if (wifi == null) {
            Log.e(TAG, "Could not acquire the wifi locks. WifiManager is null");
            return;
        }
        wifiLock = wifi.createWifiLock(LOG_TAG);
        wifiLock.acquire();

        wifiMulticastLock = wifi.createMulticastLock(LOG_TAG);
        wifiMulticastLock.acquire();

        PowerManager powerManager = (PowerManager) activity.getSystemService(Context.POWER_SERVICE);
        if (powerManager == null) {
            Log.e(LOG_TAG, "Could not acquire the PowerManager locks. PowerManager is null");
            return;
        }
        wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, LOG_TAG);
        wakeLock.acquire();
    }

    /**
     * Releases the locks taken by acquireWifiLock
     */
    private void releaseWifiLocks() {
        if (wifiLock != null) {
            wifiLock.release();
            wifiLock = null;
        }
        if (wifiMulticastLock != null) {
            wifiMulticastLock.release();
            wifiMulticastLock = null;
        }
        if (wakeLock != null) {
            wakeLock.release();
        }
    }

    @Override
    public void registerNetworkMessageReceiver(NetworkMessageReceiver networkMessageReceiver) {
        networkMessageReceivers.add(networkMessageReceiver);
    }

    @Override
    public void deregisterNetworkMessageReceiver(NetworkMessageReceiver networkMessageReceiver) {
        networkMessageReceivers.remove(networkMessageReceiver);
    }

    /**
     * Broadcasts the packet to the registered listeners.
     *
     * @param packet The packet that should be broadcast
     */
    private void broadcastPacket(DatagramPacket packet) {
        Message message = new Message(packet);
        // go over all the registered message receiver applications and forward the message
        for (NetworkMessageReceiver networkMessageReceiver : networkMessageReceivers) {
            if (networkMessageReceiver.getReceiverIdentifier() == message.getMessageReceiverIdentifier()) {
                networkMessageReceiver.handleMessage(message);
            }
        }
    }

    class Ip4PacketReceiver extends Thread {
        private DatagramSocket socket;

        @Override
        public void run() {
            try {
                //Keep a socket open to listen to all the UDP traffic that is destined for this port
                socket = new DatagramSocket(NetworkConstants.PORT_BROADCAST_IP4, InetAddress.getByName(NetworkConstants.ADDRESS_IP4_BROADCAST));
                socket.setBroadcast(true);
                socket.setSoTimeout(SO_TIMEOUT);
                Log.v(TAG, "Start listening on port:" + NetworkConstants.PORT_BROADCAST_IP4);
                while (!this.isInterrupted() && receivePackets) {
                    receivePacket();
                }
            } catch (IOException e) {
                Log.e(TAG, "Error while receiving packets", e);
            } finally {
                if (socket != null) {
                    socket.close();
                    Log.d(TAG, "Closed the socket");
                }
            }
        }

        void receivePacket() throws IOException {
            byte[] receiveBuffer = new byte[15000];
            final DatagramPacket packet = new DatagramPacket(receiveBuffer, receiveBuffer.length);
            try {
                socket.receive(packet);
                //Packet received
                broadcastPacket(packet);
                String data = new String(packet.getData()).trim();
                Log.v(TAG, "IP4 Packet data: " + data);
            } catch (SocketTimeoutException e) {
                // We do nothing since we expect to have reached the SO timeout, so we
                // will check in the for loop if we were not interrupted
            }
        }
    }

    class Ip6PacketReceiver extends Thread {
        private MulticastSocket multicastSocket = null;
        private InetAddress inetAddress = null;

        @Override
        public void run() {
            try {
                inetAddress = InetAddress.getByName(NetworkConstants.ADDRESS_IP6_MULTICAST);
                multicastSocket = new MulticastSocket(NetworkConstants.PORT_MULTICAST_IP6);
                NetworkUtil.bindToWlanNetworkInterface(multicastSocket);
                multicastSocket.setSoTimeout(SO_TIMEOUT);
                multicastSocket.joinGroup(inetAddress);
                Log.v(TAG, "IP6 Start listening on port:" + NetworkConstants.PORT_MULTICAST_IP6 + ", Address = " + NetworkConstants.ADDRESS_IP6_MULTICAST);
                while (!this.isInterrupted() && receivePackets) {
                    receivePacket();
                }
                multicastSocket.leaveGroup(inetAddress);
            } catch (IOException e) {
                Log.e(TAG, "Could not receive packet", e);
            } finally {
                if (multicastSocket != null) {
                    multicastSocket.close();
                }
            }
        }

        private void receivePacket() throws IOException {
            byte[] buffer = new byte[65535];
            DatagramPacket packet = new DatagramPacket(buffer, buffer.length, inetAddress, NetworkConstants.PORT_MULTICAST_IP6);
            try {
                multicastSocket.receive(packet);
                broadcastPacket(packet);
                String data = new String(packet.getData()).trim();
                Log.v(TAG, "IP6 Packet data: " + data);
            } catch (SocketTimeoutException e) {
                // We do nothing since we expect to have reached the SO timeout, so we
                // will check in the for loop if we were not interrupted
            }
        }
    }
}
