/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.signalprocessing.frameprocessors;

import android.util.Log;

import java.util.Map;

import ch.ethz.disco.signalprocessing.frameprocessors.filter.MostSeenSlidingWindowFilter;
import ch.ethz.disco.util.FileWriter;

/**
 * ChannelSelector that takes the frame with the maximum Gain
 */
public class MaximumGain implements ChannelSelector {
    private static final String TAG = MaximumGain.class.getCanonicalName();
    private static final int FRAME_FILTER_LENGTH_IN_MS = 333;

    private final MostSeenSlidingWindowFilter filter;
    private final FileWriter fileWriter;
    private final int frameLength;
    private int oldActive = -1;
    private int fileWriterCounter;

    public MaximumGain(int sampleRatePerSec, int frameLength) {
        this.frameLength = frameLength;
        filter = new MostSeenSlidingWindowFilter((int) calculateFilterLength(sampleRatePerSec));
        fileWriter = new FileWriter();
    }

    /**
     * Calculates the filter length such that it will have a real length of
     *
     * @param sampleRatePerSec Sample rate of the audio data
     * @return The filter length that must be passed to the filter {@link
     * this#FRAME_FILTER_LENGTH_IN_MS}
     */
    private double calculateFilterLength(int sampleRatePerSec) {
        return ((double) (FRAME_FILTER_LENGTH_IN_MS * sampleRatePerSec)) / (1000 * frameLength);
    }

    @Override
    public short[] selectFrame(Map<Integer, short[]> frame) {
        if (FrameValidityChecker.isInvalidFrame(frame)) {
            return new short[0];
        }
        return currentActiveFrame(frame);
    }

    private short[] currentActiveFrame(Map<Integer, short[]> frame) {
        int currentMax = findMaximumGainIndex(frame);
        int currentActive = filter.newValue(currentMax);
        logChanges(currentActive);
        return frame.get(currentActive);
    }

    private void logChanges(int currentActive) {
        if (oldActive != currentActive) {
            Log.v(TAG, "Changed active microphone to " + currentActive);
            oldActive = currentActive;
        }
    }

    private int findMaximumGainIndex(Map<Integer, short[]> frame) {
        long maxAvg = -1;
        int maxIndex = -1;
        for (Integer channelId : frame.keySet()) {
            long currentAvg = calcAvgGain(frame.get(channelId));
            fileWriter.write(channelId + ";" + currentAvg + "\n");
            if (currentAvg > maxAvg) {
                maxAvg = currentAvg;
                maxIndex = channelId;
            }
        }
        if (fileWriterCounter++ % 5 == 0) {
            fileWriter.flushToFile();
        }
        if (maxAvg == -1 || maxIndex == -1) {
            Log.e(TAG, "could not find maximum inside data. Returning the first frame");
            return 0;
        }
        return maxIndex;
    }

    private long calcAvgGain(short[] shorts) {
        long gainSum = 0;
        for (short s : shorts) {
            gainSum += Math.abs(s);
        }
        return gainSum / shorts.length;
    }
}
