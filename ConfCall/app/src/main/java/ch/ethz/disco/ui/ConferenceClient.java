/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.ui;

import android.app.Activity;
import android.os.Bundle;
import android.os.PowerManager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import ch.ethz.disco.audio.MuteController;
import ch.ethz.disco.client.ClientConference;
import ch.ethz.disco.client.ClientConferenceEventListener;
import ch.ethz.disco.confcall.R;
import ch.ethz.disco.confcall.Sipdroid;

public class ConferenceClient extends Activity implements ClientConferenceEventListener {
    public static final String INTENT_START_CONF_NAME = "CONFERENCE_NAME";
    private static final String TAG = ConferenceClient.class.getName();
    private ClientConference clientConference;
    private String conferenceName;
    private ImageView muteViewPlayback;
    private ImageView muteViewRecord;
    private boolean isPlaybackMute = false;
    private boolean isMicMute = false;
    private MuteController playbackMuteController = null;
    private PowerManager.WakeLock screenWakeLock;
    private MuteController recordMuteController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conference_client);

        this.conferenceName = getIntent().getStringExtra(INTENT_START_CONF_NAME);
        initUIElements();

        this.clientConference = Sipdroid.argumentToIntentClientConference;
        if (this.clientConference == null) {
            Log.e(TAG, "Could not get Client Conference.");
        } else {
            this.clientConference.registerEventListener(this);
        }
    }

    private void initUIElements() {
        setTitle(getString(R.string.joined_conference));
        setConferenceName();
        initMuteListeners();
    }

    private void initMuteListeners() {
        muteViewPlayback = findViewById(R.id.conference_mute);
        muteViewPlayback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                muteUnMuteClickedPlayback();
            }
        });
        muteViewRecord = findViewById(R.id.conference_mute_mic);
        muteViewRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                muteUnMuteClickedMic();
            }
        });
    }

    private void muteUnMuteClickedPlayback() {
        if (isPlaybackMute) {
            unMutePlayback();
        } else {
            mutePlayback();
        }
        isPlaybackMute = !isPlaybackMute;
    }

    private void muteUnMuteClickedMic() {
        if (isMicMute) {
            unMuteMic();
        } else {
            muteMic();
        }
        isMicMute = !isMicMute;
    }

    private void muteMic() {
        muteViewRecord.setImageResource(R.drawable.mic_off);
        MuteController muteController = getRecordMuteController();
        if (muteController != null) {
            muteController.setMute(true);
            Log.v(TAG, "muted record");
        } else {
            Log.v(TAG, "Could not set record to mute since MuteController is null");
        }
    }


    private void mutePlayback() {
        muteViewPlayback.setImageResource(R.drawable.volume_off);
        MuteController controller = getPlaybackMuteController();
        if (controller != null) {
            controller.setMute(true);
        }
    }


    private void unMuteMic() {
        muteViewRecord.setImageResource(R.drawable.mic_on);
        MuteController controller = getRecordMuteController();
        if (controller != null) {
            controller.setMute(false);
        }
    }

    private void unMutePlayback() {
        muteViewPlayback.setImageResource(R.drawable.volume_on);
        MuteController mc = getPlaybackMuteController();
        if (mc != null) {
            mc.setMute(false);
        }
    }

    private void setConferenceName() {
        TextView conferenceNameTextView = findViewById(R.id.conferenceName);
        conferenceNameTextView.setText(conferenceName);
    }

    @Override
    protected void onResume() {
        super.onResume();
        takeScreenWakeLock();
    }

    private void takeScreenWakeLock() {
        PowerManager pm = (PowerManager) getSystemService(POWER_SERVICE);
        releaseScreenWakeLockIfHolding();
        if (pm != null) {
            screenWakeLock = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, getResources().getString(R.string.app_name));
            if (screenWakeLock != null) {
                screenWakeLock.acquire();
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        releaseScreenWakeLockIfHolding();
    }

    private synchronized void releaseScreenWakeLockIfHolding() {
        if (screenWakeLock != null) {
            screenWakeLock.release();
            screenWakeLock = null;
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (clientConference != null) {
            clientConference.stopRunningClientConference();
        }
    }

    public void stop() {
        finish();
    }

    @Override
    public void onConferenceStop() {
        this.stop();
        this.finish();
    }

    /**
     * @return The playbackMuteController or null if no one is available.
     */
    public MuteController getPlaybackMuteController() {
        if (playbackMuteController != null) {
            return playbackMuteController;
        } else {
            playbackMuteController = clientConference.getNewPlaybackMuteController();
            return playbackMuteController;
        }
    }

    private MuteController getRecordMuteController() {
        if (recordMuteController != null) {
            return recordMuteController;
        } else {
            recordMuteController = clientConference.getNewRecordMuteController();
            return recordMuteController;
        }
    }
}
