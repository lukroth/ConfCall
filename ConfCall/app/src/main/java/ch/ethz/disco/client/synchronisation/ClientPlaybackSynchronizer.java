/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.client.synchronisation;

import android.util.Log;

import ch.ethz.disco.audio.MuteController;
import ch.ethz.disco.client.RunningClientConferenceEventListener;
import ch.ethz.disco.confcall.R;
import ch.ethz.disco.playback.SynchronisationCapableAudioPlayer;

import static ch.ethz.disco.client.synchronisation.Constants.ADMIRED_BUFFER_CONTENT_LENGTH_MASTER;
import static ch.ethz.disco.client.synchronisation.Constants.BUFFER_DIFFERENCE_MASTER_CLIENT_ZERO_LATENCY;

/**
 * Synchronizes the playback of any client device to the playback of the master device by keeping
 * the buffer length constant.
 */
public class ClientPlaybackSynchronizer implements LatencyMeasureTask.LatencyUpdateListener {
    private static final String TAG = ClientPlaybackSynchronizer.class.getCanonicalName();
    private static final long SEC_NANOSEC_FACTOR = 1000000000;
    protected final int sampleRate;
    private final String hostAddress;
    private final SynchronisationCapableAudioPlayer audioPlayer;
    private final RunningClientConferenceEventListener runningClientConferenceEventListener;
    private final MuteController audioPlayerMuteController;
    private boolean isRunning = false;
    private LatencyMeasureTask latencyMeasureTask;

    public ClientPlaybackSynchronizer(SynchronisationCapableAudioPlayer audioPlayer, int sampleRate, String hostAddress, RunningClientConferenceEventListener runningClientConferenceEventListener) {
        this.audioPlayer = audioPlayer;
        this.runningClientConferenceEventListener = runningClientConferenceEventListener;
        this.audioPlayerMuteController = new MuteController();
        this.sampleRate = sampleRate;
        this.hostAddress = hostAddress;
    }

    public void stop() {
        if (!isRunning) {
            Log.w(TAG, "Cannot stop. Not running");
        }
        if (audioPlayer != null) {
            this.audioPlayer.deregisterMuteController(audioPlayerMuteController);
        }
        if (latencyMeasureTask != null) {
            latencyMeasureTask.stopMeasuring();
        }
    }

    public void start() {
        if (isRunning) {
            Log.e(TAG, "Cannot start, already running");
            return;
        }
        this.audioPlayer.registerMuteController(audioPlayerMuteController);
        latencyMeasureTask = new LatencyMeasureTask(hostAddress, this);
        latencyMeasureTask.start();
    }

    @Override
    public void newLatencyValue(double latencyInNanoSec) {
        int latencyInSamples = (int) calcLatencyInSamples(latencyInNanoSec);
        int admiredBufferContentLength = ADMIRED_BUFFER_CONTENT_LENGTH_MASTER - BUFFER_DIFFERENCE_MASTER_CLIENT_ZERO_LATENCY - latencyInSamples;
        boolean couldBeSet = audioPlayer.setAdmiredBufferContentLength(admiredBufferContentLength);
        if (!couldBeSet) {
            // We know that we have a to big delay between this device and the master device
            boolean oldVal = audioPlayerMuteController.isMute();
            if (!oldVal) {
                // We have newly muted
                audioPlayerMuteController.setMute(true);
                runningClientConferenceEventListener.onErrorMsgForEndUser(R.string.conference_mute_playback_big_latency);
            }
        } else {
            // We will un-mute as soon as the delay gets smaller again.
            audioPlayerMuteController.setMute(false);
        }
    }

    private double calcLatencyInSamples(double latencyInNanoSec) {
        return latencyInNanoSec / SEC_NANOSEC_FACTOR * sampleRate;
    }
}
