/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.util;

import android.media.AudioRecord;
import android.media.audiofx.AcousticEchoCanceler;
import android.os.Build;
import android.util.Log;

public class AudioRecordUtil {
    private static final String TAG = AudioRecordUtil.class.getName();

    public static void disableAndroidEchoCanceler(AudioRecord record) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            if (AcousticEchoCanceler.isAvailable()) {
                Log.v(TAG, "Removing the AcousticEchoCanceler");
                AcousticEchoCanceler aec = AcousticEchoCanceler.create(record.getAudioSessionId());
                if (aec != null && aec.getEnabled()) {
                    int returnCode = aec.setEnabled(false);
                    Log.v(TAG, "SetEnabled false on the AcousticEchoCanceler -> returnCode:" + returnCode);
                    if (returnCode != AcousticEchoCanceler.SUCCESS) {
                        Log.e(TAG, "Could not disable the AcousticEchoCanceler");
                    } else {
                        Log.e(TAG, "Removed the AcousticEchoCanceler");
                    }
                }
            }
        } else {
            Log.e(TAG, "Could not disable the EchoCanceller");
        }
    }
}
