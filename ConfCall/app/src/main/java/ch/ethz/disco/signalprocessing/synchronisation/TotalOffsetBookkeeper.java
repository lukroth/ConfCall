/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.signalprocessing.synchronisation;

import android.util.Log;

import java.util.HashMap;
import java.util.Map;

import ch.ethz.disco.networking.socket.ServerCommunicationInterface;

/**
 * Keeps track of the offset of all record signals relative to the reference signal.
 */
public class TotalOffsetBookkeeper {
    private static final String TAG = TotalOffsetBookkeeper.class.getCanonicalName();
    private final ServerCommunicationInterface serverCommunicationInterface;
    /**
     * Maps all the channelIds to their offset value.
     */
    private Map<Integer, Integer> channelsToOffsets = new HashMap<>();


    /**
     * Maps all the channelIds to their initial offset value. Means when a channel sends data for
     * the first time and the channelMixer reads this data the data the reference channel already
     * read is the offset.
     */
    private Map<Integer, Integer> channelsToInitialOffsets = new HashMap<>();

    public TotalOffsetBookkeeper(ServerCommunicationInterface serverCommunicationInterface) {
        this.serverCommunicationInterface = serverCommunicationInterface;
    }

    public Map<Integer, Integer> getChannelsToOffsets() {
        return channelsToOffsets;
    }

    /**
     * Adjusts the offset values by adding the new Offsets to the old sum.
     *
     * @param channelsToOffsets
     */
    public void newOffsets(Map<Integer, Integer> channelsToOffsets) {
        for (Map.Entry<Integer, Integer> entry : channelsToOffsets.entrySet()) {
            int channelId = entry.getKey();
            int offset = entry.getValue();
            onNewOffset(channelId, offset);
        }
        logTotalOffsets();
    }

    private void logTotalOffsets() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Total Offsets:");
        for (Map.Entry<Integer, Integer> entry : channelsToOffsets.entrySet()) {
            stringBuilder.append(entry.getKey());
            stringBuilder.append(":");
            stringBuilder.append(entry.getValue());
            stringBuilder.append(" ");
        }
        Log.v(TAG, stringBuilder.toString());
    }


    private void onNewOffset(int channelId, int offset) {
        createChannelIfNotExisting(channelId);
        int oldOffset = channelsToOffsets.get(channelId);
        channelsToOffsets.put(channelId, oldOffset + offset);
    }

    private void createChannelIfNotExisting(int channelId) {
        if (!channelsToOffsets.containsKey(channelId)) {
            channelsToOffsets.put(channelId, 0);
        }
    }


    /**
     * Should be called for all the channels once when it is know what the start-offset relative to
     * the master channel is.
     *
     * @param channelId The channel id for which the offset is
     * @param offset    The offset. e.g. 300 means that this channel started 300 samples after the
     *                  reference channel.
     */
    public void newInitialRecordOffset(Integer channelId, int offset) {
        if (channelsToInitialOffsets.containsKey(channelId)) {
            Log.e(TAG, "Should only set the initial offset once! Will ignore this call.");
            return;
        }
        Log.v(TAG, "New initial offset of channelId=" + channelId + ", offset=" + offset);
        channelsToInitialOffsets.put(channelId, offset);
    }
}
