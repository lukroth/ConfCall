/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.signalprocessing.frameprocessors.relativegain;

import ch.ethz.disco.signalprocessing.frameprocessors.filter.MaximumSlidingWindowFilter;
import ch.ethz.disco.signalprocessing.frameprocessors.filter.SumSlidingWindowFilter;

/**
 * Keeps track of the noise level of a signal.
 */
class MaximumMeasurementsBookkeeperPerSignal {
    private final SumSlidingWindowFilter sumSlidingWindowFilter;
    /*
    The minimum sliding window filter here is used as a Maximum sliding window filter by just inverting the values.
     */
    private final MaximumSlidingWindowFilter maximumSlidingWindowFilter;

    MaximumMeasurementsBookkeeperPerSignal(int silenceSlidingWindowSize, int forgetThreshold) {
        /*
        We start with a very high value since then we assume a very noisy channel in the beginning
        until we actually have some data.

        We have to divide it by silenceSlidingWindowSize because otherwise we would create an
        overflow in the filter.
         */
        double initialValue = 1;
        sumSlidingWindowFilter = new SumSlidingWindowFilter(silenceSlidingWindowSize, initialValue);
        maximumSlidingWindowFilter = new MaximumSlidingWindowFilter(forgetThreshold, initialValue);
    }

    void onNewGain(double newGain) {
        sumSlidingWindowFilter.onNewElement(newGain);
        double noiseGainOfCurrentWindow = sumSlidingWindowFilter.getAverage();
        maximumSlidingWindowFilter.onNewElement(noiseGainOfCurrentWindow);
    }

    double getMaxSeenGain() {
        return maximumSlidingWindowFilter.getMaximum();
    }
}
