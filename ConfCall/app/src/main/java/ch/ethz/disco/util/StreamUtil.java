/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.util;

import java.io.IOException;
import java.io.InputStream;

public class StreamUtil {
    private StreamUtil() {
    }

    /**
     * Reads {@code n} bytes from the input stream.
     *
     * @param n How many bytes should be read.
     * @return Byte array of length {@param n} filled with the bytes read from the input stream.
     * @throws IOException IOException issued by read call to the stream or if not n bytes could be
     *                     read.
     */
    public static byte[] readNBytes(InputStream inputStream, int n) throws IOException {
        byte[] bytes = new byte[n];
        int offset = 0;
        while (offset < n) {
            int read = inputStream.read(bytes, offset, n - offset);
            if (read == -1) {
                throw new IOException("Could not read " + n + " bytes from the stream. Read returned:" + read);
            } else {
                offset += read;
            }
        }
        return bytes;
    }
}
