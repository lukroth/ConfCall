/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.signalprocessing.synchronisation.gcc;

import android.util.Log;

import org.jtransforms.fft.FloatFFT_1D;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ch.ethz.disco.signalprocessing.synchronisation.record.AudioSignalMixer;

/**
 * Finds the relative offsets (how much they are shifted relative to each other) of different
 * Signals. Uses Generalized Cross Correlation (GCC-PATH) to find the sample offset.
 */
public class GCCPathOffsetFinder {
    private static final String TAG = GCCPathOffsetFinder.class.getCanonicalName();

    private GCCPathOffsetFinder() {
    }

    /**
     * Finds the relative offsets of the different signals passed as an argument to one selected
     * reference signal.
     * <p>
     * If the signals gain is to small, the offset will not be calculated and 0 offset is assumed
     *
     * @param signals           Map of ChannelIds to the signals.
     * @param signalGainMinimum The minimum gain the signal is required to have if its offset should
     *                          be computed.
     * @return Map - Mapping a signal s1 to a map of other signals, e.g. s2 mapping to the offset s1
     * has to s2.
     */
    public static Map<Integer, Map<Integer, Integer>> computeOffsets(Map<Integer, short[]> signals, double signalGainMinimum) {
        if (signals == null || signals.size() < 2) {
            return new HashMap<>();
        }
        Map<Integer, short[]> filteredSignals = filterOutLittleGainedSignals(signals, signalGainMinimum);
        return computeOffsets(filteredSignals);
    }

    static Map<Integer, short[]> filterOutLittleGainedSignals(Map<Integer, short[]> signals, double signalGainMinimum) {
        if (signalGainMinimum == 0) {
            return signals;
        }
        Map<Integer, short[]> filteredSignals = new HashMap<>();
        for (Map.Entry<Integer, short[]> entry : signals.entrySet()) {
            if (averageGain(entry.getValue()) > signalGainMinimum) {
                filteredSignals.put(entry.getKey(), entry.getValue());
            }
        }
        return filteredSignals;
    }

    /**
     * Computes the average gain of the signal.
     *
     * @param signal The signal from which we want the average gain.
     * @return The average gain of the signal. (sum(signal) / signal.length)
     */
    static double averageGain(short[] signal) {
        // Take care of overflow: if you sum up and then divide through the length you get overflow
        // problems if the array is to large
        double average = 0.0d;
        int size = signal.length;
        for (short s : signal) {
            double tmp = s / (double) size;
            average += Math.abs(tmp);
        }
        return average;
    }


    /**
     * Finds the relative offsets of the different signals passed as an argument to one selected
     * reference signal.
     *
     * @param signals Map of ChannelIds to the signals. Assumes the signals to have a correlation
     *                with each other.
     * @return Map - Mapping a signal s1 to a map of other signals, e.g. s2 mapping to the offset s1
     * has to s2.
     */
    static Map<Integer, Map<Integer, Integer>> computeOffsets(Map<Integer, short[]> signals) {
        if (signals == null || signals.size() < 2) {
            return new HashMap<>();
        }

        List<Integer> channelIDs = new ArrayList<>(signals.keySet());
        int referenceChannelId = AudioSignalMixer.getHostChannelId();

        Map<Integer, Integer> signalToOffset = new HashMap<>();
        for (int i = 0; i < channelIDs.size(); i++) {
            Integer currentChannelId = channelIDs.get(i);
            if (currentChannelId != referenceChannelId) {
                int offset = computeOffsetGCCPath(signals.get(referenceChannelId), signals.get(currentChannelId));
                signalToOffset.put(currentChannelId, offset);
            }
        }

        Map<Integer, Map<Integer, Integer>> s1toS2toOffset = new HashMap<>();
        s1toS2toOffset.put(referenceChannelId, signalToOffset);
        return s1toS2toOffset;
    }

    /**
     * Transforms a signal by creating a copy of the original signal with a zero boundary, e.g.
     * 123456789 to 000123000.
     *
     * @param shorts        The signal which should be copied
     * @param zeroPaddIndex How much should be zero padded.
     * @return The copy of the signal which has a zero boundary of ((int) shorts.length/3)
     */
    private static short[] createZeroPaddedCopy(short[] shorts, int zeroPaddIndex) {
        short[] zeroBoundarySignal = new short[shorts.length];
        int i = 0;
        for (; i < zeroPaddIndex; i++) {
            zeroBoundarySignal[i] = 0;
        }
        for (; i < zeroPaddIndex * 2; i++) {
            zeroBoundarySignal[i] = shorts[i - zeroPaddIndex];
        }
        for (; i < shorts.length; i++) {
            zeroBoundarySignal[i] = 0;
        }
        return zeroBoundarySignal;
    }

    /**
     * Computes the delay of the second signal {@code signal} relative to the reference signal
     * {@code referenceSignal}
     * <p>
     * <b>Warning:</b> Does only find offsets which are a lot smaller than the length of the signal
     * passed as an argument. Meaning e.g. for signal.length = 5000 it works approximately for
     * offsets -800 < offset < 800;
     * <p>
     * <b>Examples:</b> 123456789, RR1234567 returns 2. 123456789, 3456789RR returns -2. R stands
     * for a random number.
     * <p>
     *
     * @param referenceSignal The reference Signal
     * @param signal          The delayed signal.
     * @return Offset: how much samples is the signal is delayed compared to the reference signal.
     */
    public static int computeOffsetGCCPath(short[] referenceSignal, short[] signal) {
        /*
        See GCC-Path, e.g. page 3 in in "Distributed Discussion Diarisation"
        https://ieeexplore.ieee.org/document/7983281/
        */
        if (referenceSignal.length != signal.length) {
            Log.e(TAG, "Expected Signals of the same length");
        }
        FloatFFT_1D fft1D = new FloatFFT_1D(referenceSignal.length);
        float[] signal1Copy = createFloatArray(referenceSignal);
        int zerroPaddIndex = referenceSignal.length / 3;
        float[] signal2Copy = createFloatArray(createZeroPaddedCopy(signal, zerroPaddIndex));
        // FFT of the signals
        fft1D.realForwardFull(signal1Copy);
        fft1D.realForwardFull(signal2Copy);
        // Conjugate the second signal
        complexConjugate(signal2Copy);
        // Multiply the signals pointwise
        float[] multipliedS1S2 = multiplyComplexArrays(signal1Copy, signal2Copy);
        // Do the PATH Normalisation Constraint (Just normalize all complex numbers)
        normalize(multipliedS1S2);
        // Take the inverse to get back into the time space
        fft1D.complexInverse(multipliedS1S2, false);
        // Search the time space for the maximum -> = Offset
        int offset = findMaximumCorrelationIndex(multipliedS1S2);
        // Adjust the offset since we did shift the second signal while zero-padding it
        offset -= zerroPaddIndex;

        if (offset == -zerroPaddIndex) {
            /*
            If we see an offset of 0 (which will be -= zeroPaddIndex) this means no offset could be
            computed -> we return 0 instead of -zeroPaddIndex.
             */
            return 0;
        }
        return offset;
    }

    /**
     * Normalizes a complex signal. Each complex number in the signal gets divided by its norm.
     *
     * @param complexSignal The signal that will be normalized
     */
    static void normalize(float[] complexSignal) {
        if (complexSignal.length % 2 != 0) {
            Log.e(TAG, "Expected the signal.length to be even.");
            return;
        }
        for (int i = 0; i < complexSignal.length; i += 2) {
            float real = complexSignal[i];
            float complex = complexSignal[i + 1];
            double length = Math.sqrt(Math.pow(real, 2) + Math.pow(complex, 2));
            if (length == 0) {
                // We cannot normalize a complex number with length 0 -> we will leave it.
                continue;
            }
            real /= length;
            complex /= length;
            complexSignal[i] = real;
            complexSignal[i + 1] = complex;
        }
    }

    private static int findMaximumCorrelationIndex(float[] complexSignal) {
        float max = -9999999;
        int maxIndex = -1;

        /*
        We only have to search the first half of the signal since the second half is mirrored (I
        don't know why, but it works like that. If we search the total area some times the mirrored
        peak at the end is found which is wrong.
         */
        int searchArea = complexSignal.length / 2;
        for (int index = 0; index < searchArea; index += 2) {
            // Not sure if we here should do abs or not...
            float currentCorrelationValue = Math.abs(complexSignal[index]);
            if (currentCorrelationValue > max) {
                max = currentCorrelationValue;
                maxIndex = index;
            }
        }
        return maxIndex / 2; // We divide by two since we have [real - imaginary - real - imaginary -...] inside the array
    }

    /**
     * Multiplies the two complex signals and saves the result in a new array. Assumes the signals
     * to have even length and the same length.
     * <p>
     * The representation of the complex signal is: e.g. a + bi, c + di,... is saved as [a, b, c, d,
     * ...]
     *
     * @param signal1 Complex signal
     * @param signal2 Complex signal
     * @return The complex multiplied signal (signal1 .* signal2)
     */
    private static float[] multiplyComplexArrays(float[] signal1, float[] signal2) {
        float[] result = new float[signal1.length];
        if (signal1.length % 2 != 0) {
            Log.e(TAG, "Think why this happens, may be correct, but cannot handle so far");
        }
        for (int realIndex = 0; realIndex < signal1.length; realIndex += 2) {
            int complexIndex = realIndex + 1;
            float realPart = signal1[realIndex] * signal2[realIndex] - signal1[complexIndex] * signal2[complexIndex];
            float complexPart = signal1[realIndex] * signal2[realIndex] + signal1[complexIndex] * signal2[complexIndex];
            result[realIndex] = realPart;
            result[complexIndex] = complexPart;
        }
        return result;
    }

    /**
     * Conjugates the signal passed as the argument.
     *
     * @param signal The complex signal that gets conjugated.
     */
    static void complexConjugate(float[] signal) {
        for (int i = 1; i < signal.length; i += 2) {
            signal[i] = -signal[i];
        }
    }

    /**
     * Creates a copy of the short array (with twice the size as the argument) and casts to floats.
     *
     * @param signal The signal which should be copied.
     * @return The signal as float array
     */
    private static float[] createFloatArray(short[] signal) {
        float[] floatCopy = new float[signal.length * 2];
        for (int i = 0; i < signal.length; i++) {
            floatCopy[i] = signal[i];
        }
        return floatCopy;
    }
}
