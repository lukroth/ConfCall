/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.playback;

import android.util.Log;

import ch.ethz.disco.networking.socket.CommunicationInterfaceCommand;
import ch.ethz.disco.networking.socket.ServerCommunicationInterface;

import static ch.ethz.disco.util.ArrayUtil.toByteArray;

/**
 * Acts like an audioPlayer but does not play the audio but sends all the audioData to the master
 * phone.
 */
public class AudioBroadcaster implements AudioPlayer {
    private static final String TAG = AudioBroadcaster.class.getCanonicalName();
    private final ServerCommunicationInterface serverCommunicationInterface;

    public AudioBroadcaster(ServerCommunicationInterface serverCommunicationInterface) {
        this.serverCommunicationInterface = serverCommunicationInterface;
    }

    @Override
    public void start() {
        broadcastCommand(CommunicationInterfaceCommand.AUDIO_START.getCode());
    }

    @Override
    public void pause() {
        broadcastCommand(CommunicationInterfaceCommand.AUDIO_PAUSE.getCode());
    }

    @Override
    public int getPlaybackHeadPosition() {
        Log.e(TAG, "Not implemented");
        return 0;
    }

    @Override
    public void stop() {
        broadcastCommand(CommunicationInterfaceCommand.AUDIO_PAUSE.getCode());
    }

    @Override
    public int write(short[] audioData, int offsetInShorts, int sizeInShorts, int writtenShorts) {
        Log.e(TAG, "Not implemented. Use other write this class provides");
        return sizeInShorts;
    }


    @Override
    public void startIfNotAlreadyPlaying() {
        throw new UnsupportedOperationException("Not implemented");
    }

    /**
     * Same as {@code write(short[] audioData, int offsetInShorts, int sizeInShorts)}, but with the
     * addition that if {@code statIfNotPlayingAlready} is true the player will play if he does not
     * already play.
     *
     * @param audioData                The audio Data
     * @param offsetInShorts           The offset inside the audioData
     * @param sizeInShorts             The length of the audioData
     * @param startIfNotPlayingAlready True means that the player should start play if he does not
     *                                 already play.
     * @param writtenShorts            The number of shorts that have been written until the start
     *                                 of this data
     * @return How many shorts have been written.
     */
    public int write(short[] audioData, int offsetInShorts, int sizeInShorts, boolean startIfNotPlayingAlready, int writtenShorts) {
        byte[] audioDataAsByte = toByteArray(audioData, offsetInShorts, sizeInShorts);
        broadcastDataCommand(CommunicationInterfaceCommand.AUDIO_WRITE_START_PLAY_IF_NOT_PLAYING.getCode(), writtenShorts, audioDataAsByte);
        return audioDataAsByte.length / 2;
    }

    private void broadcastCommand(int command) {
        broadcastDataCommand(command, 0, new byte[0]);
    }

    private void broadcastDataCommand(int command, int value, byte[] data) {
        serverCommunicationInterface.broadcastCommandAndData(command, value, data);
    }
}