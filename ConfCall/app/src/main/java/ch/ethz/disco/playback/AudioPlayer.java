/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.playback;

/**
 * Audio Player that plays audio data in streaming mode.
 */
public interface AudioPlayer {
    /**
     * Start playing
     */
    void start();

    /**
     * Pause playing
     */
    void pause();

    /**
     * Get the position where we are playing at the moment.
     * <p>
     * See javadoc of the android AudioTrack class.
     */
    int getPlaybackHeadPosition();

    /**
     * Stop playing
     */
    void stop();

    /**
     * see javadoc of the android AudioTrack class.
     */
    int write(short[] audioData, int offsetInShorts, int sizeInShorts, int writtenShorts);

    /**
     * Calls start() if not already running.
     */
    void startIfNotAlreadyPlaying();
}
