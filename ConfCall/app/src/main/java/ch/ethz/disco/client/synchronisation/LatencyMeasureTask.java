/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.client.synchronisation;

import android.util.Log;

import java.io.IOException;
import java.net.InetAddress;

import ch.ethz.disco.signalprocessing.frameprocessors.filter.SumSlidingWindowFilter;

/**
 * Measures the network latency between this device and some remote device.
 */
class LatencyMeasureTask extends Thread {
    private static final String TAG = LatencyMeasureTask.class.getName();
    private static final int REQUESTS_PER_SECOND = 4;
    private static final int FILTER_LENGTH = 5 * REQUESTS_PER_SECOND;
    private final String hostAddress;
    private final LatencyUpdateListener latencyUpdateListener;
    private final SumSlidingWindowFilter latencyAverageFilter = new SumSlidingWindowFilter(FILTER_LENGTH, (double) 0);
    private boolean running = false;
    private int counter = 0;

    /**
     * @param hostAddress           The address for which you want to measure the latency
     * @param latencyUpdateListener The updateListener that receives latency updates.
     */
    LatencyMeasureTask(String hostAddress, LatencyUpdateListener latencyUpdateListener) {
        this.hostAddress = hostAddress;
        this.latencyUpdateListener = latencyUpdateListener;
    }

    @Override
    public void run() {
        running = true;
        while (running) {
            int timeOut = 5000;
            long nanoBefore = System.nanoTime();
            boolean status = false;
            try {
                status = InetAddress.getByName(hostAddress).isReachable(timeOut);
            } catch (IOException e) {
                Log.w(TAG, "Could not ping", e);
            }
            long nanoAfter = System.nanoTime();
            long latency = (nanoAfter - nanoBefore) / 2;
            if (status) {
                latencyAverageFilter.onNewElement((double) latency);
            }
            try {
                Thread.sleep(1000 / REQUESTS_PER_SECOND);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                running = false;
                break;
            }
            if (counter++ % FILTER_LENGTH == 0) {
                Double averageLatency = latencyAverageFilter.getAverage();
                Log.v(TAG, "average latency:" + averageLatency);
                latencyUpdateListener.newLatencyValue(averageLatency);
            }
        }
    }

    public void stopMeasuring() {
        this.running = false;
    }

    public interface LatencyUpdateListener {
        void newLatencyValue(double latencyInNanoSec);
    }
}
