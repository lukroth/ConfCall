/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.signalprocessing.synchronisation;

import android.util.Log;

import java.util.HashSet;
import java.util.Set;

import ch.ethz.disco.signalprocessing.synchronisation.record.AudioSignalMixer;
import ch.ethz.disco.signalprocessing.synchronisation.record.Frame;

class InitialChannelOffsetDetector {
    private static final String TAG = InitialChannelOffsetDetector.class.getName();
    private final TotalOffsetBookkeeper totalOffsetBookkeeper;

    private Set<Integer> seenChannels = new HashSet<>();

    InitialChannelOffsetDetector(TotalOffsetBookkeeper totalOffsetBookkeeper) {
        this.totalOffsetBookkeeper = totalOffsetBookkeeper;
    }

    public void processFrame(Frame frame) {
        for (Integer channelId : frame.getChannelsToDataMap().keySet()) {
            onNewDataFromChannel(channelId, calcCurrentOffset(frame));
        }
    }

    private int calcCurrentOffset(Frame frame) {
        int frameIndex = frame.getFrameIndex();
        frameIndex--;
        int frameLength = frame.getChannelsToDataMap().values().iterator().next().length;
        return frameIndex * frameLength;
    }

    private void onNewDataFromChannel(Integer channelId, int offset) {
        if (!seenChannels.contains(channelId)) {
            newChannel(channelId, offset);
            seenChannels.add(channelId);
        }
    }

    private void newChannel(Integer channelId, int offset) {
        if (channelId == AudioSignalMixer.getHostChannelId()) {
            // For the host channel we do not have to do anything
            if (offset != 0) {
                Log.e(TAG, "Would expect a 0 offset of the host channel, " +
                        "since the host channel acts as a reference channel");
            }
            return;
        }
        totalOffsetBookkeeper.newInitialRecordOffset(channelId, offset);
    }
}
