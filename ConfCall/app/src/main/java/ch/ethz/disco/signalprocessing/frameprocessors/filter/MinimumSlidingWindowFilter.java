/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.signalprocessing.frameprocessors.filter;

import java.util.PriorityQueue;

/**
 * Sliding window filter to get a minimum value over a sliding window.
 *
 * @param <E> The elements inside the sliding window.
 */
public class MinimumSlidingWindowFilter<E extends Comparable> extends QueueSlidingWindowFilter<E> {
    private final PriorityQueue<E> priorityQueue = new PriorityQueue<>();

    public MinimumSlidingWindowFilter(int filterLength, E initialElement) {
        super(filterLength, initialElement);
        for (int i = 0; i < filterLength; i++) {
            priorityQueue.add(initialElement);
        }
    }

    @Override
    public void onNewElement(E element) {
        E oldestElement = addNewElementToQueue(element);
        priorityQueue.add(element);
        priorityQueue.remove(oldestElement);
    }

    public E getMinimum() {
        return priorityQueue.peek();
    }
}
