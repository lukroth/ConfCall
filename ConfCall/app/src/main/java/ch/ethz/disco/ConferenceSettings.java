/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco;

import android.media.AudioFormat;
import android.preference.PreferenceManager;

import org.sipdroid.sipua.ui.Settings;

import ch.ethz.disco.confcall.Receiver;

public class ConferenceSettings {
    /**
     * The conference can only operate under 16BIT PCM encoding.
     */
    public static final int AUDIO_FORMAT = AudioFormat.ENCODING_PCM_16BIT;

    public static boolean isConferenceInitiator() {
        return PreferenceManager.getDefaultSharedPreferences(Receiver.mContext).getBoolean(Settings.PREF_IS_CONFERENCE_INITIATOR, Settings.DEFAULT_IS_CONFERENCE_INITIATOR);
    }

    public static boolean isPlaybackConference() {
        return isConferenceInitiator();
    }

    public static boolean isConferenceSearcher() {
        return PreferenceManager.getDefaultSharedPreferences(Receiver.mContext).getBoolean(Settings.PREF_IS_CONFERENCE_SEARCHER, Settings.DEFAULT_IS_CONFERENCE_SEARCHER);
    }
}