/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.networking.wifibroadcaster.receive;

/**
 * Gets the broadcast messages sent over the network and broadcasts them again to the registered
 * {@code NetworkMessageReceiver}.
 */
public interface NetworkMessageBroadcaster {
    /**
     * Register the {@link NetworkMessageReceiver} to get the broadcast packets.
     *
     * @param networkMessageReceiver The {@link NetworkMessageReceiver} which will be registered.
     */
    void registerNetworkMessageReceiver(NetworkMessageReceiver networkMessageReceiver);

    /**
     * Deregister the {@link NetworkMessageReceiver} passed in the argument.
     *
     * @param networkMessageReceiver The {@link NetworkMessageReceiver} which will be
     *                               de-registered.
     */
    void deregisterNetworkMessageReceiver(NetworkMessageReceiver networkMessageReceiver);
}
