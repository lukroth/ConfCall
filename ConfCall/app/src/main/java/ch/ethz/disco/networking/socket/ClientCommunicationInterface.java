/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.networking.socket;

import android.util.Log;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;

import ch.ethz.disco.client.RunningClientConference;
import ch.ethz.disco.playback.AudioPlayer;
import ch.ethz.disco.util.ArrayUtil;
import ch.ethz.disco.util.StreamUtil;

/**
 * Receives the audio data and audio commands from the server over the socket inputStream and
 * forwards it to the corresponding services.
 */
public class ClientCommunicationInterface {
    private static final String TAG = ClientCommunicationInterface.class.getCanonicalName();
    private final DataInputStream inputStream;
    private final AudioPlayer audioPlayer;
    private final RunningClientConference runningClientConference;
    private boolean isRunning = false;
    private MessageReceiverInternal messageReceiverInternal;

    /**
     * @param inputStream The InputStream from which the commands are read.
     * @param audioPlayer The audioPlayer to which the commands are forwarded to.
     */
    public ClientCommunicationInterface(InputStream inputStream, AudioPlayer audioPlayer, RunningClientConference runningClientConference) {
        this.inputStream = new DataInputStream(inputStream);
        this.audioPlayer = audioPlayer;
        this.runningClientConference = runningClientConference;
    }

    /**
     * Start reading commands from the InputStream
     */
    public void start() {
        if (isRunning) {
            Log.e(TAG, "Cannot start a running audioReceiver");
            return;
        }
        isRunning = true;
        Log.v(TAG, "Start ClientCommunicationInterface");
        messageReceiverInternal = new MessageReceiverInternal();
        messageReceiverInternal.start();
    }

    /**
     * Stop reading commands from the InputStream
     */
    public void stop() {
        if (isRunning) {
            isRunning = false;
            Log.v(TAG, "Stop ClientCommunicationInterface");
            messageReceiverInternal.interrupt();
        } else {
            Log.w(TAG, "Cannot stop a not running ClientCommunicationInterface");
        }
    }

    /**
     * Do the action the command specifies. e.g. start the audioPlayer.
     */
    boolean handleCommand(CommunicationInterfaceCommand command, int value, byte[] data) {
        if (command == null) {
            Log.e(TAG, "Null command cannot be handled!");
            return false;
        }
        switch (command) {
            case AUDIO_START:
                audioPlayer.start();
                break;
            case AUDIO_PAUSE:
                audioPlayer.pause();
                break;
            case AUDIO_STOP:
                audioPlayer.stop();
                break;
            case AUDIO_WRITE:
                write(value, data);
                break;
            case AUDIO_WRITE_START_PLAY_IF_NOT_PLAYING:
                audioPlayer.startIfNotAlreadyPlaying();
                write(value, data);
                break;
            case CONFERENCE_RESET_CONNECTION:
                runningClientConference.resetCommand();
                break;
            case CONFERENCE_STOP:
                runningClientConference.stopCommand();
                break;
            default:
                Log.e(TAG, "Not known command:" + command);
                return false;
        }
        return true;
    }

    /**
     * Write the data to the audioPlayer.
     *
     * @param data AudioData as byte array.
     */
    private void write(int totalOffset, byte[] data) {
        short[] shorts = ArrayUtil.toShortArray(data);
        audioPlayer.write(shorts, 0, shorts.length, totalOffset);
    }

    private class MessageReceiverInternal extends Thread {
        @Override
        public void run() {
            while (isRunning && !Thread.interrupted()) {
                try {
                    readNextMessage();
                } catch (IOException e) {
                    if (isRunning) {
                        Log.e(TAG, "IO error while reading next message", e);
                        ClientCommunicationInterface.this.stop();
                        return;
                    } // else: we can ignore the error since we exit
                }
            }
        }

        /**
         * Read the next message from the InputStream and parse it.
         */
        private void readNextMessage() throws IOException {
            int command = inputStream.readInt();
            if (command == -1) {
                throw new IOException("End of stream");
            }
            int value = inputStream.readInt();
            int dataLength = inputStream.readInt();
            if (dataLength == -1) {
                throw new IOException("End of stream");
            }
            byte[] data = StreamUtil.readNBytes(inputStream, dataLength);
            parseMessage(command, value, data);
        }

        /**
         * Parse the message specified by the arguments.
         *
         * @param command The command id. Matches a {@code CommunicationInterfaceCommand}
         * @param value   The value that a command can have (like an argument)
         * @param data    The data following the command
         */
        private void parseMessage(int command, int value, byte[] data) {
            CommunicationInterfaceCommand communicationInterfaceCommand = CommunicationInterfaceCommand.fromCode(command);
            if (communicationInterfaceCommand == null) {
                Log.e(TAG, "Cannot parse command with number=" + command);
            }
            handleCommand(communicationInterfaceCommand, value, data);
        }
    }
}
