/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.signalprocessing.frameprocessors.relativegain;

import java.util.HashMap;
import java.util.Map;

import ch.ethz.disco.signalprocessing.frameprocessors.filter.MaximumSlidingWindowFilter;

/**
 * Adjusts the gain of an audio signal. Should be used to adjust audio signals captured by multiple
 * microphones on multiple devices.
 */
public class GainAdjuster {
    private static final double DESIRED_GAIN_MAX = Short.MAX_VALUE * 0.2;
    private static final int MAXIMUM_SLIDING_WINDOW_FILTER_LENGTH_IN_SECONDS = 60 * 10; // 10 minutes
    private final int maximumSlidingWindowFilterLengthInFrames;
    private Map<Integer, MaximumSlidingWindowFilter> maxSeenValueFilters = new HashMap<>();

    GainAdjuster(int sampleRate, int frameLength) {
        maximumSlidingWindowFilterLengthInFrames = (int) (MAXIMUM_SLIDING_WINDOW_FILTER_LENGTH_IN_SECONDS * ((float) sampleRate / frameLength));
    }

    private static double maxValueInside(short[] inputData) {
        short max = -1;
        for (short s : inputData) {
            if (Math.abs(s) > max) {
                max = (short) Math.abs(s); //Problem if we had a Short.MIN_VAL when casting back to int, but should not happen...
            }
        }
        return max;
    }

    /**
     * Adjusts the gain of the audio signal passed in inputAudioData to some predefined level
     * derived from the {@code maxMeasurementOnThisChannel}and the maximum gain value seen in the
     * {@code inputAudioData}.
     *
     * @param inputAudioData              The input data. Is left unchanged.
     * @param maxMeasurementOnThisChannel The maximum average gain ever seen on the channel where
     *                                    the audioData originates.
     * @return A copy of the {@code inputAudioData} with adjusted gain or the original {@code
     * inputAudioData} if {@code maxMeasurementOnThisChannel}== 0
     */
    public short[] adjustGain(short[] inputAudioData, double maxMeasurementOnThisChannel, int channelId) {
        if (maxMeasurementOnThisChannel == 0) {
            return inputAudioData;
        }
        MaximumSlidingWindowFilter maxSeenValueFilter = getMaxSeenValueFilter(channelId);
        maxSeenValueFilter.onNewElement(maxValueInside(inputAudioData));
        double maxValueSeenInside = maxSeenValueFilter.getMaximum();

        short[] res = new short[inputAudioData.length];
        double volumeDerivedFromGlobalMax = DESIRED_GAIN_MAX / maxMeasurementOnThisChannel;
        double volumeDerivedFromThisData = DESIRED_GAIN_MAX / maxValueSeenInside;
        double volume = Math.min(volumeDerivedFromGlobalMax, volumeDerivedFromThisData);
        for (int i = 0; i < inputAudioData.length; i++) {
            res[i] = (short) (inputAudioData[i] * volume);
        }
        return res;
    }

    private MaximumSlidingWindowFilter getMaxSeenValueFilter(int channelId) {
        if (!maxSeenValueFilters.containsKey(channelId)) {
            maxSeenValueFilters.put(channelId, new MaximumSlidingWindowFilter(maximumSlidingWindowFilterLengthInFrames, 0));
        }
        return maxSeenValueFilters.get(channelId);
    }
}
