/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.signalprocessing.synchronisation.record;

import android.util.Log;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * Takes audioParts from all the queues and merges them. As a reference channel it takes the master
 * channel. If one channel is not sending the FrameTaker ignores this channel until this channel
 * sends again.
 */
public class FrameTakerMasterAsReference {
    private static final String TAG = FrameTakerMasterAsReference.class.getCanonicalName();
    /**
     * How many times do we allow a channel to be not in time until we reset it.
     */
    private static final int INACTIVE_CHANNEL_RESET_THRESHOLD = 20;
    private final int howManyTimesIgnoredAfterReset;
    /**
     * The channels that have been reset. We will not take from this channels for some time to
     * give them time and not removing missed elements again
     */
    private Map<Integer, Long> resetChannelToPauseCount = new HashMap<>();
    /**
     * This field describes how long we wait until we check again the maxQueueSize condition.
     * <p>
     * Important: that this is not the condition on which we decide not to take some channels, it
     * only describes the interval of how often we check the condition.
     */
    private int timeOutMs = 50;
    private Map<Integer, BlockingQueue<short[]>> channelAudioPartsQueue;
    /**
     * The maxQueueSize describes how long the queue should get at most. If one Channel is no more
     * sending then the AudioSignalMixer waits until the maxQueueSize is reached and ignores this
     * channel until he sends again.
     */
    private int maxQueueSize;
    private DelayedFramesBookkeeper delayedFramesBookkeeper = new DelayedFramesBookkeeper();
    private InactiveChannelBookkeeper inactiveChannelBookkeeper = new InactiveChannelBookkeeper(INACTIVE_CHANNEL_RESET_THRESHOLD);
    private int frameCounter = 0;
    private Map<Integer, Integer> channelsWhoSentAtLeastOnceData = new HashMap<>();

    /**
     * @param channelAudioPartsQueue The queue from which the audioParts should be taken to generate
     *                               the frame.
     * @param maxQueueSize           The maxQueueSize describes how long the queue should get at
     *                               most. If one Channel is no more sending then the
     *                               AudioSignalMixer waits until the maxQueueSize is reached and
     *                               then ignores this channel until he sends again.
     */
    FrameTakerMasterAsReference(Map<Integer, BlockingQueue<short[]>> channelAudioPartsQueue, int maxQueueSize, ChannelResetter channelResetter) {
        this.channelAudioPartsQueue = channelAudioPartsQueue;
        this.maxQueueSize = maxQueueSize;
        this.howManyTimesIgnoredAfterReset = maxQueueSize / 3;
    }

    public void setTimeOutMs(int timeOutMs) {
        this.timeOutMs = timeOutMs;
    }

    /**
     * Generates the next frame from the different Queues.
     *
     * @return The next frame generated from all the channelQueues
     * @throws InterruptedException If Interrupted during taking the next frame.
     */
    public Frame takeNextFrame() throws InterruptedException {
        trimQueue();
        Map<Integer, short[]> frame = new HashMap<>();
        Set<Integer> channelsToTakeFrom = getChannelsToTakeFrom();
        while (queueBufferMaxNotReached() && !channelsToTakeFrom.isEmpty()) {
            Integer currentChannelId = channelsToTakeFrom.iterator().next();
            if (takeAudioPart(frame, currentChannelId, timeOutMs)) {
                channelsToTakeFrom.remove(currentChannelId);
            }
        }

        // if the queueBufferMax is reached we just take what we can get without any delay and leave the rest

        // to remove ConcurrentModificationException inside the next loop make a copy
        Integer[] channelIdsNotYetTakenFromArrayCopy = channelsToTakeFrom.toArray(new Integer[0]);
        for (Integer channelId : channelIdsNotYetTakenFromArrayCopy) {
            if (takeAudioPart(frame, channelId, 0)) {
                channelsToTakeFrom.remove(channelId);
            }
        }
        addToOnceActiveChannels(frame.keySet());

        Set<Integer> channelsOnceActive = new HashSet<>(channelsToTakeFrom);
        channelsOnceActive.retainAll(getChannelsThatSentAtLeastNTimes(maxQueueSize / 3));
        onInactiveChannels(channelsOnceActive);
        logInactiveChannels(channelsToTakeFrom);
        frameCounter++;
        return new Frame(frameCounter, frame);
    }

    private Collection<?> getChannelsThatSentAtLeastNTimes(int numberOfRequiredSents) {
        Set<Integer> activeChannels = new HashSet<>();
        for (Map.Entry<Integer, Integer> entry : channelsWhoSentAtLeastOnceData.entrySet()) {
            if (entry.getValue() > numberOfRequiredSents) {
                activeChannels.add(entry.getKey());
            }
        }
        return activeChannels;
    }

    private void addToOnceActiveChannels(Set<Integer> channelIds) {
        for (int channelId : channelIds) {
            Integer timesSent = channelsWhoSentAtLeastOnceData.get(channelId);
            if (timesSent == null) {
                timesSent = 0;
            }
            channelsWhoSentAtLeastOnceData.put(channelId, timesSent + 1);
        }
    }

    private void onInactiveChannels(Set<Integer> inactiveChannelIds) {
        delayedFramesBookkeeper.ignoredFramesFromChannels(inactiveChannelIds);
        Set<Integer> channelsToReset = inactiveChannelBookkeeper.onInactiveChannels(inactiveChannelIds);
        delayedFramesBookkeeper.resetIgnoreCounterForChannels(channelsToReset);
        for (int channelId : channelsToReset) {
            resetChannelToPauseCount.put(channelId, (long) howManyTimesIgnoredAfterReset);
        }
    }

    /**
     * If some remotes send to much (can happen when they before sent to less and now send the
     * remaining data) we remove the oldest data that the remote queues are also at most of size
     * {@link FrameTakerMasterAsReference#maxQueueSize}.
     */
    private void trimQueue() {
        for (Map.Entry<Integer, BlockingQueue<short[]>> entry : channelAudioPartsQueue.entrySet()) {
            int channelId = entry.getKey();
            if (channelId != AudioSignalMixer.getHostChannelId()) {
                BlockingQueue<short[]> queue = entry.getValue();
                while (queue.size() > maxQueueSize) {
                    short[] removedElem = queue.poll();
                    if (removedElem == null) {
                        Log.e(TAG, "Expected non null element since queue should have elements");
                    }
                    Log.v(TAG, "Removed element from queue for ChannelID = " + channelId + ". Queue overflow");
                }
            }
        }
    }

    private void logInactiveChannels(Set<Integer> channelIdsNotYetTakenFrom) {
        if (channelIdsNotYetTakenFrom.isEmpty()) {
            return;
        }
        Log.v(TAG, "Channels not sending at the moment: " + channelIdsNotYetTakenFrom.toString());
    }

    /**
     * Takes an audio part and adds it to the frame if there was one. Removes any frames that should
     * be removed because of they were delayed.
     *
     * @param frame     The frame where the audioPart should be added to.
     * @param channelId The channelId from where the audioPart should be taken
     * @param timeOutMs The time we wait for an audioPart to be there.
     * @return Boolean - true if an audioPart was added or the channel is no more active. False if
     * none was added due to the timeOut.
     * @throws InterruptedException If interrupted during taking the audio part.
     */
    private boolean takeAudioPart(Map<Integer, short[]> frame, Integer channelId, int timeOutMs) throws InterruptedException {
        short[] audioPart;
        BlockingQueue<short[]> queue = channelAudioPartsQueue.get(channelId);
        if (queue == null) {
            // Can happen when a channel gets removed by another thread.
            return true;
        }
        if (!delayedFramesBookkeeper.removeFramesIfNecessary(channelId, queue)) {
            // Not enough audio data there so we return false.
            return false;
        }
        if (timeOutMs != 0) {
            audioPart = queue.poll(timeOutMs, TimeUnit.MILLISECONDS);
        } else {
            audioPart = queue.poll();
        }
        if (audioPart == null) {
            return false;
        }
        frame.put(channelId, audioPart);
        return true;
    }

    /**
     * Checks if the queue buffer defined over the maxQueueSize is not full
     *
     * @return true if not full, false otherwise
     */
    private boolean queueBufferMaxNotReached() {
        return channelAudioPartsQueue.get(AudioSignalMixer.getHostChannelId()).size() <= maxQueueSize;
    }

    private Set<Integer> copy(Set<Integer> original) {
        return new HashSet<>(original);
    }

    private synchronized Set<Integer> getChannelsToTakeFrom() {
        Set<Integer> channelsToTakeFrom = copy(channelAudioPartsQueue.keySet());
        Set<Integer> shortTimeAgoResettedChannels = resetChannelToPauseCount.keySet();
        channelsToTakeFrom.removeAll(shortTimeAgoResettedChannels);

        Map<Integer, Long> newResettedChannelToPauseCount = new HashMap<>();

        for (Map.Entry<Integer, Long> entry : resetChannelToPauseCount.entrySet()) {
            if (entry.getValue() - 1 > 0) {
                newResettedChannelToPauseCount.put(entry.getKey(), entry.getValue() - 1);
            }
        }
        resetChannelToPauseCount = newResettedChannelToPauseCount;
        return channelsToTakeFrom;
    }

    /**
     * Keeps track of the ignored frames (ignored frames = the frames that do not arrive on time).
     * We later ignore this frames if they ever arrive.
     */
    public class DelayedFramesBookkeeper {
        /**
         *
         */
        private Map<Integer, Integer> channelIdToIgnoreFrameCount = new HashMap<>();

        /**
         * Informs about newly frames which were not on time.
         *
         * @param channelIds The channelIds for which the frames were not there.
         */
        void ignoredFramesFromChannels(Set<Integer> channelIds) {
            for (int channelId : channelIds) {
                incrementIgnoreCounter(channelId);
            }
        }

        /**
         * Saves that one more frame must be ignored.
         *
         * @param channelId The channelId where the frame belongs to.
         */
        private void incrementIgnoreCounter(int channelId) {
            if (!channelIdToIgnoreFrameCount.containsKey(channelId)) {
                channelIdToIgnoreFrameCount.put(channelId, 0);
            }
            int oldCount = channelIdToIgnoreFrameCount.remove(channelId);
            channelIdToIgnoreFrameCount.put(channelId, oldCount + 1);
        }

        /**
         * Removes frame from the queue if there should be frames removed because they were late and
         * cannot be used anymore now.
         *
         * @param channelId The ChannelId to which the queue belongs
         * @param queue     The queue of new frames.
         * @return True if all frames that must be deleted could be deleted. False if they could not
         * be deleted and therefor no reading should happen on this queue.
         */
        boolean removeFramesIfNecessary(Integer channelId, BlockingQueue<short[]> queue) {
            while (channelIdToIgnoreFrameCount.get(channelId) != null && channelIdToIgnoreFrameCount.get(channelId) > 0) {
                short[] frameToRemove = queue.poll();
                if (frameToRemove != null) {
                    // We must remove this frame and decrement the counter
                    Log.v(TAG, "Ignored a frame from channelID = " + channelId);
                    decrementIgnoreCounter(channelId);
                } else {
                    // No frames are ready
                    return false;
                }
            }
            return true;
        }

        /**
         * Saves that we could ignore a frame on the channelIds queue.
         *
         * @param channelId The channel id for which a frame was ignored
         */
        private void decrementIgnoreCounter(Integer channelId) {
            Integer oldValue = channelIdToIgnoreFrameCount.remove(channelId);
            if (oldValue == null) {
                Log.e(TAG, "Expected a value here since it should have a positive count if we remove");
                return;
            }
            int newValue = oldValue - 1;
            if (newValue > 0) {
                channelIdToIgnoreFrameCount.put(channelId, newValue);
            }
        }

        void resetIgnoreCounterForChannels(Set<Integer> channelsToReset) {
            for (int channelId : channelsToReset) {
                channelIdToIgnoreFrameCount.remove(channelId);
                Log.w(TAG, "Channel " + channelId + " was not sending - resetting the DelayedFramesBookkeeper");
            }
        }
    }
}
