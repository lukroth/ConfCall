/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.util;

import android.util.Log;

import java.net.MulticastSocket;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

public class NetworkUtil {
    private static final String TAG = NetworkUtil.class.getCanonicalName();

    private NetworkUtil() {
    }

    /**
     * Finds Network Interface of Wifi Ethernet.
     * <p>
     * Thanks to: https://stackoverflow.com/questions/6550618/multicast-support-on-android-in-hotspot-tethering-mode/27699370#27699370
     *
     * @return The wifi network interface or null if none present.
     */
    public static NetworkInterface findWifiNetworkInterface() {
        Enumeration<NetworkInterface> enumeration = null;
        try {
            enumeration = NetworkInterface.getNetworkInterfaces();
        } catch (SocketException e) {
            Log.e(TAG, "Could not get NetworkInterfaces", e);
            return null;
        }

        NetworkInterface wlan0 = null;
        while (enumeration.hasMoreElements()) {
            wlan0 = enumeration.nextElement();
            if (wlan0.getName().equals("wlan0")) {
                return wlan0;
            }
        }

        return null;
    }

    /**
     * Binds the socket passed in the argument to the wlan0 network interface.
     *
     * @param multicastSocket The socket that gets bind to the wlan0 network interface.
     * @throws SocketException
     */
    public static void bindToWlanNetworkInterface(MulticastSocket multicastSocket) throws SocketException {
        NetworkInterface wlanInterface = findWifiNetworkInterface();
        if (wlanInterface == null) {
            Log.w(TAG, "Could not bind to wlan network interface");
            return;
        }
        multicastSocket.setNetworkInterface(wlanInterface);
    }
}
