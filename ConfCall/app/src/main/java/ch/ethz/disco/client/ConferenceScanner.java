/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.client;

import android.app.Activity;

import ch.ethz.disco.networking.wifibroadcaster.receive.PacketReceiver;

/**
 * Searches for open conferences.
 */
public class ConferenceScanner {
    private final PacketReceiver packetReceiver;

    ConferenceScanner(Activity activity, Listener listener) {
        packetReceiver = new PacketReceiver(activity);
        new BroadcastMessageParser(packetReceiver, listener);
    }

    public void startScanning() {
        packetReceiver.startListeningForPackets();
    }

    public void stopScanning() {
        packetReceiver.stopListeningForPackets();
    }

    /**
     * Listens for events the {@link ConferenceScanner} emits.
     */
    interface Listener {
        void availableConferenceBroadcast(int sampleRate, String hostAddress, int port, String conferenceName);
    }
}
