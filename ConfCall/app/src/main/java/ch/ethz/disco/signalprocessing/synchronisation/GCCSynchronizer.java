/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.signalprocessing.synchronisation;

import android.util.Log;

import ch.ethz.disco.networking.socket.ServerCommunicationInterface;
import ch.ethz.disco.server.PlaybackConference;
import ch.ethz.disco.server.RecordConference;
import ch.ethz.disco.signalprocessing.synchronisation.record.AudioSignalMixer;
import ch.ethz.disco.signalprocessing.synchronisation.record.Frame;
import ch.ethz.disco.signalprocessing.synchronisation.record.GCCAudioRecordSynchronizer;
import speex.EchoCancellerHelper;

/**
 * Synchronizes the audio records and playbacks.
 */
public class GCCSynchronizer implements AudioSynchronizer {
    private static final String TAG = GCCSynchronizer.class.getCanonicalName();
    private final InitialChannelOffsetDetector initialChannelOffsetDetector;
    private GCCAudioRecordSynchronizer recordSynchronizer;
    private RecordConference recordConference = null;
    private PlaybackConference playbackConference = null;

    public GCCSynchronizer(int audioRecordSynchronisationLengthInFrames, ServerCommunicationInterface serverCommunicationInterface, EchoCancellerHelper echoCancellerHelper) {
        TotalOffsetBookkeeper totalOffsetBookkeeper = new TotalOffsetBookkeeper(serverCommunicationInterface);
        this.initialChannelOffsetDetector = new InitialChannelOffsetDetector(totalOffsetBookkeeper);
        this.recordSynchronizer = new GCCAudioRecordSynchronizer(audioRecordSynchronisationLengthInFrames, totalOffsetBookkeeper, echoCancellerHelper);
    }

    @Override
    public void setAudioSignalMixer(AudioSignalMixer audioSignalMixer) {
        recordSynchronizer.setAudioSignalMixer(audioSignalMixer);
    }

    @Override
    public void newRecordFrame(Frame frame) {
        initialChannelOffsetDetector.processFrame(frame);
        recordSynchronizer.processFrame(frame);
    }

    @Override
    public void setRecordConference(RecordConference recordConference) {
        if (this.recordConference != null) {
            Log.e(TAG, "Already having a RecordConference set");
            return;
        }
        this.recordConference = recordConference;
    }

    @Override
    public void setPlaybackConference(PlaybackConference playbackConference) {
        if (this.playbackConference != null) {
            Log.e(TAG, "Already having a playbackConference set");
            return;
        }
        Log.v(TAG, "Setting playback conference");
        this.playbackConference = playbackConference;
    }
}
