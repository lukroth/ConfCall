/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.networking.socket;

import android.util.Log;

import java.io.IOException;
import java.net.Socket;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import ch.ethz.disco.networking.wifibroadcaster.NetworkConstants;
import ch.ethz.disco.signalprocessing.synchronisation.record.AudioSignalMixer;

/**
 * In general all the communication between the master and the client (this device) runs through
 * this class.
 * <p>
 * This class collects the audio signal packets from the other conference participants and writes
 * them into the correct stream.
 * <p>
 * It is also responsible to send the audio data of this device to the other applications.
 */
public class ServerCommunicationInterface {
    private static final String TAG = ServerCommunicationInterface.class.getCanonicalName();
    private AudioSignalMixer audioSignalMixer;
    private ServerSocketAcceptor serverSocketAcceptor;

    /**
     * ChannelId -> SocketBlockReaders
     */
    private Map<Integer, SocketBlockReader> openSocketBlockReaders = new ConcurrentHashMap<>();

    public ServerCommunicationInterface() {
        serverSocketAcceptor = new ServerSocketAcceptor(NetworkConstants.PORT_COMMUNICATION_INTERFACE, new ServerSocketAcceptor.Listener() {
            @Override
            public void onNewSocket(Socket socket) {
                onNewSocketOuter(socket);
            }
        });
    }

    /**
     * Start accepting sockets and collecting data from the connected sockets.
     */
    public void start() {
        if (this.audioSignalMixer == null) {
            Log.e(TAG, "Set the audioSignalMixer");
            return;
        }
        serverSocketAcceptor.start();
    }

    /**
     * Stop accepting sockets and close connected sockets.
     */
    public void stop() {
        if (serverSocketAcceptor != null) {
            serverSocketAcceptor.stop();
        }
        stopSocketConnections();
    }

    private void stopSocketConnections() {
        for (SocketBlockReader socketBlockReader : openSocketBlockReaders.values()) {
            socketBlockReader.stopReading();
        }
    }

    private void onNewSocketOuter(Socket socket) {
        SocketBlockReader socketBlockReader;
        try {
            socketBlockReader = new SocketBlockReader(socket, audioSignalMixer, this);
            socketBlockReader.startReading();
            openSocketBlockReaders.put(socketBlockReader.getChannelId(), socketBlockReader);
        } catch (IOException e) {
            Log.e(TAG, "Failed to create a new SocketBlockReader", e);
        }
    }

    /**
     * Removes the passed socketBlockReader from the openSocketBlockReaders list. Assumes the socket
     * was already closed.
     *
     * @param socketBlockReader The object to remove
     */
    public void removeSocketBlockReader(SocketBlockReader socketBlockReader) {
        SocketBlockReader removedSocket = openSocketBlockReaders.remove(socketBlockReader.getChannelId());
        if (removedSocket == null) {
            Log.w(TAG, "You try to remove a socketBlockReader which is not in the list. ID = " + socketBlockReader.getChannelId());
        } else if (removedSocket != socketBlockReader) {
            Log.w(TAG, "Consistency error. Have socket id in list but does not match object. ID = " + socketBlockReader.getChannelId());
        } else {
            Log.v(TAG, "Removed socketBlockReader. ID = " + socketBlockReader.getChannelId());
        }
    }

    /**
     * Broadcasts the command, data.length, data to all the connected clients.
     *
     * @param command The command
     * @param value   The value associated with the command (is like an additional data field)
     * @param data    The data
     */
    public void broadcastCommandAndData(int command, int value, byte[] data) {
        for (Integer openChannelId : openSocketBlockReaders.keySet()) {
            sendCommandAndData(openChannelId, command, value, data);
        }
    }

    /**
     * Sends the command, data.length, data to the client.
     *
     * @param channelId The client we want to send to.
     * @param command   The command ({@link CommunicationInterfaceCommand}
     * @param value     an integer as additional information to the command
     * @param data      The data following the command.
     */
    public void sendCommandAndData(int channelId, int command, int value, byte[] data) {
        SocketBlockReader socket = openSocketBlockReaders.get(channelId);
        if (socket != null) {
            socket.sendCommandAndData(command, value, data);
        }
    }

    public void setAudioSignalMixer(AudioSignalMixer audioSignalMixer) {
        this.audioSignalMixer = audioSignalMixer;
    }
}
