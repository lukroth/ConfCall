/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.playback;

import java.util.HashSet;
import java.util.Set;

import ch.ethz.disco.audio.MuteController;

/**
 * Audio player that is able to add or remove samples to the audio stream it is playing.
 */
public abstract class SynchronisationCapableAudioPlayer implements AudioPlayer {
    private Set<MuteController> muteControllers = new HashSet<>();

    /**
     * Ignore/add samples to correct an offset:
     *
     * @param numberOfSamplesToAddToThisDevicesPlaybackStream The offset that should be corrected.
     */
    public abstract void correctOffset(int numberOfSamplesToAddToThisDevicesPlaybackStream);

    /**
     * Sets how many samples should be in the audioBuffer.
     *
     * @param admiredBufferContentLength The desired buffer length.
     * @return True if the value was big enough and was set. False otherwise.
     */
    public abstract boolean setAdmiredBufferContentLength(int admiredBufferContentLength);

    public void registerMuteController(MuteController muteController) {
        muteControllers.add(muteController);
    }

    public void deregisterMuteController(MuteController muteController) {
        muteControllers.remove(muteController);
    }

    /**
     * @return True if the audio player is on mute. Meaning one of its mute controllers is on mute.
     */
    public boolean isMute() {
        for (MuteController muteController : muteControllers) {
            if (muteController.isMute()) {
                return true;
            }
        }
        return false;
    }

}
