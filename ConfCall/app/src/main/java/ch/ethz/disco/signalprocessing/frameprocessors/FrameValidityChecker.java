/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.signalprocessing.frameprocessors;

import android.util.Log;

import java.util.Map;

/**
 * Can act as a preprocessor of any ChannelSelector to validate the frames passed to the
 * ChannelSelector.
 */
public class FrameValidityChecker {
    private static final String TAG = FrameValidityChecker.class.getCanonicalName();

    private FrameValidityChecker() {
    }

    /**
     * Checks if the frame is not null and not empty. Logs if null or empty.
     *
     * @param frame The frame that should be checked.
     * @return True if (frame==null)||(frame.isEmpty()), False otherwise
     */
    public static boolean isInvalidFrame(Map<Integer, short[]> frame) {
        if (frame == null) {
            Log.e(TAG, "Expected not null frame");
            return true;
        }
        if (frame.isEmpty()) {
            Log.e(TAG, "Expected at least one channel");
            return true;
        }
        return false;
    }
}
