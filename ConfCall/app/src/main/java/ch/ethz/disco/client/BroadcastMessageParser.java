/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.client;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import ch.ethz.disco.networking.wifibroadcaster.receive.Message;
import ch.ethz.disco.networking.wifibroadcaster.receive.NetworkMessageReceiver;
import ch.ethz.disco.networking.wifibroadcaster.receive.PacketReceiver;

import static ch.ethz.disco.networking.wifibroadcaster.WifiConferenceOfferMessageBroadcaster.Messages.MESSAGE_CONFERENCE_NAME;
import static ch.ethz.disco.networking.wifibroadcaster.WifiConferenceOfferMessageBroadcaster.Messages.MESSAGE_PORT;
import static ch.ethz.disco.networking.wifibroadcaster.WifiConferenceOfferMessageBroadcaster.Messages.MESSAGE_SAMPLE_RATE;
import static ch.ethz.disco.networking.wifibroadcaster.WifiConferenceOfferMessageBroadcaster.Messages.MESSAGE_TYPE;
import static ch.ethz.disco.networking.wifibroadcaster.WifiConferenceOfferMessageBroadcaster.Messages.MESSAGE_TYPE_START;

/**
 * Parses messages received by the PacketReceiver and triggers the corresponding actions.
 * <p>
 * e.g. that this device should start/stop broadcasting its audio record.
 */
public class BroadcastMessageParser implements NetworkMessageReceiver {
    private static final String TAG = BroadcastMessageParser.class.getCanonicalName();
    private final ConferenceScanner.Listener listener;

    BroadcastMessageParser(PacketReceiver packetReceiver, ConferenceScanner.Listener listener) {
        this.listener = listener;
        packetReceiver.registerNetworkMessageReceiver(this);
    }

    @Override
    public int getReceiverIdentifier() {
        return 0;
    }

    @Override
    public void handleMessage(Message message) {
        String messageString = message.getDataAsString();
        try {
            JSONObject jsonObject = new JSONObject(messageString);
            if (jsonObject.has(MESSAGE_TYPE)) {
                String messageType = jsonObject.getString(MESSAGE_TYPE);
                if (messageType.equals(MESSAGE_TYPE_START) &&
                        jsonObject.has(MESSAGE_SAMPLE_RATE) &&
                        jsonObject.has(MESSAGE_PORT) &&
                        jsonObject.has(MESSAGE_CONFERENCE_NAME)
                        ) {
                    listener.availableConferenceBroadcast(jsonObject.getInt(MESSAGE_SAMPLE_RATE),
                            message.getOriginalMessage().getAddress().getHostAddress(),
                            jsonObject.getInt(MESSAGE_PORT), jsonObject.getString(MESSAGE_CONFERENCE_NAME));
                    return;
                }
            }
        } catch (JSONException e) {
            Log.w(TAG, "Could not parse this message:" + messageString);
            return;
        }
        // All non-existent else blocks fall into this warn message.
        Log.w(TAG, "Could not parse this message:" + messageString);
    }
}
