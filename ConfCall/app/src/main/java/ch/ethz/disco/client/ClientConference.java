/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.client;


import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Timer;

import ch.ethz.disco.ConferenceSettings;
import ch.ethz.disco.audio.MuteController;
import ch.ethz.disco.confcall.R;

/**
 * Main entry point for a conference from the clients view. Waits for network packets that indicate
 * that a Server has initiated a conference. Starts a {@link RunningClientConference} if start
 * broadcast received.
 */
public class ClientConference implements RunningClientConferenceEventListener, ConferenceScanner.Listener {
    private static final String TAG = ClientConference.class.getCanonicalName();
    private static final long CLEANUP_CONFERENCES_INTERVAL_MS = 2000;
    private static Set<ClientConference> scanningClientConferences = new HashSet<>();
    private static boolean serverConference = false;
    private final Activity activity;
    private final ConferenceScanner conferenceScanner;
    private final Listener availableConferenceListener;
    /**
     * Null if no client conference is running.
     */
    private RunningClientConference runningClientConference;
    private boolean isScanningForOpenConferences = false;
    private List<AvailableConference> availableConferences = new ArrayList<>();

    private Timer cleanupOpenConferencesTimer = new Timer();
    /*
    May be null!
     */
    private ClientConferenceEventListener clientConferenceEventListener;


    public ClientConference(Activity activity, Listener availableConferenceListener) {
        this.activity = activity;
        this.availableConferenceListener = availableConferenceListener;
        conferenceScanner = new ConferenceScanner(activity, this);

    }

    public static void setIsServerConference(boolean serverConference) {
        ClientConference.serverConference = serverConference;
    }

    public static void stopRunningClientConferences() {
        for (ClientConference clientConference : scanningClientConferences) {
            clientConference.stopRunningClientConference();
        }
    }

    private static boolean isServerConference() {
        return serverConference;
    }

    /**
     * Starts listening for open conferences.
     */
    public void startScanningForOpenConferences() {
        if (ConferenceSettings.isConferenceSearcher()) {
            if (!isScanningForOpenConferences) {
                conferenceScanner.startScanning();
                scanningClientConferences.add(this);
                isScanningForOpenConferences = true;

                OldConferenceRemover oldConferenceRemover = new OldConferenceRemover(availableConferences, availableConferenceListener);
                cleanupOpenConferencesTimer.schedule(oldConferenceRemover, 0, CLEANUP_CONFERENCES_INTERVAL_MS);


            } else {
                Log.w(TAG, "Already scanning for open conferences. Ignoring this call");
            }

        }
    }

    /**
     * Stops listening for open conferences.
     */
    public void stopScanningForOpenConferences() {
        if (ConferenceSettings.isConferenceSearcher()) {
            if (isScanningForOpenConferences) {
                conferenceScanner.stopScanning();
                scanningClientConferences.remove(this);
                isScanningForOpenConferences = false;

                cleanupOpenConferencesTimer.cancel();
                cleanupOpenConferencesTimer = new Timer();
            } else {
                Log.w(TAG, "Not scanning for open conferences. Ignoring this call to stop");
            }
        }
    }

    @Override
    public void onStart(int sampleRate, String hostAddress, int hostPort) {
        activity.runOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(activity, activity.getResources().getString(R.string.onStartConference), Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onStop() {
        activity.runOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(activity, activity.getResources().getString(R.string.onStopConference), Toast.LENGTH_LONG).show();
            }
        });
        conferenceStopped();
    }

    @Override
    public void onReset(final String reason) {
        stopRunningClientConference();
        activity.runOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(activity, activity.getResources().getString(R.string.onResetConference) + reason, Toast.LENGTH_LONG).show();
            }
        });
        conferenceStopped();
    }

    @Override
    public void onErrorMsgForEndUser(final String msg) {
        activity.runOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(activity, msg, Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onErrorMsgForEndUser(int stringResourceId) {
        onErrorMsgForEndUser(activity.getResources().getString(stringResourceId));
    }

    private void conferenceStopped() {
        runningClientConference = null;
        startScanningForOpenConferences();
        if (clientConferenceEventListener != null) {
            clientConferenceEventListener.onConferenceStop();
        }
    }

    @Override
    public void availableConferenceBroadcast(int sampleRate, String hostAddress, int port, String conferenceName) {
        long timeStamp = System.currentTimeMillis();
        Log.v(TAG, "New available conference: sampleRate=" + sampleRate + ", hostAddress=" + hostAddress + ", port=" + port + ", conferenceName=" + conferenceName);
        AvailableConference availableConference = new AvailableConference(sampleRate, hostAddress, port, conferenceName, timeStamp);
        if (availableConferences.contains(availableConference)) {
            // If we knew about this conference we just need to update its timestamp.
            availableConferences.get(availableConferences.indexOf(availableConference)).setTimeStamp(timeStamp);
        } else {
            availableConferences.add(availableConference);
        }
        availableConferenceListener.onAvailableConferencesChanged(availableConferences);
    }

    public void stopRunningClientConference() {
        if (runningClientConference != null) {
            runningClientConference.stop();
        }
    }

    /**
     * @return True if succeeded, false otherwise.
     */
    public boolean joinOpenConference(int positionInList, String conferenceName) {
        AvailableConference availableConference = getOpenConference(positionInList, conferenceName);
        if (availableConference == null) {
            Toast.makeText(activity.getApplicationContext(), "Could not join the conference. Please retry.", Toast.LENGTH_LONG).show();
            Log.w(TAG, "Could not join the conference. Please retry.");
            return false;
        }
        if (runningClientConference == null && !isServerConference()) {
            runningClientConference = new RunningClientConference(activity, this);
            try {
                runningClientConference.start(availableConference.sampleRate, availableConference.hostAddress, availableConference.port);
                return true;
            } catch (IOException e) {
                Log.e(TAG, "Could not start the conference", e);
                runningClientConference.stop();
                runningClientConference = null;
                return false;
            }
        }
        return false;
    }

    private AvailableConference getOpenConference(int positionInList, String conferenceName) {
        // first we check if at the position in the list the conference is available.
        AvailableConference availableConference = null;
        if (availableConferences.size() > positionInList) {
            availableConference = availableConferences.get(positionInList);
        }
        if (availableConference == null) {
            Log.w(TAG, "Could not find the conference at position:" + positionInList + ", will try to find it by its name");
            return getOpenConferenceByName(conferenceName);
        } else if (availableConference.getName().equals(conferenceName)) {
            return availableConference;
        } else {
            Log.w(TAG, "Conference at requested position " + positionInList + " has name " + availableConference.getName() + ", but you requested for a conference with name " + conferenceName + ". Will ignore the position of the conference and search by name.");
            return getOpenConferenceByName(conferenceName);
        }
    }

    private AvailableConference getOpenConferenceByName(String conferenceName) {
        for (AvailableConference availableConference : availableConferences) {
            if (availableConference.getName().equals(conferenceName)) {
                return availableConference;
            }
        }
        return null;
    }

    public void registerEventListener(ClientConferenceEventListener clientConferenceEventListener) {
        this.clientConferenceEventListener = clientConferenceEventListener;
    }

    /**
     * @return A new muteController if available or null otherwise.
     */
    public MuteController getNewPlaybackMuteController() {
        if (runningClientConference != null) {
            return runningClientConference.getNewPlaybackMuteController();
        }
        return null;
    }

    public MuteController getNewRecordMuteController() {
        if (runningClientConference != null) {
            return runningClientConference.getNewRecordMuteController();
        }
        return null;
    }

    public interface Listener {
        void onAvailableConferencesChanged(List<AvailableConference> availableConferences);
    }
}