/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.signalprocessing.frameprocessors.relativegain;

import android.util.Log;

import java.util.HashMap;
import java.util.Map;

import ch.ethz.disco.signalprocessing.frameprocessors.ChannelSelector;
import ch.ethz.disco.signalprocessing.frameprocessors.FrameValidityChecker;
import ch.ethz.disco.signalprocessing.frameprocessors.filter.MostSeenSlidingWindowFilter;
import ch.ethz.disco.signalprocessing.synchronisation.record.AudioSignalMixer;

import static ch.ethz.disco.util.ArrayUtil.average;

/**
 * ChannelSelector that takes the frame with the maximum relative gain. This means it searches what
 * the maximum is this microphone measured and then selects the channel which has the higher average
 * gain / the maximum ever measured.
 */
public class RelativeMaximumGain implements ChannelSelector {
    private static final String TAG = RelativeMaximumGain.class.getCanonicalName();

    /**
     * The length of the filter which controls the change of speaker.
     * <p>
     * NOTE: It does not matter if we start at the wrong microphone and then switch later to the
     * better mic. But it sounds ugly if we switch at the end of each speech pause to another
     * microphone!
     */
    private static final int SPEAKER_CHANGE_FILTER_LENGTH_MS = 1000;
    /**
     * How long the slice should be that it will be measured as a maximum measured value.
     */
    private static final int MAX_MEASURED_SLICE_LENGTH_IN_MS = 2000;
    /**
     * How long a measured slice is valid.
     */
    private static final int SLICE_VALIDITY_IN_MS = 60 * 1000 * 20; // 20 Minutes
    /**
     * When no strong signal is present the mics should not change. This threshold ensures that to
     * get to a new active phone from an old one one must be above a certain threshold. Otherwise
     * the microphone changed always to one specific phone in silent times which makes no sense,
     * since most likely the old speaker continuous speaking after a 1 sec pause.
     */
    private static final Double RELATIVE_GAIN_THRESHOLD_TO_INITIATE_CHANGE = 0.1;
    private final MostSeenSlidingWindowFilter speakerChangeFilter;
    private final MaximumMeasurementsBookkeeper maximumMeasurementsBookkeeper;
    private final GainAdjuster gainAdjuster;
    private int oldActive = -1;

    public RelativeMaximumGain(int sampleRate, int frameLength) {
        int maxMeasuredSliceLength = (int) convertMsToFrames(sampleRate, MAX_MEASURED_SLICE_LENGTH_IN_MS, frameLength);
        int maxMeasuredSliceValidityWindow = (int) convertMsToFrames(sampleRate, SLICE_VALIDITY_IN_MS, frameLength);
        maximumMeasurementsBookkeeper = new MaximumMeasurementsBookkeeper(maxMeasuredSliceLength, maxMeasuredSliceValidityWindow);
        speakerChangeFilter = new MostSeenSlidingWindowFilter((int) convertMsToFrames(sampleRate, SPEAKER_CHANGE_FILTER_LENGTH_MS, frameLength));
        gainAdjuster = new GainAdjuster(sampleRate, frameLength);
    }

    /**
     * Calculates the amount of frames such that it will have a real length of ms
     *
     * @param sampleRatePerSec Sample rate of the audio data
     * @param lengthInMs       The length in ms which should be converted into frames
     * @param frameLenth       The length of one frame
     * @return The filter length that must be passed to the filter - or how many frames are used to
     * have the desired time
     */
    private static double convertMsToFrames(int sampleRatePerSec, int lengthInMs, int frameLenth) {
        return (double) lengthInMs * sampleRatePerSec / (1000 * frameLenth);
    }

    @Override
    public short[] selectFrame(Map<Integer, short[]> frame) {
        if (FrameValidityChecker.isInvalidFrame(frame)) {
            return new short[0];
        }
        return findCurrentActiveFrame(frame);
    }

    private short[] findCurrentActiveFrame(Map<Integer, short[]> frame) {
        int currentMaxRelativeIndex = findMaximumRelativeIndex(frame);
        int currentActiveFilteredIndex = speakerChangeFilter.newValue(currentMaxRelativeIndex);
        logActiveMicrophone(currentActiveFilteredIndex);
        oldActive = currentActiveFilteredIndex;
        if (frame.containsKey(currentActiveFilteredIndex)) {
            return gainAdjuster.adjustGain(frame.get(currentActiveFilteredIndex), maximumMeasurementsBookkeeper.getMaxMeasurement(currentActiveFilteredIndex), currentActiveFilteredIndex);
        } else {
            Log.v(TAG, "Active phone not sending at the moment. Returning the master phones frame instead");
            int hostChannelId = AudioSignalMixer.getHostChannelId();
            short[] hostFrame = frame.get(hostChannelId);
            if (hostFrame == null) {
                Log.e(TAG, "Active and host frames are empty");
            }
            return gainAdjuster.adjustGain(hostFrame, maximumMeasurementsBookkeeper.getMaxMeasurement(hostChannelId), hostChannelId);
        }
    }

    private void logActiveMicrophone(int currentActive) {
        if (oldActive != currentActive) {
            Log.v(TAG, "Changed active microphone to " + currentActive);
        }
    }

    private int findMaximumRelativeIndex(Map<Integer, short[]> frame) {
        Map<Integer, Double> avgGains = calgAvgGains(frame);
        maximumMeasurementsBookkeeper.onNewFrame(avgGains);
        Map<Integer, Double> relativeValues = maximumMeasurementsBookkeeper.getRelativeFromAverageGains(avgGains);
        if (relativeValues.size() != frame.size()) {
            Log.e(TAG, "Expected the relative values to have the same size as the noise values");
        }
        double maxRelVal = 0;
        int maxRelValIndex = -1;
        for (Integer channelId : relativeValues.keySet()) {
            double relativeGain = relativeValues.get(channelId);
            if (relativeGain > maxRelVal) {
                maxRelVal = relativeGain;
                maxRelValIndex = channelId;
            }
        }
        if (maxRelValIndex == -1) {
            // could not find maximum inside data, most likely because no one is speaking. Returning the old active microphone
            return oldActive;
        }
        if (relativeValues.get(maxRelValIndex) < RELATIVE_GAIN_THRESHOLD_TO_INITIATE_CHANGE) {
            // The new microphone was not loud enough to get active.
            return oldActive;
        }
        return maxRelValIndex;
    }

    private Map<Integer, Double> calgAvgGains(Map<Integer, short[]> frame) {
        Map<Integer, Double> avgGains = new HashMap<>(frame.size());
        for (Integer channelId : frame.keySet()) {
            avgGains.put(channelId, average(frame.get(channelId)));
        }
        return avgGains;
    }
}
