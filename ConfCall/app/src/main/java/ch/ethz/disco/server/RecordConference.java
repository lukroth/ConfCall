/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.server;

import android.media.AudioRecord;
import android.util.Log;

import ch.ethz.disco.networking.socket.ServerCommunicationInterface;
import ch.ethz.disco.signalprocessing.synchronisation.record.AudioSignalMixer;

public class RecordConference implements AudioDeviceReader.ErrorListener {
    private static final String TAG = RecordConference.class.getCanonicalName();
    private final ServerCommunicationInterface serverCommunicationInterface;
    /**
     * The sample rate at which the {@code RecordConference} is operating.
     */
    private final int sampleRate;
    private final AudioSignalMixer audioSignalMixer;
    private final AudioDeviceReader audioDeviceReader;
    private final ServerConference serverConference;
    /**
     * True if the RecordConference is running. False otherwise.
     */
    private boolean isRunning = false;

    /**
     * @param record                       Assumes an Audiorecord which is properly initialized and
     *                                     startRecording is already called on the record.
     * @param sampleRate                   The sample rate the conference App is supposed to use.
     * @param serverCommunicationInterface The {@link ServerCommunicationInterface} which must be
     *                                     properly initialized.
     * @param audioSignalMixer             The initialized audioSignal Mixer
     * @param serverConference             The {@link ServerConference} this {@link
     *                                     RecordConference} belongs to
     */
    RecordConference(AudioRecord record, int sampleRate, ServerCommunicationInterface serverCommunicationInterface, AudioSignalMixer audioSignalMixer, ServerConference serverConference) {
        this.sampleRate = sampleRate;
        this.serverCommunicationInterface = serverCommunicationInterface;
        this.audioSignalMixer = audioSignalMixer;
        this.audioDeviceReader = new AudioDeviceReader(record, this.audioSignalMixer, this);
        this.serverConference = serverConference;
    }


    /**
     * Reads audio data from the conference interface into a short array.
     *
     * @param audioData      short: the array to which the recorded audio data is written.
     * @param offsetInShorts int: index in audioData to which the data is written expressed in
     *                       shorts. Must not be negative, or cause the data access to go out of
     *                       bounds of the array.
     * @param sizeInShorts   int: the number of requested shorts. Must not be negative, or cause the
     *                       data access to go out of bounds of the array.
     * @return int: the number of read shorts
     */
    public int read(short[] audioData, int offsetInShorts, int sizeInShorts) {
        return audioSignalMixer.read(audioData, offsetInShorts, sizeInShorts);
    }

    /**
     * Starts the record conference
     */
    public void start() {
        if (isRunning) {
            Log.e(TAG, "RecordConference is already running. Ignoring the start() call");
            return;
        }
        isRunning = true;
        serverCommunicationInterface.start();
        audioSignalMixer.start();
        audioDeviceReader.startRecording();
    }

    /**
     * Stops the record conference
     */
    public void stop() {
        if (serverConference != null) {
            serverCommunicationInterface.stop();
        }
        if (audioSignalMixer != null) {
            audioSignalMixer.stop();
        }
        if (audioDeviceReader != null) {
            audioDeviceReader.stopRecording();
        }
        isRunning = false;
    }

    public int getSampleRate() {
        return sampleRate;
    }

    @Override
    public void onError(String msg) {
        serverConference.stop();
    }
}
