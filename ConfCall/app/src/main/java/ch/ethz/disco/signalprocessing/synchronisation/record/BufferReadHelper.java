/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.signalprocessing.synchronisation.record;

import java.util.concurrent.BlockingQueue;

/**
 * Wraps a {@link BlockingQueue<short[]>} and enables to easy read from this queue N bytes, possibly spread
 * over multiple elements of the queue.
 */
class BufferReadHelper {
    private final BlockingQueue<short[]> processedShortsBuffer;
    private int posInNextShortBuff;
    private short[] nextShortBuff;

    /**
     *
     * @param processedShortsBuffer The Buffer Queue that should be wrapped to read from.
     */
    BufferReadHelper(BlockingQueue<short[]> processedShortsBuffer) {
        this.processedShortsBuffer = processedShortsBuffer;
        this.posInNextShortBuff = 0;
        this.nextShortBuff = null;
    }

    public synchronized int readFromBuffer(short[] audioData, int offsetInShorts, int sizeInShorts) throws InterruptedException {
        for (int audioDataFillingOffset = offsetInShorts; audioDataFillingOffset < offsetInShorts + sizeInShorts; ) {
            int toRead = offsetInShorts + sizeInShorts - audioDataFillingOffset;
            if (toRead == 0) {
                return sizeInShorts;
            }
            checkIfNewBufferNeedsToBeTaken();
            int available = nextShortBuff.length - posInNextShortBuff;
            int effectiveRead = Math.min(toRead, available);
            System.arraycopy(nextShortBuff, posInNextShortBuff, audioData, audioDataFillingOffset, effectiveRead);

            audioDataFillingOffset += effectiveRead;
            posInNextShortBuff += effectiveRead;
        }
        return sizeInShorts;
    }

    private void checkIfNewBufferNeedsToBeTaken() throws InterruptedException {
        if (nextShortBuff == null) {
            takeNextShortBuffer();
        }
        if (posInNextShortBuff == nextShortBuff.length) {
            takeNextShortBuffer();
        }
    }

    private void takeNextShortBuffer() throws InterruptedException {
        nextShortBuff = processedShortsBuffer.take();
        posInNextShortBuff = 0;
    }
}
