/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.networking.wifibroadcaster;

/**
 * Network constants of ConfCall.
 */
public final class NetworkConstants {
    public static final int PORT_BROADCAST_IP4 = 3029;
    public static final int PORT_MULTICAST_IP6 = 3033;
    public static final String ADDRESS_IP6_MULTICAST = "224.2.76.24";
    public static final int PORT_COMMUNICATION_INTERFACE = 3030;
    public static final String ADDRESS_IP4_BROADCAST = "0.0.0.0";

    private NetworkConstants() {
    }
}
