package speex;

import android.util.Log;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

import ch.ethz.disco.signalprocessing.synchronisation.record.AudioSignalMixer;
import ch.ethz.disco.signalprocessing.synchronisation.record.Frame;

/**
 * Used to cancel out the echo that was recorded by the other devices (non-Host devices) when the
 * host plays sound.
 * <p>
 * This echo would be sent back to the caller of this conference if not removed with this class.
 */
public class EchoCancellerHelper {
    private static final String TAG = EchoCancellerHelper.class.getName();
    /**
     * The tail length of the Echo canceller. See speex echo canceller doc.
     * <p>
     * I think it should be equal or higher than the {@link this#PLAYBACK_QUEUE_MAX_LENGTH_MS} since
     * we want to be able to filter out echo which are delayed by max {@link
     * this.#PLAYBACK_QUEUE_MAX_LENGTH_MS}. But there might also be a case when the playback queue
     * gets filled with delay and then the taillength would have to be longer? I think this does not
     * happen.
     */
    private static final int TAIL_LENGHT_MS = 1000;
    /**
     * The maximum length the PlaybackQueue can reach since we are not interested in to old
     * playback-data when removing the echo. Speex specifies that the PlaybackData corresponding to
     * the echo frame should be already played when we give it to the echo canceller - otherwise it
     * is obvious that the echo cannot be removed.
     */
    private static final int PLAYBACK_QUEUE_MAX_LENGTH_MS = TAIL_LENGHT_MS;
    /**
     * The audio records were adjusted by offsets from time to time to align them. This means
     * samples get added/removed to the signal we get. When this happens we reset the echo canceller
     * if the offset was to high.
     */
    private static final int OFFSET_CORRECTION_RESET_THRESHOLD_SAMPLES = 100;

    private final int sampleRate;
    private final int frameLength;
    /**
     * ChannelId -> corresponding echo canceller.
     */
    private Map<Integer, EchoCanceller> echoCancellerMap = new HashMap<>();
    /**
     * Used to queue the playback data.
     */
    private short[] playbackFillingFrame;
    /**
     * The position inside the {@link this#playbackFillingFrame} where we should write the next
     * shorts.
     */
    private int playbackFillingFramePos = 0;
    /**
     * The audio data that was played but not yet consumed by the echo canceller.
     */
    private Queue<short[]> playbackQueue = new LinkedList<>();

    /**
     * @param sampleRate  The sample rate in HZ
     * @param frameLength The length of the data junks inside the frames that are passed to the
     *                    {@link EchoCancellerHelper}
     */
    public EchoCancellerHelper(int sampleRate, int frameLength) {
        this.sampleRate = sampleRate;
        this.frameLength = frameLength;
        playbackFillingFrame = new short[frameLength];
        playbackFillingFramePos = 0;
    }

    /**
     * For the passed {@link Frame} tries to remove the echo expect from the HosChannel since there
     * Android does the job.
     *
     * @param frame The frame which should be changed such that no echo is present anymore.
     */
    public void processFrame(Frame frame) {
        Map<Integer, short[]> channelsToData = frame.getChannelsToDataMap();
        Map<Integer, short[]> channelsToDataResult = new HashMap<>();
        int hostChannelId = AudioSignalMixer.getHostChannelId();
        short[] playbackData = getPlaybackData();
        if (playbackData == null) {
            // If we do not have any playback data we just return since we cannot do echo cancellation.
            Log.e(TAG, "No playback data!, cannot do echo cancellation");
            resetAllEchoCancellers();
            return;
        }

        for (Map.Entry<Integer, short[]> entry : channelsToData.entrySet()) {
            // Think about wether it makes sense to enable the speex echo canceller also on the master or not...
            // We let the speex echo canceller also do its job if the android echo canceller is available...
            // if ((entry.getKey() != hostChannelId) || DISABLE_ANDROID_ECHO_CANCELLER) {
            // For the host channel android does the echo filtering.
            short[] echoCancelledData = getEchoCancellerForChannel(entry.getKey()).process(entry.getValue(), playbackData);
            channelsToDataResult.put(entry.getKey(), echoCancelledData);
            //}
        }
    }

    /**
     * Returns the echo canceller or creates a new one if no one exists.
     *
     * @param channelId The channelId for which the echo canceller is searched.
     * @return The {@link EchoCanceller} for the ChannelId passed as an argument.
     */
    private EchoCanceller getEchoCancellerForChannel(Integer channelId) {
        EchoCanceller echoCanceller = echoCancellerMap.get(channelId);
        if (echoCanceller == null) {
            echoCanceller = new EchoCanceller();
            int tailLength = (sampleRate * TAIL_LENGHT_MS) / 1000;
            echoCanceller.open(sampleRate, frameLength, tailLength);
            echoCancellerMap.put(channelId, echoCanceller);
        }
        return echoCanceller;
    }

    /**
     * Should be called for all the data played on the device. Is then used to remove the echo.
     */
    public synchronized void playedData(short[] audioData, int offsetInShorts, int sizeInShorts) {
        for (int pos = offsetInShorts; pos < sizeInShorts + offsetInShorts; pos++) {
            if (playbackFillingFramePos == frameLength) {
                newFullPlaybackFillingFrame();
            }
            playbackFillingFrame[playbackFillingFramePos++] = audioData[pos];
        }
    }

    /**
     * Must be called when the {@link this#playbackFillingFrame} is full.
     */
    private void newFullPlaybackFillingFrame() {
        playbackQueue.add(playbackFillingFrame);
        playbackFillingFrame = new short[frameLength];
        playbackFillingFramePos = 0;
    }

    /**
     * @return The playback data recently played or null if no data is available.
     */
    private short[] getPlaybackData() {
        while (playbackQueue.size() > ((sampleRate * PLAYBACK_QUEUE_MAX_LENGTH_MS) / 1000) / frameLength) {
            Log.e(TAG, "Removing form the playback queue because we want this queue to be short since the echo canceller needs the playback data before the record data");
            playbackQueue.poll();
        }
        return playbackQueue.poll();
    }


    /**
     * Resets the echo cancellers of all channels.
     */
    private void resetAllEchoCancellers() {
        for (Integer channelId : echoCancellerMap.keySet()) {
            resetEchoCancellerOfChannel(channelId);
        }
    }

    /**
     * Resets the echo canceller for the channel passed (if we even have a echo canceller for this
     * channel).
     *
     * @param channelId The channelId of the echo canceller which should be reset.
     */
    private void resetEchoCancellerOfChannel(Integer channelId) {
        EchoCanceller echoCanceller = echoCancellerMap.get(channelId);
        if (echoCanceller != null) {
            Log.v(TAG, "Resetting the echo canceller of channel:" + channelId);
            echoCanceller.reset();
        }
    }

    public void correctOffsetInAudioRecord(Integer channelId, Integer offset) {
        if (Math.abs(offset) > OFFSET_CORRECTION_RESET_THRESHOLD_SAMPLES) {
            Log.v(TAG, "Resetting the echo canceller since an offset of " + offset + " was corrected in the record. ChannelId=" + channelId);
            this.resetEchoCancellerOfChannel(channelId);
            return;
        }
        Log.v(TAG, "An offset of " + offset + " was corrected in the record. ChannelId=" + channelId + ", but no actions were taken in the echo canceller.");
    }
}
