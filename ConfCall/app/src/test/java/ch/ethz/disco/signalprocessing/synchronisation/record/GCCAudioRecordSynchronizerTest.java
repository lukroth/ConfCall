/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.signalprocessing.synchronisation.record;

import junit.framework.Assert;

import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

public class GCCAudioRecordSynchronizerTest extends GCCAudioRecordSynchronizer {
    private static final short[] shortArray1 = {1, 2, 3};
    private static final short[] shortArray2 = {4, 5, 6};
    private static final short[] concatenated12 = {1, 2, 3, 4, 5, 6};

    public GCCAudioRecordSynchronizerTest() {
        super(0, null, null);
    }

    @Test
    public void audioSignalAssemble() {
        Queue<Frame> queue = new LinkedList<>();
        Map<Integer, short[]> frame1 = new HashMap<>();
        frame1.put(0, shortArray1);
        frame1.put(1, shortArray1);
        queue.add(new Frame(1, frame1));
        Map<Integer, short[]> frame2 = new HashMap<>();
        frame2.put(0, shortArray2);
        frame2.put(2, shortArray1);
        queue.add(new Frame(2, frame2));
        GCCAudioRecordSynchronizer.FrameBundleProcessor frameBundleProcessor = new GCCAudioRecordSynchronizer.FrameBundleProcessor(queue);
        Map<Integer, short[]> assembledSignlas = frameBundleProcessor.assembleSignals();
        Assert.assertEquals("Only one signal was complete", 1, assembledSignlas.size());
        Assert.assertTrue("Signal 0 was complete", assembledSignlas.containsKey(0));
        Assert.assertTrue("Concatenated correctly",
                Arrays.equals(
                        concatenated12,
                        assembledSignlas.get(0)
                ));
    }
}