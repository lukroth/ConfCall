/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.signalprocessing.synchronisation.gcc;

import junit.framework.Assert;

import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class GCCPathOffsetFinderTest {
    private final static double delta = 0.0001;
    private final static long seed = 4209381;
    private final static int signalLength = 5000;
    private Random random = new Random(seed);

    @Test
    public void filterOutLittleGainedSignals() {
        Map<Integer, short[]> signals = new HashMap<>();
        signals.put(1, new short[]{1, 1});
        signals.put(2, new short[]{5, 5});
        signals.put(3, new short[]{5, 5});
        signals.put(4, new short[]{1, 1});
        Map<Integer, short[]> filteredSignals = GCCPathOffsetFinder.filterOutLittleGainedSignals(signals, 3);
        Assert.assertTrue("Filters out small signal", !filteredSignals.containsKey(1));
        Assert.assertTrue("Contains big enough signal", filteredSignals.containsKey(2));
        Assert.assertTrue("Contains big enough signal", filteredSignals.containsKey(3));
        Assert.assertTrue("Filters out small signal", !filteredSignals.containsKey(4));
    }

    @Test
    public void filterOutLittleGainedSignalsLongSignal() {
        Map<Integer, short[]> signals = new HashMap<>();
        signals.put(1, createRandomSignal(signalLength));
        Map<Integer, short[]> filteredSignals = GCCPathOffsetFinder.filterOutLittleGainedSignals(signals, 3);
        Assert.assertTrue("Contains big enough signal", filteredSignals.containsKey(1));
    }

    @Test
    public void averageGain() {
        double gain = GCCPathOffsetFinder.averageGain(new short[]{3, 3, 3, 3, 3, 3, 3, 3, 3, 3});
        Assert.assertEquals("Average gain correct", 3.0, gain, delta);
        gain = GCCPathOffsetFinder.averageGain(new short[]{3, 5, 3, 5});
        Assert.assertEquals("Average gain correct", 4, gain, delta);
    }

    @Test
    public void computeOffsets() {
        short[] signal1 = createRandomSignal(signalLength);
        int offset = 20;
        short[] signal2 = copyAndDelay(signal1, offset);

        Map<Integer, short[]> signals = new HashMap<>();
        signals.put(0, signal1);
        signals.put(1, signal2);
        Map<Integer, Map<Integer, Integer>> offsets = GCCPathOffsetFinder.computeOffsets(signals);
        Assert.assertEquals("Offset computed correctly and saved", offset, (int) offsets.get(0).get(1));

        offsets = GCCPathOffsetFinder.computeOffsets(signals, 0);
        Assert.assertEquals("Offset computed correctly and saved", offset, (int) offsets.get(0).get(1));
    }

    @Test
    public void computeOffset0To500() {
        // Try for some random numbers if it works
        for (int offset = 0; offset < 500; offset += 9) {
            computeOffsetTest(offset);
        }
    }

    private void computeOffsetTest(int offset) {
        short[] signal1 = createRandomSignal(signalLength);
        short[] signal2 = copyAndDelay(signal1, offset);
        int offsetS1S2 = GCCPathOffsetFinder.computeOffsetGCCPath(signal1, signal2);
        Assert.assertEquals("Correct Offset:", offset, offsetS1S2);

        signal1 = createRandomSignal(signalLength);
        signal2 = copyAndDelay(signal1, offset);
        int offsetS2S1 = GCCPathOffsetFinder.computeOffsetGCCPath(signal2, signal1);
        Assert.assertEquals("Correct Negative Offset:", -offset, offsetS2S1);
    }

    @Test
    public void offsetOfEmptySignal() {
        short[] signal1 = new short[100];
        short[] signal2 = new short[100];
        int offsetS1S2 = GCCPathOffsetFinder.computeOffsetGCCPath(signal1, signal2);
        Assert.assertEquals("Correct Offset of empty signal:", 0, offsetS1S2);

    }

    @Test
    public void computeOffsetSameSignal() {
        computeOffsetTest(0);
    }

    /*
     * Creates a copy of the signal and shifts it by offset to the left.
     * 123456789, 2 -> RR1234567, R = new Random number
     */
    private short[] copyAndDelay(short[] signal, int offset) {
        short[] result = new short[signal.length];
        for (int i = 0; i < offset; i++) {
            result[i] = (short) random.nextInt();
        }
        System.arraycopy(signal, 0, result, offset, signal.length - offset);
        return result;
    }

    /**
     * Creates a random short signal of the {@code signalLength}
     *
     * @param signalLength The length of the signal that should be created.
     * @return The newly created random signal.
     */
    private short[] createRandomSignal(int signalLength) {
        short[] result = new short[signalLength];
        for (int i = 0; i < result.length; i++) {
            result[i] = (short) random.nextInt();
        }
        return result;
    }

    @Test
    public void complexConjugate() {
        float[] signal = {1, 2, 3, 4, 5, 6};
        float[] signal_copy = new float[signal.length];
        System.arraycopy(signal, 0, signal_copy, 0, signal_copy.length);
        float[] signal_conjugated = {1, -2, 3, -4, 5, -6};

        GCCPathOffsetFinder.complexConjugate(signal);
        ensureFloatArraysAreEqual("Correct conjugate signal", signal, signal_conjugated);

        GCCPathOffsetFinder.complexConjugate(signal);
        ensureFloatArraysAreEqual("Twice conjugate signal should be equal to original signal", signal, signal_copy);
    }

    /**
     * Checks if the float arrays are equal up to a difference of delta. If not Assertion will fail
     * with the message {@code errorMessage}.
     *
     * @param errorMessage Message that should be displayed if the signals are not equal
     * @param signal1      The first signal to compare.
     * @param signal2      The second signal to compare.
     */
    private void ensureFloatArraysAreEqual(String errorMessage, float[] signal1, float[] signal2) {
        for (int i = 0; i < signal1.length; i++) {
            Assert.assertEquals(errorMessage + "\nArrays are not equal:\n" + Arrays.toString(signal1) + "\n" + Arrays.toString(signal2), signal1[i], signal2[i], delta);
        }
    }

    @Test
    public void normalize() {
        float[] signal = {5, 0, 0, 5, 1, 1};
        float[] normalizedSignal = {1, 0, 0, 1, (float) (1 / 1.41421356237), (float) (1 / 1.41421356237)};
        GCCPathOffsetFinder.normalize(signal);
        ensureFloatArraysAreEqual("Normalisation does not work", signal, normalizedSignal);
    }
}