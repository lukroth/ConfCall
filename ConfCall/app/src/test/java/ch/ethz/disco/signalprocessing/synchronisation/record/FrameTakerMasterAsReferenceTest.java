/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.signalprocessing.synchronisation.record;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class FrameTakerMasterAsReferenceTest {

    private int maxQueueSize = 2;
    private ChannelResetter channelResetter = new EmptyChannelResetter();

    @Test
    public void normalUse() throws InterruptedException {
        Map<Integer, BlockingQueue<short[]>> queue = new HashMap<>();
        BlockingQueue<short[]> q0 = new LinkedBlockingQueue<>();
        queue.put(0, q0);
        BlockingQueue<short[]> q1 = new LinkedBlockingQueue<>();
        queue.put(1, q1);
        FrameTakerMasterAsReference frameTaker = new FrameTakerMasterAsReference(queue, maxQueueSize, channelResetter);
        frameTaker.setTimeOutMs(0);
        q0.put(new short[0]);
        q1.put(new short[0]);
        Map<Integer, short[]> frame = frameTaker.takeNextFrame().getChannelsToDataMap();
        Assert.assertEquals("got the frame of correct size", 2, frame.size());
    }

    @Test
    public void secondChannelNotSending() throws InterruptedException {
        Map<Integer, BlockingQueue<short[]>> queue = new HashMap<>();
        BlockingQueue<short[]> q0 = new LinkedBlockingQueue<>();
        queue.put(0, q0);
        BlockingQueue<short[]> q1 = new LinkedBlockingQueue<>();
        queue.put(1, q1);
        FrameTakerMasterAsReference frameTaker = new FrameTakerMasterAsReference(queue, maxQueueSize, channelResetter);
        frameTaker.setTimeOutMs(0);

        q0.put(new short[0]);
        q0.put(new short[0]);
        q0.put(new short[0]);
        Map<Integer, short[]> frame = frameTaker.takeNextFrame().getChannelsToDataMap();
        Assert.assertEquals("Taking only input from the first channel", 1, frame.size());
    }

    @Test
    public void secondElemWhileWaiting() throws InterruptedException {
        Map<Integer, BlockingQueue<short[]>> queue = new HashMap<>();
        BlockingQueue<short[]> q0 = new LinkedBlockingQueue<>();
        queue.put(0, q0);
        final BlockingQueue<short[]> q1 = new LinkedBlockingQueue<>();
        queue.put(1, q1);
        FrameTakerMasterAsReference frameTaker = new FrameTakerMasterAsReference(queue, maxQueueSize, channelResetter);
        frameTaker.setTimeOutMs(1000);

        q0.put(new short[0]);
        new Timer().schedule(
                new TimerTask() {
                    @Override
                    public void run() {
                        try {
                            q1.put(new short[0]);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                },
                20
        );
        Map<Integer, short[]> frame = frameTaker.takeNextFrame().getChannelsToDataMap();
        Assert.assertEquals(2, frame.size());
    }

    @Test
    public void longRunning() throws InterruptedException {
        Map<Integer, BlockingQueue<short[]>> queue = new HashMap<>();
        BlockingQueue<short[]> q0 = new LinkedBlockingQueue<>();
        queue.put(0, q0);
        BlockingQueue<short[]> q1 = new LinkedBlockingQueue<>();
        queue.put(1, q1);
        FrameTakerMasterAsReference frameTaker = new FrameTakerMasterAsReference(queue, maxQueueSize, channelResetter);
        frameTaker.setTimeOutMs(0);
        for (int i = 0; i < 100; i++) {
            short[] s0 = new short[0];
            short[] s1 = new short[1];
            q0.put(s0);
            q1.put(s1);
            Map<Integer, short[]> frame = frameTaker.takeNextFrame().getChannelsToDataMap();
            Assert.assertEquals("got the frame of correct size", 2, frame.size());
            Assert.assertEquals("correct frame data", s0, frame.get(0));
            Assert.assertEquals("correct frame data", s1, frame.get(1));
        }
    }

    private class EmptyChannelResetter implements ChannelResetter {
        @Override
        public void resetChannels(Set<Integer> channelIds) {
        }

        @Override
        public void resetChannel(Integer channelId) {
        }
    }
}