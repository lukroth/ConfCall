/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.networking.socket;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import ch.ethz.disco.client.RunningClientConference;
import ch.ethz.disco.client.RunningClientConferenceEventListener;
import ch.ethz.disco.playback.AudioPlayer;

public class ClientCommunicationInterfaceTest {
    private ClientCommunicationInterface communicationInterface;

    @Before
    public void init() {
        InputStream inputStream = new ByteArrayInputStream(new byte[0]);
        AudioPlayer audioPlayer = new AudioPlayerMock();
        RunningClientConference runningConference = new RunningClientConference(null, new RunningConferenceRunningClientConferenceEventListenerMock());
        int sampleRate = 16000;
        communicationInterface = new ClientCommunicationInterface(inputStream, audioPlayer, runningConference);
    }

    @Test
    public void testIfAllCommandsAreHandled() {
        for (CommunicationInterfaceCommand possibleCommand : CommunicationInterfaceCommand.values()) {
            Assert.assertTrue("Should be able to handle command:" + possibleCommand, communicationInterface.handleCommand(possibleCommand, 0, new byte[0]));
        }
    }

    private class AudioPlayerMock implements AudioPlayer {
        @Override
        public void start() {
        }

        @Override
        public void pause() {
        }

        @Override
        public int getPlaybackHeadPosition() {
            return 0;
        }

        @Override
        public void stop() {

        }

        @Override
        public int write(short[] audioData, int offsetInShorts, int sizeInShorts, int writtenShorts) {
            return 0;
        }

        @Override
        public void startIfNotAlreadyPlaying() {
        }
    }

    private class RunningConferenceRunningClientConferenceEventListenerMock implements RunningClientConferenceEventListener {
        @Override
        public void onStart(int sampleRate, String hostAddress, int hostPort) {
        }

        @Override
        public void onStop() {
        }

        @Override
        public void onReset(String reason) {
        }

        @Override
        public void onErrorMsgForEndUser(String msg) {
        }

        @Override
        public void onErrorMsgForEndUser(int stringResourceId) {
        }
    }
}