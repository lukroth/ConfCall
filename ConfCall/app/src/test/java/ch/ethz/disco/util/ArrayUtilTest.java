/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.util;

import junit.framework.Assert;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ch.ethz.disco.playback.AudioBroadcaster;

public class ArrayUtilTest {
    private static final short[] shortArray1 = {1, 2, 3};
    private static final short[] shortArray2 = {4, 99, 6};
    private static final short[] shortArray3 = {7, 8, 9};
    private static final short[] concatenated12 = {1, 2, 3, 4, 99, 6};
    private static final short[] concatenated123 = {1, 2, 3, 4, 99, 6, 7, 8, 9};

    @Test
    public void concatenateArraysEmpty() {
        Assert.assertTrue("Empty Array concatenation",
                Arrays.equals(
                        new short[0],
                        ArrayUtil.concatenateShortArrays(new ArrayList<short[]>())
                ));
    }

    @Test
    public void concatenateArraysNormalUseCase() {
        List<short[]> shorts = new ArrayList<>();
        shorts.add(shortArray1);
        shorts.add(shortArray2);
        Assert.assertTrue("Correct concatenation",
                Arrays.equals(
                        concatenated12,
                        ArrayUtil.concatenateShortArrays(shorts)
                ));
        shorts.add(shortArray3);
        Assert.assertTrue("Correct concatenation",
                Arrays.equals(
                        concatenated123,
                        ArrayUtil.concatenateShortArrays(shorts)
                ));
    }

    @Test
    public void toByteArray1() {
        short[] data = new short[]{55};
        byte[] res = ArrayUtil.toByteArray(data, 0, 1);
        org.junit.Assert.assertArrayEquals(data, ArrayUtil.toShortArray(res));
    }

    @Test
    public void toByteArray2() {
        short[] data = new short[]{3, 5, 2, 5, 3, 5, 76, 7};
        byte[] res = ArrayUtil.toByteArray(data, 2, 2);
        org.junit.Assert.assertArrayEquals(new short[]{2, 5}, ArrayUtil.toShortArray(res));
    }

}