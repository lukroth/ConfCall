/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.signalprocessing.frameprocessors.snr;

import org.junit.Assert;
import org.junit.Test;

public class NoiseThresholdBookkeeperPerSignalTest {
    private int silenceWindowSize = 10;
    private int forgetThreshold = 200;
    private double epsilon = 0.1;

    @Test
    public void severalGains() {
        NoiseThresholdBookkeeperPerSignal bookkeeper = new NoiseThresholdBookkeeperPerSignal(silenceWindowSize, forgetThreshold);
        for (int i = 0; i < silenceWindowSize; i++) {
            bookkeeper.onNewGain(200);
        }
        for (int i = 0; i < silenceWindowSize; i++) {
            bookkeeper.onNewGain(100);
        }
        for (int i = 0; i < silenceWindowSize; i++) {
            bookkeeper.onNewGain(200);
        }
        // we expect to be the silence gain : 100 * silenceWindowSize
        Assert.assertEquals("Gain after first filling correct", 100, bookkeeper.getNoiseGain(), epsilon);
    }

    @Test
    public void forgetThresholdUsed() {
        // if we fill over the forget threshold
        NoiseThresholdBookkeeperPerSignal bookkeeper = new NoiseThresholdBookkeeperPerSignal(silenceWindowSize, forgetThreshold);
        for (int i = 1; i < silenceWindowSize* 2; i++) {
            bookkeeper.onNewGain(100);
        }
        Assert.assertEquals("Expect 100 gain", 100, bookkeeper.getNoiseGain(), epsilon);
        for (int i = 1; i < forgetThreshold + silenceWindowSize + 5; i++) {
            bookkeeper.onNewGain(300);
        }
        Assert.assertEquals("Expect only higher gain", 300, bookkeeper.getNoiseGain(), epsilon);
    }
}