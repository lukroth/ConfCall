/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.playback;

import junit.framework.Assert;

import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import ch.ethz.disco.networking.socket.CommunicationInterfaceCommand;

public class ServerCommunicationInterfaceCommandTest {

    @Test
    public void mapFilled() {
        //ensure the map has a key for all CommunicationInterfaceCommand
        for (CommunicationInterfaceCommand command : CommunicationInterfaceCommand.values()) {
            Assert.assertFalse("No code", 0 == command.getCode());
        }
    }

    @Test
    public void allDifferentCodes() {
        Set<Integer> seenCodes = new HashSet<>();
        for (CommunicationInterfaceCommand command : CommunicationInterfaceCommand.values()) {
            int code = command.getCode();
            if(seenCodes.contains(code)){
                Assert.fail("Twice the same Code:" + code);
            }
            seenCodes.add(code);
        }
    }

    @Test
    public void conversionWorks() {
        for (CommunicationInterfaceCommand command : CommunicationInterfaceCommand.values()) {
            Assert.assertEquals(command, CommunicationInterfaceCommand.fromCode(command.getCode()));
        }
    }
}