/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.signalprocessing.frameprocessors.filter;

import org.junit.Assert;
import org.junit.Test;

public class MinimumSlidingWindowFilterTest {

    @Test
    public void initialMinimum() {
        MinimumSlidingWindowFilter<Integer> minimumSlidingWindowFilter = new MinimumSlidingWindowFilter<>(10, 99);
        Assert.assertEquals("Initial minimum correct", 99, (long) minimumSlidingWindowFilter.getMinimum());
    }

    @Test
    public void addedNewMinimum() {
        MinimumSlidingWindowFilter<Integer> minimumSlidingWindowFilter = new MinimumSlidingWindowFilter<>(10, 99);
        minimumSlidingWindowFilter.onNewElement(22);
        Assert.assertEquals("minimum correct", 22, (long) minimumSlidingWindowFilter.getMinimum());
    }

    @Test
    public void addedMultipleNewValues() {
        MinimumSlidingWindowFilter<Integer> minimumSlidingWindowFilter = new MinimumSlidingWindowFilter<>(10, 99);
        minimumSlidingWindowFilter.onNewElement(22);
        minimumSlidingWindowFilter.onNewElement(33);
        minimumSlidingWindowFilter.onNewElement(11);
        Assert.assertEquals("minimum correct", 11, (long) minimumSlidingWindowFilter.getMinimum());
    }

    @Test
    public void forgetValuesOutOfFilterSize() {
        int filterLength = 3;
        MinimumSlidingWindowFilter<Integer> minimumSlidingWindowFilter = new MinimumSlidingWindowFilter<>(filterLength, 99);
        minimumSlidingWindowFilter.onNewElement(1);
        for (int i = 0; i < filterLength; i++) {
            minimumSlidingWindowFilter.onNewElement(50);
        }
        Assert.assertEquals("minimum correct", 50, (long) minimumSlidingWindowFilter.getMinimum());
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void emptyFilter() {
        new MinimumSlidingWindowFilter<>(0, 99);
    }

    @Test
    public void testIfMultipleInitialElementsAreReallyInQueue(){
        MinimumSlidingWindowFilter<Integer> minimumSlidingWindowFilter = new MinimumSlidingWindowFilter<>(3, 99);
        minimumSlidingWindowFilter.onNewElement(100);
        Assert.assertEquals("Still one initial element is present", 99, (long) minimumSlidingWindowFilter.getMinimum());
    }
}