/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.signalprocessing.synchronisation.gcc;

import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class OffsetFilterTest {
    @Test
    public void onNewOffsets100FilterPercentage() {
        int filterLength = 10;
        int scaleFactor = 10;
        OffsetFilter offsetFilter = new OffsetFilter(filterLength, 1, scaleFactor);
        Map<Integer, Map<Integer, Integer>> offsets = new HashMap<>();
        offsets.put(1, new HashMap<Integer, Integer>());
        Integer offset = 4444;
        offsets.get(1).put(2, offset);
        Map<Integer, Map<Integer, Integer>> res;
        for (int i = 0; i < filterLength - 1; i++) {
            res = offsetFilter.onNewOffsets(offsets);
            Assert.assertEquals("No results until whole filter is filled", 0, res.size());
        }
        res = offsetFilter.onNewOffsets(offsets);
        Assert.assertEquals("Result is the offset rounded down", (offset / scaleFactor) * scaleFactor, (long) res.get(1).get(2));

        // Test reset after one result:
        res = offsetFilter.onNewOffsets(offsets);
        Assert.assertEquals("No results since now the filter should be empty again", 0, res.size());
    }

    @Test
    public void onNewOffsets50FilterPercentage() {
        int filterLength = 10;
        int scaleFactor = 10;
        OffsetFilter offsetFilter = new OffsetFilter(filterLength, 0.5, scaleFactor);
        Map<Integer, Map<Integer, Integer>> offsets = new HashMap<>();
        offsets.put(1, new HashMap<Integer, Integer>());
        Integer offset = 4444;
        offsets.get(1).put(2, offset);
        Map<Integer, Map<Integer, Integer>> res;
        for (int i = 0; i < filterLength / 2; i++) {
            res = offsetFilter.onNewOffsets(offsets);
            Assert.assertEquals("No results until half of the filter is filled", 0, res.size());
        }
        res = offsetFilter.onNewOffsets(offsets);
        Assert.assertEquals("Result is the offset rounded down", (offset / scaleFactor) * scaleFactor, (long) res.get(1).get(2));
    }


    @Test
    public void onNewOffsetsScaleFactor() {
        int filterLength = 1;
        int scaleFactor = 10;
        OffsetFilter offsetFilter = new OffsetFilter(filterLength, 1, scaleFactor);
        Map<Integer, Map<Integer, Integer>> offsets = new HashMap<>();
        offsets.put(1, new HashMap<Integer, Integer>());
        Integer offset = 4444;
        offsets.get(1).put(2, offset);
        Map<Integer, Map<Integer, Integer>> res;
        res = offsetFilter.onNewOffsets(offsets);
        Assert.assertEquals("Result is the offset rounded down", 4440, (long) res.get(1).get(2));
    }




}