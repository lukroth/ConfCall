/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.signalprocessing.synchronisation.record;

import org.junit.Assert;
import org.junit.Test;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class BufferReadHelperTest {

    private short[] shortsOrdered = {1, 2, 3, 4, 5, 6, 7, 8, 9};
    private short[] shorts2 = {10, 11, 12, 13, 14, 15, 16, 17};

    @Test
    public void simple() throws InterruptedException {
        BlockingQueue<short[]> queue = new LinkedBlockingQueue<>();
        queue.put(shortsOrdered);
        BufferReadHelper bufferReadHelper = new BufferReadHelper(queue);
        short[] readBuffer = new short[shortsOrdered.length];
        bufferReadHelper.readFromBuffer(readBuffer, 0, readBuffer.length);
        Assert.assertArrayEquals(shortsOrdered, readBuffer);
    }

    @Test
    public void offset() throws InterruptedException {
        BlockingQueue<short[]> queue = new LinkedBlockingQueue<>();
        queue.put(shortsOrdered);
        BufferReadHelper bufferReadHelper = new BufferReadHelper(queue);
        short[] readBuffer = new short[5];
        bufferReadHelper.readFromBuffer(readBuffer, 1, 2);
        Assert.assertArrayEquals(new short[]{0, 1, 2, 0, 0}, readBuffer);
    }


    @Test
    public void multipleInputAndOutputArrays() throws InterruptedException {
        BlockingQueue<short[]> queue = new LinkedBlockingQueue<>();
        queue.put(shortsOrdered);
        queue.put(shorts2);
        BufferReadHelper bufferReadHelper = new BufferReadHelper(queue);

        short[] out1 = new short[2];
        bufferReadHelper.readFromBuffer(out1, 0, out1.length);
        Assert.assertArrayEquals(new short[]{1, 2}, out1);


        short[] out2 = new short[10];
        bufferReadHelper.readFromBuffer(out2, 0, out2.length);
        Assert.assertArrayEquals(new short[]{3, 4, 5, 6, 7, 8, 9, 10, 11, 12}, out2);

        short[] out3 = new short[4];
        bufferReadHelper.readFromBuffer(out3, 0, out3.length);
        Assert.assertArrayEquals(new short[]{13, 14, 15, 16}, out3);
    }
}