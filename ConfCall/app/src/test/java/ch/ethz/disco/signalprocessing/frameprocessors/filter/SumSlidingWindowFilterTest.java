/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.signalprocessing.frameprocessors.filter;

import org.junit.Assert;
import org.junit.Test;

public class SumSlidingWindowFilterTest {
    private double initialElement = 9999d;
    private static final double epsilon = 0.1;

    @Test
    public void initialMinimum() {
        int filterLength = 10;
        SumSlidingWindowFilter sumSlidingWindowFilter = new SumSlidingWindowFilter(filterLength, initialElement);
        Assert.assertEquals("Initial sum correct", initialElement * filterLength, sumSlidingWindowFilter.getSum(), epsilon);
    }

    @Test
    public void addedNewValue() {
        SumSlidingWindowFilter sumSlidingWindowFilter = new SumSlidingWindowFilter(2, initialElement);
        sumSlidingWindowFilter.onNewElement((double) 1);
        sumSlidingWindowFilter.onNewElement((double) 1);
        Assert.assertEquals("sum correct", 2, sumSlidingWindowFilter.getSum(), epsilon);
    }

    @Test
    public void addedMultipleNewValues() {
        SumSlidingWindowFilter sumSlidingWindowFilter = new SumSlidingWindowFilter(2, initialElement);
        sumSlidingWindowFilter.onNewElement((double) 33);
        sumSlidingWindowFilter.onNewElement((double) 44);
        sumSlidingWindowFilter.onNewElement((double) 55);
        Assert.assertEquals("sum correct", 44 + 55, sumSlidingWindowFilter.getSum(), epsilon);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void emptyFilter() {
        new SumSlidingWindowFilter(0, initialElement);
    }


    @Test
    public void basicFunctionality(){
        int filterLength = 10;
        SumSlidingWindowFilter sumSlidingWindowFilter = new SumSlidingWindowFilter(filterLength, initialElement);
        for(int i = 0; i < filterLength; i++){
            sumSlidingWindowFilter.onNewElement((double) 100);
        }
        Assert.assertEquals("Correct double sum", 100*filterLength, sumSlidingWindowFilter.getSum(), epsilon);
        Assert.assertEquals("Correct avg.", 100, sumSlidingWindowFilter.getAverage(), epsilon);
    }
}