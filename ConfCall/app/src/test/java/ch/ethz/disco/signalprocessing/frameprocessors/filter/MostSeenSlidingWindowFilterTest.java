/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.signalprocessing.frameprocessors.filter;

import org.junit.Assert;
import org.junit.Test;

public class MostSeenSlidingWindowFilterTest extends MostSeenSlidingWindowFilter {
    /**
     * Initializes with default value of length 10 for testing.
     */
    public MostSeenSlidingWindowFilterTest() {
        super(10);
    }

    @Test
    public void testNewValue() {
        int filterLength = 9;
        MostSeenSlidingWindowFilter filter = new MostSeenSlidingWindowFilter(filterLength);
        Assert.assertEquals(0, filter.newValue(5));
        Assert.assertEquals(0, filter.newValue(5));
        Assert.assertEquals(0, filter.newValue(5));
        Assert.assertEquals(0, filter.newValue(5));
        Assert.assertEquals(5, filter.newValue(5));
        Assert.assertEquals(5, filter.newValue(5));
        Assert.assertEquals(5, filter.newValue(5));
        Assert.assertEquals(5, filter.newValue(5));
        Assert.assertEquals(5, filter.newValue(5));
    }

    @Test
    public void testGetMostSeenValueAndCount() {
        int filterLength = 19;
        MostSeenSlidingWindowFilter filter = new MostSeenSlidingWindowFilter(filterLength);
        int i = 0;
        for (; i < 9; i++) {
            filter.newValue(5);
            Assert.assertEquals(0, filter.getMostSeenValueAndCount().getValue());
            Assert.assertEquals(filterLength - i - 1, filter.getMostSeenValueAndCount().getCount());
        }

        for (; i < 19; i++) {
            filter.newValue(5);
            Assert.assertEquals(5, filter.getMostSeenValueAndCount().getValue());
            Assert.assertEquals(i + 1, filter.getMostSeenValueAndCount().getCount());
        }
    }

    @Test
    public void testReset() {
        int filterLength = 9;
        MostSeenSlidingWindowFilter filter = new MostSeenSlidingWindowFilter(filterLength);
        for (int i = 0; i < filterLength; i++) {
            filter.newValue(77);
        }
        Assert.assertEquals(77, filter.newValue(77));
        filter.reset(filterLength);
        Assert.assertEquals(0, filter.newValue(77));
    }


    @Test
    public void correctInternalRepresentation() {
        this.reset(10);
        int testValue = 55;
        this.newValue(testValue);
        Assert.assertEquals("Correct size", 2, valuesToTimesSeen.size());
        Assert.assertEquals(9, (long) valuesToTimesSeen.get(0));
        Assert.assertEquals(1, (long) valuesToTimesSeen.get(testValue));

        this.newValue(testValue);
        Assert.assertEquals("Correct size", 2, valuesToTimesSeen.size());
        Assert.assertEquals(8, (long) valuesToTimesSeen.get(0));
        Assert.assertEquals(2, (long) valuesToTimesSeen.get(testValue));

    }
}