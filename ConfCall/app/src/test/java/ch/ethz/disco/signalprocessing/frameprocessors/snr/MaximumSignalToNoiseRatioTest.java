/*
 * Copyright (C) 2018 Lukas Roth - Eidgenössische Technische Hochschule Zürich (ETH Zürich)
 *
 * This file is part of ConfCall
 *
 * ConfCall is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.ethz.disco.signalprocessing.frameprocessors.snr;

import junit.framework.Assert;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class MaximumSignalToNoiseRatioTest {
    private static final int sampleRate = 44000;

    /**
     * Creates a signal containing at each sample the signalGain.
     *
     * @param frameLength Length of the signal
     * @param signalGain  The Value the signal should have at each sample
     * @return The created signal
     */
    private static short[] createSignal(int frameLength, short signalGain) {
        short[] signal = new short[frameLength];
        for (int i = 0; i < frameLength; i++) {
            signal[i] = signalGain;
        }
        return signal;
    }

    @Test
    public void calculateFilterLength() {
        Assert.assertEquals(22, (int) MaximumSignalToNoiseRatio.convertMsToFrames(sampleRate, 500, 1000));
    }

    /**
     * Tests the base functionality for two mics.
     */
    @Test
    public void simpleTestTwoMicsUnfiltered() {
        // We use findMaximumSNRIndex instead of selectFrame to get the unfiltered results
        int frameLength = 1000;
        MaximumSignalToNoiseRatio maximumSignalToNoiseRatio = new MaximumSignalToNoiseRatio(sampleRate, frameLength);

        // We first play 10 seconds "silent" audio
        Map<Integer, short[]> frame = new HashMap<>();
        frame.put(0, createSignal(frameLength, (short) 5));
        frame.put(1, createSignal(frameLength, (short) 10));
        for (int i = 0; i < MaximumSignalToNoiseRatio.convertMsToFrames(sampleRate, 10 * 1000, frameLength); i++) {
            maximumSignalToNoiseRatio.findMaximumSNRIndex(frame);
        }
        // We now assume the first signal has a better snr since it has an avg. noise gain of 5 where as
        // the second value has a noise gain of 10
        int result;

        // We if both are +- the same the first one should win because of the better noise value
        frame = new HashMap<>();
        frame.put(0, createSignal(frameLength, (short) 100));
        frame.put(1, createSignal(frameLength, (short) 101));
        result = maximumSignalToNoiseRatio.findMaximumSNRIndex(frame);
        Assert.assertEquals("CorrectFrameSelected", 0, result);

        // also if the first one has 0.5*second_gain less gain
        frame = new HashMap<>();
        frame.put(0, createSignal(frameLength, (short) 60));
        frame.put(1, createSignal(frameLength, (short) 100));
        result = maximumSignalToNoiseRatio.findMaximumSNRIndex(frame);
        Assert.assertEquals("CorrectFrameSelected", 0, result);

        // here the second one is expected to win
        frame = new HashMap<>();
        frame.put(0, createSignal(frameLength, (short) 30));
        frame.put(1, createSignal(frameLength, (short) 100));
        result = maximumSignalToNoiseRatio.findMaximumSNRIndex(frame);
        Assert.assertEquals("CorrectFrameSelected", 1, result);
    }
}